package com.zonzonzon.common.service;

import com.zonzonzon.common.domain.DomainAggregate;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Set;

public class ReflectionUtils {
  /**
   * Find a handler for the event in a given aggregate.
   *
   * @param aggregate Aggregate to call event handler in.
   * @param event A happened event.
   * @param publish Event handler should publish the event. New events are published. Reconstructed
   *     events - are not.
   */
  public static <E> void invokeMatchingEventHandlers(
      DomainAggregate aggregate, E event, boolean publish) {

    Set<Method> matchingMethods =
        com.zonzonzon.common.utils.ReflectionUtils.getMethodsByParameterType(
            aggregate.getClass(), event.getClass(), "on");

    matchingMethods.forEach(
        method -> {
          try {
            method.invoke(aggregate, event, publish);
          } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
          }
        });
  }
}
