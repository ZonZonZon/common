package com.zonzonzon.common.service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to generalize Application services. Application service for technical coordination.
 * Domain-specific logics should be extracted and placed in Domain Service. Application service
 * manipulates domain service.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ApplicationService {}
