package com.zonzonzon.common.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.zonzonzon.common.domain.DomainEvent;

/** Requirements for MQ or other adapters that will process the event for the outside needs. */
public interface EventPublishingOutgoingPort {

  /**
   * Publish event.
   *
   * @param event Some domain event.
   * @return Event is successfully published.
   */
  boolean publishEvent(DomainEvent event);

  /**
   * Publish any data structure.
   *
   * @param data Some non-formalized data.
   * @param topicName Topic to publish to.
   * @return Event is successfully published.
   */
  boolean publishEvent(JsonNode data, String topicName);
}
