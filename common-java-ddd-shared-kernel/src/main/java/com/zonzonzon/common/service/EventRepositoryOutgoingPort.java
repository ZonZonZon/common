package com.zonzonzon.common.service;

import com.zonzonzon.common.domain.DomainAggregate;
import com.zonzonzon.common.domain.DomainEvent;
import com.zonzonzon.common.domain.SnapshotEvent;
import java.util.UUID;

/**
 * Expected event repository operations.
 *
 * @param <E> One domain event. May be wrapped into specific technology class.
 * @param <M> Many domain events. May be wrapped into specific technology class.
 * @param <S> Snapshot event. May be wrapped into specific technology class.
 * @param <A> Many repository domain entities. May be wrapped into specific technology class.
 */
public interface EventRepositoryOutgoingPort<E, M, S, A> {

  /** A path to snapshot class that helps to distinguish between snapshots and events. */
  String SNAPSHOT_CLASS_PATH = SnapshotEvent.class.getCanonicalName();

  /** Save event. */
  E persist(DomainEvent event, String aggregateName);

  /**
   * Find event by id.
   *
   * @param eventId Identifier of an event to find.
   * @param aggregateName Aggregate name to point to a group of events.
   * @return Event if found.
   */
  E getById(UUID eventId, String aggregateName);

  /**
   * Find previous event id. All events are supposed to be related to one aggregate type and
   * instance.
   *
   * @param aggregateId Aggregate instance universal unique identifier.
   * @param aggregateName Aggregate name to point to a group of events.
   */
  E getPreviousEvent(UUID aggregateId, String aggregateName);

  /**
   * Find last event.
   *
   * @param aggregateId Aggregate instance universal unique identifier.
   * @param aggregateName Aggregate name to point to a group of events.
   */
  E getLastEvent(UUID aggregateId, String aggregateName);

  /**
   * Find one last snapshot-event.
   *
   * @param aggregateId Aggregate instance universal unique identifier.
   * @param aggregateName Aggregate name to point to a group of events.
   */
  S getLastSnapshotEvent(UUID aggregateId, String aggregateName);

  /**
   * Find events after a given snapshot-event.
   *
   * @param aggregateId Identifier of an aggregate to select events for.
   * @param aggregateName Aggregate name to point to a group of events.
   */
  M getLastEvents(UUID aggregateId, String aggregateName);

  /**
   * Mark the event published after publishing is accomplished.
   *
   * @param event Event to be set published.
   * @param aggregateName Aggregate name to point to a group of events.
   * @return Event in published state.
   */
  E setPublished(DomainEvent event, String aggregateName);

  /**
   * Get aggregate based on last snapshot plus latest events applied. Is used for update commands.
   *
   * @param aggregate Example of aggregate instance with identifier set.
   * @param aggregateName Aggregate name to point to a group of events.
   * @return Event in published state.
   */
  A getAggregate(DomainAggregate<?> aggregate, String aggregateName);
}
