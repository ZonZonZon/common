package com.zonzonzon.common.domain;

import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Snapshot event contains all aggregate data at the moment of snapshot. It includes all previous
 * event changes and doesn't add anything new to it.
 */
@Builder
@Getter
@ToString
public class SnapshotEvent implements DomainEvent {

  private final UUID id;
  private final ZonedDateTime actedAt;
  private final UUID actedBy;
  private final DomainAggregate data;
  private final long modelVersion;
  @Setter private boolean isPublished;
}
