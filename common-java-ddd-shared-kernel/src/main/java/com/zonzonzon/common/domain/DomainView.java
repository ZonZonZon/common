package com.zonzonzon.common.domain;

import java.time.ZonedDateTime;
import java.util.List;

/** Common fields in structure of all domain views. */
public interface DomainView {

  /** Domain business-errors history. */
  List<DomainError> getErrors();

  ZonedDateTime getLastModified();
}
