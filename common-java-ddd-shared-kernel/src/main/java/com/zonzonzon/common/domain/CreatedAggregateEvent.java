package com.zonzonzon.common.domain;

/** For first event when aggregate gets created. */
public interface CreatedAggregateEvent extends DomainEvent {}
