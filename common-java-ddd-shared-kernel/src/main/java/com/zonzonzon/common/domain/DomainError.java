package com.zonzonzon.common.domain;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/** Common domain error thrown. */
@Getter
@NoArgsConstructor
@Slf4j
public class DomainError extends Exception {

  /**
   * Creates an exception that can be thrown outside. Logs exception. Applies exception as a command
   * to be stored in each aggregate instance provided.
   *
   * @param code See @{@link DomainError#code}
   * @param message See @{@link DomainError#message}
   * @param aggregates Aggregates to apply error to: maybe instances of one aggregate type or
   *     different types.
   */
  public DomainError(
      String code, String message, Set<DomainAggregate<?>> aggregates) {

    super(message);

    this.code = code;
    this.message = message;
    this.occurredAt.add(ZonedDateTime.now());

    //noinspection ThrowableNotThrown
    aggregates.forEach(aggregate -> aggregate.handle(this));
    log.error("{} at {} in {}: {}", code, message, aggregates, occurredAt);
  }
  public DomainError(String message, Set<DomainAggregate<?>> aggregates) {
    this(null, message, aggregates);
  }

  /** Alpha-numeric code as a constant key to identify error, as message may change. */
  String code;

  /** Error message content. */
  @NotNull String message;

  /**
   * A list of moments when the same error has occurred. Equals comparison excludes occurred at
   * field. Aggregation of equal errors reduces aggregate size.
   */
  @Setter @NotNull List<ZonedDateTime> occurredAt = new ArrayList<>();

  public void addOccurredAt(ZonedDateTime occurredAt) {
    this.getOccurredAt().add(occurredAt);
  }
  public void addAllOccurredAt(List<ZonedDateTime> occurredAt) {
    this.getOccurredAt().addAll(occurredAt);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DomainError that = (DomainError) o;
    return Objects.equals(getCode(), that.getCode())
        && Objects.equals(getMessage(), that.getMessage());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getCode(), getMessage());
  }
}
