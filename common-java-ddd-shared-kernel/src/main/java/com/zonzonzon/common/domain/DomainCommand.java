package com.zonzonzon.common.domain;

import java.util.UUID;
import javax.validation.constraints.NotNull;

/** Command wrapped around an aggregate.
 */
public interface DomainCommand {

  UUID getId();

  /**
   * An identifier of a system user who created the command.
   */
  UUID getActedBy();

  /**
   * Data passed to command. This could be external event payload to parse values from.
   */
  @NotNull DomainAggregate getData();
}
