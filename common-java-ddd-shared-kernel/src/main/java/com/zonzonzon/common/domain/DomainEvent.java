package com.zonzonzon.common.domain;

import java.time.ZonedDateTime;
import java.util.UUID;

/** Event wrapped around an aggregate. */
public interface DomainEvent {

  UUID getId();

  /**
   * Event moment is registered on the application side. Otherwise, database applies default NOW()
   * value.
   */
  ZonedDateTime getActedAt();

  /**
   * An identifier of a system user responsible for the event.
   */
  UUID getActedBy();

  /**
   * Serialized aggregate tree data. Can contain all aggregate field values or changed fields only
   * {@link SnapshotEvent}.
   */
  DomainAggregate getData();

  /**
   * A model version of an enclosed aggregate data. Version number is a class constant and can't be
   * a part of persisted object. Aggregate can't be reconstructed with a version number different
   * from the one in class. So model is stored outside the aggregate data.
   */
  long getModelVersion();

  /**
   * Event is first persisted and then published, as storage is a single source of truth for events
   * when application is scaled. When message publication is acknowledged by MQ, then this field is
   * updated.
   */
  boolean isPublished();

  /**
   * See {@link DomainEvent#isPublished()}
   *
   * @param isPublished Set published.
   */
  void setPublished(boolean isPublished);
}
