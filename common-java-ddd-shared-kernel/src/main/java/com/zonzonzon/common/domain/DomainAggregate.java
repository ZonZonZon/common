package com.zonzonzon.common.domain;

import com.zonzonzon.common.service.ReflectionUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Common in all aggregate structures. Aggregates should only be constructed with NoArgsConstructor.
 * Their state is mutated only using command handlers (including Create command). This protects from
 * misuse when coding.
 *
 * @param <A> Aggregate type.
 */
public interface DomainAggregate<A extends DomainAggregate> {

  /**
   * Aggregate model version to be considered after migration. This is an example field that should
   * be hidden by a similar field in aggregate instance.
   */
  Integer MODEL_VERSION = 1;

  /** Unique aggregate object identifier. */
  UUID getId();

  /** Get current aggregate model version number. */
  Integer getModelVersion();

  /** Last event applied to this aggregate. */
  UUID getLastEventId();

  /** Set identifier of the last event applied to aggregate. */
  void setLastEventId(UUID eventId);

  /** This aggregate is not accessible to queries anymore. */
  Boolean isDeleted();

  /**
   * Amount of new events that happened after last snapshot. Is used to fire snapshots at some
   * intervals.
   */
  Integer getNewEventsCount();

  /** Set value of new events counter. */
  void setNewEventsCount(Integer count);

  /** A history of errors related to this aggregate instance. */
  List<DomainError> getErrors();

  /** Replaces the whole error history for this aggregate instance. */
  void setErrors(List<DomainError> errors);

  /**
   * Reconstructs the state of the aggregate.
   *
   * @param lastSnapshot Last snapshot of this aggregate instance.
   * @param lastEvents Events after last snapshot instance.
   */
  default A reconstruct(SnapshotEvent lastSnapshot, List<DomainEvent> lastEvents) {
    // Default all fields to last snapshot values:
    A aggregate = (A) lastSnapshot.getData();

    //    this.message = lastSnapshotState.message;

    // Apply events after last snapshot:
    lastEvents.forEach(
        event -> ReflectionUtils.invokeMatchingEventHandlers(aggregate, event, false));

    // Set last event id:
    aggregate.setLastEventId(
        lastEvents.stream()
            .reduce((previousEvent, currentEvent) -> currentEvent)
            .map(DomainEvent::getId)
            .orElse(null));

    aggregate.setNewEventsCount(lastEvents.size());

    return aggregate;
  }

  /**
   * Apply error to this aggregate.
   *
   * @param error Error event.
   * @return The same event serves as a command and as an event.
   */
  default DomainError handle(DomainError error) {
    return error;
  }

  /**
   * Adds error to the history of this aggregate instance. For duplicate errors only new time is
   * added to save in aggregate size.
   *
   * @param event Error event applied to this aggregate instance.
   */
  default void on(DomainError event, boolean publish) {
    List<DomainError> previousAggregateErrors = getErrors();
    List<DomainError> newAggregateErrors = new ArrayList<>();

    previousAggregateErrors.forEach(
        previousAggregateError -> {
          if (previousAggregateError.equals(event)) {
            previousAggregateError.addAllOccurredAt(event.getOccurredAt());
          } else {
            newAggregateErrors.add(event);
          }
        });

    previousAggregateErrors.addAll(newAggregateErrors);
  }
}
