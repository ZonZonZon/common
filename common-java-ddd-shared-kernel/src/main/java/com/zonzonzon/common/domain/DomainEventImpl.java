package com.zonzonzon.common.domain;

import java.time.ZonedDateTime;
import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Common implementation of domain event. Should only be used as technical mock like search pattern,
 * not as a real event.
 */
@Event
@Builder
@Getter
@ToString
public class DomainEventImpl implements DomainEvent {

  private final UUID id;
  private final ZonedDateTime actedAt;
  private final UUID actedBy;
  private final DomainAggregate data;
  private final long modelVersion;
  @Setter private boolean isPublished;
}
