# Common

<h2>Functionality</h2>
<p>Shared kernel libraries to reduce code duplicates and to have a single source of update. 
Import only applicable modules as they are grouped into different conforming technologies stacks:
<ul>
<li>COMMON-JAVA - Pure Java static method utilities</li>
<li>COMMON-JAVA-ADVANCED - Utilities based on some non-framework technologies</li>
<li>COMMON-JAVA-DDD - Domain-Driven design, Event-Sourcing and CQRS utilities</li>
<li>COMMON-SPRING - Common Spring technologies</li>
<li>COMMON-SPRING-REACTIVE - Utilities based on Spring with reactive HTTP Servers like Netty</li>
</ul>
<h2>Common Java Infrastructure Technologies</h2>
<ul>
<li>Java 11</li>
<li>Maven</li>
<li>Lombok</li>
<li>SLF4J</li>
<li>JUint5</li>
<li>AssertJ</li>
</ul>
<h2>Common Java Advanced Infrastructure Technologies</h2>
<ul>
<li>Common Java Infrastructure Technologies</li>
<li>AspectJ</li>
<li>Jackson</li>
</ul>
<h2>Common Java DDD Infrastructure Technologies</h2>
<ul>
<li>Common Java Advanced Infrastructure Technologies</li>
</ul>
<h2>Common Spring Infrastructure Technologies</h2>
<ul>
<li>Common Java Advanced Infrastructure Technologies</li>
<li>Spring Context</li>
<li>Spring Boot</li>
<li>Spring Web</li>
<li>Spring Data</li>
<li>Redis Jedis</li>
<li>Logback</li>
</ul>
<h2>Common Spring Reactive Infrastructure Technologies</h2>
<ul>
<li>Common Spring Infrastructure Technologies</li>
<li>Spring Messaging</li>
<li>Spring Cloud</li>
<li>Spring Integration</li>
</ul>

<h2>Exploitation</h2>
<h3>Build</h3>
Build a JAR artifact:
<br>
<code>mvn clean install</code>
<br>
Artifacts can be found at project root `/target` folder and remote Maven repository.
<h3>Deploy</h3>
Builds and deploys a JAR artifact to a global Maven repository:
<br>
<code>mvn clean deploy -P releaseProfile -s ../../.m2/common.xml</code>
<br>
Artifacts can be imported:
<br>

    <dependency>
        <groupId>com.zonzonzon</groupId>
        <artifactId>common-java-shared-kernel</artifactId>
        <version>1.0.0</version>
    </dependency>
<br>

[Versions history](VERSIONS.md)
