package com.zonzonzon.common.utils;

import static com.zonzonzon.common.utils.StringUtils.constantCaseToHeaderCase;
import static com.zonzonzon.common.utils.StringUtils.toKebabCase;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class StringUtilsTest {

  @ParameterizedTest
  @CsvSource({
    "One Two Three",
    "one two three",
    "OneTwoThree",
    "OneTwo Three",
    "One.Two-Three",
    "One..Two--Three",
    "One.twoThree"
  })
  void text_Converted_ToKebabCase(String text) {
    String expected = "one-two-three";
    String result = toKebabCase(text);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void constantText_Converted_IsHeaderCase() {
    String given = "MY_CONSTANT";
    String expected = "My Constant";
    String result = constantCaseToHeaderCase(given);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
    "'JOHN DOE','John Doe'",
    "'john doe','John Doe'",
    "'joHn dOe','John Doe'",
    "'p&g company','P&G Company'",
    "'abc company','ABC Company'",
    "'Abc company','ABC Company'"
  })
  void text_ToTitleCase_IsConverted(String given, String expected) {
    String result = StringUtils.toTitleCase(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void text_ToMethodCase_IsConverted() {
    String given = "someFieldName";
    String expected = "getSomeFieldName";
    String result = StringUtils.toMethodCase("get", given);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({"'JOHN DOE'", "'john  doe'", "'joHn dOe'", "'John Doe'"})
  void words_ToSnakeCase_IsConverted(String given) {
    String expected = "john_doe";
    String result = StringUtils.wordsToSnakeCase(given);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
    "'JOHN DOE','johnDoe'",
    "'john  doe','johnDoe'",
    "'joHn dOe','johnDoe'",
    "'John Doe','johnDoe'",
    "'John D.o.e','johnDOE'"
  })
  void words_ToCamelCase_IsConverted(String given, String expected) {
    String result = StringUtils.wordsToCamelCase(given, false);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({"'getSomeFieldValue','someFieldValue'", "'getIt','it'"})
  void getterName_ToCamelCase_IsConverted(String given, String expected) {
    String result = StringUtils.getterToCamelCase(given);
    assertThat(result).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({
    "'JOHN DOE','j-o-h-n-d-o-e'",
    "'john  doe','john-doe'",
    "'joHn dOe','jo-hn-d-oe'",
    "'John Doe','john-doe'",
    "'John D.o.e','john-d-o-e'"
  })
  void words_ToKebabCase_IsConverted(String given, String expected) {
    String result = StringUtils.toKebabCase(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  @SneakyThrows
  void inputStream_ToString_IsConverted() {
    String expected = "some string";
    ByteArrayInputStream stream = new ByteArrayInputStream(expected.getBytes(UTF_8));
    Reader given = new InputStreamReader(stream, "UTF-8");
    String result = StringUtils.toString(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  @SneakyThrows
  void text_InvisibleCharacters_AreRemoved() {
    char tabCharacter = '\t';
    String given = "some " + tabCharacter + "string";
    String expected = "some string";
    String result = StringUtils.removeInvisibleCharacters(given);
    assertThat(result).isEqualTo(expected);
  }
}
