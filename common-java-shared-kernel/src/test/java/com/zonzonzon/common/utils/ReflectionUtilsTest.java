package com.zonzonzon.common.utils;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class ReflectionUtilsTest {

  @Test
  @SuppressWarnings("Convert2MethodRef")
  void nullValue_CalledInAChain_NoExceptionIsThrown() {
    Integer given = null;
    Integer expected = null;
    Integer result = ReflectionUtils.getSafe(() -> given.intValue());
    assertThat(result).isEqualTo(expected);
    assertThrows(NullPointerException.class, () -> given.intValue());
  }

  @Test
  void privateField_Read_ValueIsReturned() {
    ExampleClass given = new ExampleClass();
    String expected = "value";
    String result = ReflectionUtils.getPrivateField(given, "field");
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void class_Read_PublicGettersAreReturned() {
    Class<?> given = ExampleClass.class;
    String expected = "getBool";
    String expected2 = "getField";
    List<Method> result = ReflectionUtils.getPublicGetters(given);
    List<String> methodNames = result.stream().map(Method::getName).collect(Collectors.toList());
    assertThat(methodNames).contains(expected);
    assertThat(methodNames).contains(expected2);
  }

  @Test
  void object_WithNullValues_NonNullAreUpdatedOnly()
      throws NoSuchMethodException, NoSuchFieldException, ClassNotFoundException,
          InvocationTargetException, IllegalAccessException {

    ExampleClass givenSource = new ExampleClass();
    ExampleClass givenTarget = new ExampleClass();
    int expected = 1;
    String expected2 = "value";
    givenSource.setInteger(expected);
    givenSource.setField(null);
    ReflectionUtils.updateWithNonNull(givenSource, givenTarget, false, emptyList());
    assertThat(givenTarget.getInteger()).isEqualTo(expected);
    assertThat(givenTarget.getField()).isEqualTo(expected2);
    givenTarget = new ExampleClass();
    ReflectionUtils.updateWithNonNull(givenSource, givenTarget, false, List.of("getInteger"));
    assertThat(givenTarget.getInteger()).isZero();
  }

  @Test
  void class_Called_CallerClassNameAndMethodIsDefined() {
    String expected =
        "com.zonzonzon.common.utils.ReflectionUtilsTest.class_Called_CallerClassNameAndMethodIsDefined()";
    String result = ReflectionUtils.getCallerClassName(1);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void class_Called_StackTraceElementIsReturned() {
    String expected = "com.zonzonzon.common.utils.ReflectionUtilsTest";
    StackTraceElement result = ReflectionUtils.getCaller(1);
    assertThat(result.getClassName()).isEqualTo(expected);
  }

  @Test
  void class_MethodsWithParameters_AreReturned() {
    Class<ExampleClass> givenClass = ExampleClass.class;
    Class<ExampleSubclass> givenParameterClass = ExampleSubclass.class;
    String expected = "testMethod1";

    Set<Method> result =
        ReflectionUtils.getMethodsByParameterType(givenClass, givenParameterClass, "testMethod1");

    List<String> methodNames = result.stream().map(Method::getName).collect(Collectors.toList());
    assertThat(methodNames).contains(expected);

    result = ReflectionUtils.getMethodsByParameterType(givenClass, givenParameterClass, "on");

    methodNames = result.stream().map(Method::getName).collect(Collectors.toList());
    assertThat(methodNames).isEmpty();
  }

  @Test
  void stackTrace_MethodNameRequired_IsReturned() {
    StackTraceElement given = ReflectionUtils.getCaller(1);
    String expected = "stackTrace_MethodNameRequired_IsReturned";
    String result = ReflectionUtils.getMethodName(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void class_MethodsByName_AreReturned() {
    Class<ExampleClass> givenClass = ExampleClass.class;
    String expected = "testMethod2";
    Method result = ReflectionUtils.getMethod("testMethod2", givenClass, true);
    assertThat(result.getName()).isEqualTo(expected);
  }

  @ParameterizedTest
  @CsvSource({"field,", "integer,", ",getBool", "container.value,"})
  void сlass_SearchedForAField_FoundOrNot(String fieldName, String specialFieldName) {
    boolean hasField = ReflectionUtils.hasField(ExampleClass.class, fieldName, specialFieldName);
    assertTrue(hasField);
  }

  @Test
  void classField_GetterIsSearched_IsReturned() {
    Class<ExampleClass> givenClass = ExampleClass.class;
    String expected = "getInteger";
    Method result = ReflectionUtils.getGetter(givenClass, "integer", null);
    assertThat(result.getName()).isEqualTo(expected);
  }

  @Test
  void class_WithListField_ListTypeIsReturned() {
    Class<ExampleClass> givenClass = ExampleClass.class;
    Class<ExampleSubclass> expected = ExampleSubclass.class;
    Class<?> result = ReflectionUtils.getCollectionType(givenClass, "subclasses");
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void class_IfIsCollection_IsAssessed() {
    List<?> given = emptyList();
    boolean result = ReflectionUtils.isClassCollection(given.getClass());
    assertThat(result).isTrue();

    ExampleClass givenNegative = new ExampleClass();
    result = ReflectionUtils.isClassCollection(givenNegative.getClass());
    assertThat(result).isFalse();
  }

  @Test
  void class_Instantiated_ObjectIsReturned() {
    Class<ExampleClass> given = ExampleClass.class;
    ExampleClass result = (ExampleClass) ReflectionUtils.getClassInstance(given, "value2");
    assertThat(result.getField()).isEqualTo("value2");
  }

  @Test
  void class_FieldIsSearchedByName_IsReturned() {
    Class<ExampleClass> given = ExampleClass.class;
    Field result = ReflectionUtils.getField(given, "container");
    assertThat(result.getName()).isEqualTo("container");
  }

  @Getter
  @Setter
  private static class ExampleClass {

    public ExampleClass() {}

    public ExampleClass(String field) {
      this.field = field;
    }

    private String field = "value";
    private int integer;
    private Boolean bool;
    private ExampleSubclass container;
    private List<ExampleSubclass> subclasses;

    public Boolean getBool() {
      return bool;
    }

    public void testMethod1(ExampleSubclass parameter) {}

    public void testMethod2(ExampleSubclass parameter) {}
  }

  @Getter
  private static class ExampleSubclass {
    private Set<Long> values;
  }
}
