package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class ValidationUtilsTest {

  @Test
  void input_ContainingInjection_IsSanitized() {
    String input = "New; DELETE FROM table; York";
    String expected = "New DELETE FROM table York";
    String result = ValidationUtils.sanitizeInjection(input);
    assertThat(result).isEqualTo(expected);
  }
}
