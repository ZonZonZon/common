package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class SqlUtilsTest {

  @Test
  void sqlQuery_WithStringVariablePlaceholder_VariableIsReplacedWithValue() {
    String givenQuery = "SELECT * FROM my_table WHERE name = :myName";
    String givenPlaceholderName = "myName";
    String givenPlaceholderValue = "Some? Value";
    String expected = "SELECT * FROM my_table WHERE name = 'Some Value'";
    String result =
        SqlUtils.substituteVariable(givenQuery, givenPlaceholderName, givenPlaceholderValue, true);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void sqlQuery_WithIntegerVariablePlaceholder_VariableIsReplacedWithValue() {
    String givenQuery = "SELECT * FROM my_table WHERE name = :my";
    String givenPlaceholderName = "my";
    Integer givenPlaceholderValue = 123;
    String expected = "SELECT * FROM my_table WHERE name = 123";
    String result =
        SqlUtils.substituteVariable(givenQuery, givenPlaceholderName, givenPlaceholderValue);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void sqlQuery_WithDoubleVariablePlaceholder_VariableIsReplacedWithValue() {
    String givenQuery = "SELECT * FROM my_table WHERE name = :my";
    String givenPlaceholderName = "my";
    Double givenPlaceholderValue = 123.0;
    String expected = "SELECT * FROM my_table WHERE name = 123.0";
    String result =
        SqlUtils.substituteVariable(givenQuery, givenPlaceholderName, givenPlaceholderValue);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void sqlQuery_WithUuidVariablePlaceholder_VariableIsReplacedWithValue() {
    String givenQuery = "SELECT * FROM my_table WHERE name = :my";
    String givenPlaceholderName = "my";
    UUID givenPlaceholderValue = UUID.fromString("3ad10ae0-88a7-484d-b0a5-1d70e8c65a86");
    String expected = "SELECT * FROM my_table WHERE name = 0x3ad10ae088a7484db0a51d70e8c65a86";
    String result =
        SqlUtils.substituteVariable(givenQuery, givenPlaceholderName, givenPlaceholderValue);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void sqlQuery_WithUuidListVariablePlaceholder_VariableIsReplacedWithValue() {
    String givenQuery = "SELECT * FROM my_table WHERE name IN (:my)";
    String givenPlaceholderName = "my";

    List<UUID> givenPlaceholderValue =
        List.of(
            UUID.fromString("3ad10ae0-88a7-484d-b0a5-1d70e8c65a86"),
            UUID.fromString("4ad10ae0-88a7-484d-b0a5-1d70e8c65a86"));

    String expected =
        "SELECT * FROM my_table WHERE name IN (0x3ad10ae088a7484db0a51d70e8c65a86, 0x4ad10ae088a7484db0a51d70e8c65a86)";

    String result =
        SqlUtils.substituteVariable(givenQuery, givenPlaceholderName, givenPlaceholderValue);

    assertThat(result).isEqualTo(expected);
  }

  @Test
  void sqlQuery_WithOffsetAndLimitPlaceholders_IsReplacedWithValues() {
    String given = "SELECT * FROM my_table OFFSET :offset LIMIT :limit";
    String expected = "SELECT * FROM my_table OFFSET 1 LIMIT 2";
    String result = SqlUtils.substitutePageable(given, 1, 2);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void sqlQuery_WithAssignmentCharacter_IsScreened() {
    String given = "SET @pv:=1";
    String expected = "SET @pv\\\\:=1";
    String result = SqlUtils.screenAssignmentSigns(given);
    assertThat(result).isEqualTo(expected);
  }
}
