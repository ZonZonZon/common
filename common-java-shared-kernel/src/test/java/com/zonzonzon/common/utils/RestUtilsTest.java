package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.UnsupportedEncodingException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.util.Map;
import org.junit.jupiter.api.Test;

class RestUtilsTest {

  @Test
  void methodWithRestAnnotation_RequestTypeDefined_IsReturned() throws NoSuchMethodException {
    Method given = RestUtilsTest.class.getMethod("testRequest");
    String expected = "POST";
    RestQueryType result = RestUtils.getRequestType(given);
    assertThat(result.getName()).isEqualTo(expected);
  }

  @Test
  void restRequestMethod_Parameters_AreListed()
      throws NoSuchMethodException, UnsupportedEncodingException {
    String given = "abc=123&abc=333&ddd=333";
    Map<String, String> result = RestUtils.getRequestParameters(given);
    assertThat(result.size()).isEqualTo(2);
    assertThat(result.get("abc")).isEqualTo("333");
    assertThat(result.get("ddd")).isEqualTo("333");
  }

  @Retention(RetentionPolicy.RUNTIME)
  public @interface PostMapping{};

  @PostMapping
  public void testRequest() {}
}
