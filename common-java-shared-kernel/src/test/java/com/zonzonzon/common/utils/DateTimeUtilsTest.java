package com.zonzonzon.common.utils;

import static java.util.Calendar.JANUARY;
import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import org.junit.jupiter.api.Test;

class DateTimeUtilsTest {

  @Test
  void dateTimeString_InSystemZone_ReturnedAsDate() throws ParseException {
    String given = "2022-01-01 19:19:19";
    Date result = DateTimeUtils.fromInSystemDefaultZone(given, DateTimeUtils.DATABASE_FORMATTER);
    Calendar expected = Calendar.getInstance();
    expected.clear();
    expected.set(2022, JANUARY, 1, 19, 19, 19);
    assertThat(result).isEqualTo(expected.getTime());
  }

  @Test
  void date_InSystemZone_FormattedAsString() {
    Calendar calendar = Calendar.getInstance();
    Date given = calendar.getTime();

    String expected =
        ZonedDateTime.now().withZoneSameInstant(ZoneId.of("UTC")).toString().substring(0, 19);

    String result = DateTimeUtils.formatAsUtc(given, DateTimeUtils.ISO_8601_FORMATTER);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void date_HavingFromAndToFormatters_IsFormatted() {
    ZonedDateTime now = ZonedDateTime.now();
    String given = DateTimeUtils.ISO_8601_FORMATTER.format(now);
    String expected = DateTimeUtils.DATABASE_FORMATTER.format(now);

    String result =
        DateTimeUtils.format(
            given, DateTimeUtils.ISO_8601_FORMATTER, DateTimeUtils.DATABASE_FORMATTER);

    assertThat(result).isEqualTo(expected);
  }

  @Test
  void utcDate_AfterOffsetHours_IsFormatted() {
    ZonedDateTime now = ZonedDateTime.now();
    String givenUtcDateTime =
        DateTimeUtils.DATABASE_FORMATTER.withZone(ZoneId.of("UTC")).format(now);

    int givenOffsetHours = 1;

    String expected =
        DateTimeUtils.ISO_8601_FORMATTER
            .withZone(ZoneId.of("UTC"))
            .format(now.plusHours(givenOffsetHours));

    String result =
        DateTimeUtils.fromDatabaseUtcByOffsetHours(
            givenUtcDateTime, givenOffsetHours, DateTimeUtils.ISO_8601_FORMATTER);

    assertThat(result).isEqualTo(expected);
  }

  @Test
  void millis_Formatted_AsText() {
    long given = 12345678;
    String expected = "0 Days 3 Hours 25 Minutes 45 Seconds";
    String result = DateTimeUtils.formatToDaysHourMinSec(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void interval_Formatted_AsText() {
    ZonedDateTime givenFrom = ZonedDateTime.now();
    ZonedDateTime givenTo = givenFrom.plusDays(1).plusHours(2).plusMinutes(3).plusSeconds(4);
    String expected = "1 Days 2 Hours 3 Minutes 4 Seconds";
    String result = DateTimeUtils.formatIntervalToDaysHourMinSec(givenFrom, givenTo);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTimeString_StringPatternApplied_IsFormatted() {
    ZonedDateTime given = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("UTC"));
    String givenString = DateTimeUtils.ISO_8601_FORMATTER.format(given);
    ZonedDateTime expected = given.truncatedTo(ChronoUnit.SECONDS);
    ZonedDateTime result =
        DateTimeUtils.from(givenString, DateTimeUtils.ISO_8601_PATTERN_WITHOUT_ZONE);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTimeString_ToUtc_IsIso8601Formatted() {
    ZonedDateTime given = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    OffsetDateTime utc = OffsetDateTime.ofInstant(given.toInstant(), ZoneId.of("UTC"));
    OffsetDateTime paris = OffsetDateTime.ofInstant(given.toInstant(), ZoneId.of("Europe/Paris"));
    int offsetDifference = paris.getHour() - utc.getHour();

    String expected =
        given
            .minusHours(offsetDifference)
            .withZoneSameLocal(ZoneId.of("UTC"))
            .toString()
            .replace("[UTC]", "");

    String result = DateTimeUtils.toIso8601UtcFromIso8601Formatted(given.toString());
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTimeString_FromDatabaseFormat_IsIso8601Formatted() {
    ZonedDateTime dateTimeGiven = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("UTC"));
    TimeZone timezoneGiven = TimeZone.getTimeZone("Europe/Paris");
    OffsetDateTime utc = OffsetDateTime.ofInstant(dateTimeGiven.toInstant(), ZoneId.of("UTC"));

    OffsetDateTime paris =
        OffsetDateTime.ofInstant(dateTimeGiven.toInstant(), ZoneId.of("Europe/Paris"));

    int offsetDifference = paris.getHour() - utc.getHour();

    ZonedDateTime expected =
        dateTimeGiven.plusHours(offsetDifference).withZoneSameLocal(ZoneId.of("Europe/Paris"));

    ZonedDateTime result = DateTimeUtils.toAnotherZone(dateTimeGiven, timezoneGiven);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTime_ToIso8601String_IsFormatted() {
    ZonedDateTime dateTimeGiven = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("UTC"));
    TimeZone timezoneGiven = TimeZone.getTimeZone("Europe/Paris");
    OffsetDateTime utc = OffsetDateTime.ofInstant(dateTimeGiven.toInstant(), ZoneId.of("UTC"));

    OffsetDateTime paris =
        OffsetDateTime.ofInstant(dateTimeGiven.toInstant(), ZoneId.of("Europe/Paris"));

    int offsetDifference = paris.getHour() - utc.getHour();

    String expected =
        dateTimeGiven
            .plusHours(offsetDifference)
            .withZoneSameLocal(ZoneId.of("Europe/Paris"))
            .toString();

    String result = DateTimeUtils.offsetAsIso8601Formatted(timezoneGiven, dateTimeGiven);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTimeString_ToDatabaseString_IsFormatted() {
    ZonedDateTime dateTimeGiven = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    String dateTimeGivenString = dateTimeGiven.format(DateTimeFormatter.ISO_ZONED_DATE_TIME);

    int offsetDifference =
        DateTimeUtils.getOffsetHoursDifference(ZoneId.of("UTC"), ZoneId.of("Europe/Paris"));

    String expected =
        dateTimeGiven
            .minusHours(offsetDifference)
            .withZoneSameLocal(ZoneId.of("UTC"))
            .format(DateTimeUtils.DATABASE_FORMATTER);

    String result = DateTimeUtils.offsetToUtcDatabaseFormatted(dateTimeGivenString);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTimeAndLocale_ToAmPmString_IsFormatted() {
    ZonedDateTime givenDateTime = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    Locale givenLocale = Locale.US;

    String expected =
        givenDateTime.getMonth().getDisplayName(TextStyle.SHORT, givenLocale)
            + " "
            + givenDateTime.getDayOfMonth()
            + ", "
            + givenDateTime.getYear()
            + ", "
            + "0"
            + (givenDateTime.getHour() - 12)
            + ":"
            + givenDateTime.getMinute()
            + " "
            + (givenDateTime.getHour() > 12 ? "PM" : "AM")
            + " "
            + "CET";

    String result = DateTimeUtils.formatAsAmPm(givenDateTime, givenLocale);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTime_ToStandardAbbreviation_IsFormatted() {
    ZonedDateTime givenDateTime = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    String expected = "CET";
    String result = DateTimeUtils.getZoneAsStandardAbbreviation(givenDateTime);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTime_ToDstAbbreviation_IsFormatted() {
    ZonedDateTime givenDateTime = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    String expected = "CEST";
    String result = DateTimeUtils.getZoneAsDstAbbreviation(givenDateTime);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTime_ToStandardOffset_IsFormatted() {
    ZonedDateTime givenDateTime = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    String expected = "+01:00";
    String result = DateTimeUtils.getZoneAsStandardOffset(givenDateTime);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateTime_ToDstOffset_IsFormatted() {
    ZonedDateTime givenDateTime = ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    String expected = "+02:00";
    String result = DateTimeUtils.getZoneAsDstOffset(givenDateTime);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void iso8601String_ToZonedDateTime_IsParsed() {
    String given = "2021-01-14T11:52:02.160-08:00[America/Los_Angeles]";

    ZonedDateTime expected =
        ZonedDateTime.of(2021, 1, 14, 11, 52, 2, 160000000, ZoneId.of("America/Los_Angeles"));

    ZonedDateTime result = DateTimeUtils.fromIso8601(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void dateRange_GivenZoneAndLocale_IsFormatted() {
    ZonedDateTime givenFromDateTime =
        ZonedDateTime.now().withZoneSameLocal(ZoneId.of("Europe/Paris"));
    ZonedDateTime givenToDateTime =
        givenFromDateTime.plusHours(1).withZoneSameLocal(ZoneId.of("Europe/Paris"));
    Locale givenLocale = Locale.US;

    String expected =
        givenFromDateTime.getMonth().getDisplayName(TextStyle.SHORT, givenLocale)
            + " "
            + givenFromDateTime.getDayOfMonth()
            + ", "
            + givenFromDateTime.getYear()
            + ", "
            + "0"
            + (givenFromDateTime.getHour() - 12)
            + ":"
            + (givenFromDateTime.getMinute() > 9 ? "" : "0")
            + givenFromDateTime.getMinute()
            + " "
            + (givenFromDateTime.getHour() > 12 ? "PM" : "AM")
            + " "
            + "CET";

    String result = DateTimeUtils.formatAsAmPm(givenFromDateTime, givenLocale);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void localDate_ToDate_IsFormatted() {
    Calendar calendar = Calendar.getInstance();
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH) + 1;
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    LocalDate given = LocalDate.of(year, month, day);
    Date result = DateTimeUtils.fromDate(given);
    Calendar resultCalendar = Calendar.getInstance();
    resultCalendar.setTime(result);
    assertThat(resultCalendar.get(Calendar.YEAR)).isEqualTo(year);
    assertThat(resultCalendar.get(Calendar.MONTH) + 1).isEqualTo(month);
    assertThat(resultCalendar.get(Calendar.DAY_OF_MONTH)).isEqualTo(day);
  }

  @Test
  void date_ToLocalDate_IsFormatted() {
    Date given = new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(given);
    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH) + 1;
    int day = calendar.get(Calendar.DAY_OF_MONTH);
    LocalDate result = DateTimeUtils.fromDate(given);
    assertThat(result.getYear()).isEqualTo(year);
    assertThat(result.getMonthValue()).isEqualTo(month);
    assertThat(result.getDayOfMonth()).isEqualTo(day);
  }

  @Test
  void periodAndDates_Juxtaposed_DefinesIfIncluded() {
    ZonedDateTime now = ZonedDateTime.now();
    ZonedDateTime one = now.minusHours(2);
    ZonedDateTime two = now.minusHours(1);
    ZonedDateTime three = now.plusHours(1);
    ZonedDateTime four = now.plusHours(2);

    boolean result = DateTimeUtils.isWithinPeriod(one, four, two, three);
    assertThat(result).isTrue();
    result = DateTimeUtils.isWithinPeriod(one, four, one, four);
    assertThat(result).isTrue();
    result = DateTimeUtils.isWithinPeriod(null, four, one, four);
    assertThat(result).isTrue();
    result = DateTimeUtils.isWithinPeriod(one, null, one, four);
    assertThat(result).isTrue();
    result = DateTimeUtils.isWithinPeriod(null, null, one, four);
    assertThat(result).isTrue();
    result = DateTimeUtils.isWithinPeriod(one, two, three, four);
    assertThat(result).isFalse();
    result = DateTimeUtils.isWithinPeriod(one, three, three, four);
    assertThat(result).isFalse();
    result = DateTimeUtils.isWithinPeriod(one, three, two, four);
    assertThat(result).isFalse();
    result = DateTimeUtils.isWithinPeriod(three, four, one, two);
    assertThat(result).isFalse();
    result = DateTimeUtils.isWithinPeriod(three, four, one, three);
    assertThat(result).isFalse();
    result = DateTimeUtils.isWithinPeriod(two, four, one, three);
    assertThat(result).isFalse();
  }

  @Test
  void dateTime_WithZone_IsOffsetAndFormatted() {
    ZonedDateTime givenDateTime = ZonedDateTime.now();
    TimeZone givenZone = TimeZone.getTimeZone(ZoneId.of("Europe/Paris"));
    String givenZoneString = givenZone.getID();

    String expected =
        givenDateTime.toString().substring(0, 19)
            + " "
            + TimeZoneEnum.EUROPE_PARIS.getStdAbbreviation();

    String result =
        DateTimeUtils.offsetAndFormat(
            givenDateTime, givenZoneString, DateTimeUtils.ISO_8601_FORMATTER);

    assertThat(result).isEqualTo(expected);

    result =
        DateTimeUtils.offsetAndFormat(givenDateTime, givenZone, DateTimeUtils.ISO_8601_FORMATTER);

    assertThat(result).isEqualTo(expected);
  }

  @Test
  void date_BoundariesToDetect_MonthStartAndEndDatesAreReturned() {
    Date given = new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(given);
    List<Date> result = DateTimeUtils.getMonthBoundaryDays(given);
    calendar.setTime(result.get(0));
    int resultStartDay = calendar.get(Calendar.DAY_OF_MONTH);
    calendar.setTime(result.get(1));
    int resultEndDay = calendar.get(Calendar.DAY_OF_MONTH);
    assertThat(resultStartDay).isEqualTo(calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
    assertThat(resultEndDay).isEqualTo(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
  }

  @Test
  void dateObjectAndFormatter_ToString_IsFormatted() {
    ZonedDateTime givenDateTime = ZonedDateTime.now();
    String expected = givenDateTime.format(DateTimeUtils.ISO_8601_FORMATTER);
    String result = DateTimeUtils.fromObject(givenDateTime, DateTimeUtils.ISO_8601_FORMATTER);
    assertThat(result).isEqualTo(expected);
    Date givenDate = new Date();
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(givenDate);
    result = DateTimeUtils.fromObject(givenDate, DateTimeUtils.ISO_8601_FORMATTER);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void zonedDateTime_DstZoneNameRequested_Returned() {
    ZonedDateTime now = ZonedDateTime.now().withZoneSameInstant(ZoneId.of("America/Los_Angeles"));
    String zoneName = DateTimeUtils.getZoneName(now, "dstAbbreviation");
    assertThat(zoneName).isEqualTo(TimeZoneEnum.AMERICA_LOS_ANGELES.getDstAbbreviation());
  }

  @Test
  void todayAndPeriod_InclusivenessAssessed_IsReturned() {
    ZonedDateTime now = ZonedDateTime.now();
    ZonedDateTime one = now.minusHours(2);
    ZonedDateTime two = now.minusHours(1);
    ZonedDateTime three = now.plusHours(1);
    ZonedDateTime four = now.plusHours(2);
    boolean result = DateTimeUtils.isTodayWithinPeriod(two, three);
    assertThat(result).isTrue();
    result = DateTimeUtils.isTodayWithinPeriod(three, four);
    assertThat(result).isFalse();
  }

  @Test
  void todayAndPeriodDates_InclusivenessAssessed_IsReturned() throws ParseException {
    ZonedDateTime now = ZonedDateTime.now();
    ZonedDateTime one = now.minusHours(2);
    Date two = DateTimeUtils.from(now.minusHours(1));
    Date three = DateTimeUtils.from(now.plusHours(1));
    Date four = DateTimeUtils.from(now.plusHours(2));
    boolean result = DateTimeUtils.isTodayWithinPeriod(two, three);
    assertThat(result).isTrue();
    result = DateTimeUtils.isTodayWithinPeriod(three, four);
    assertThat(result).isFalse();
  }
}
