package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Set;
import org.junit.jupiter.api.Test;

class PermissionUtilsTest {

  @Test
  void annotationWithExistingPermission_ComparedToAllPermission_Passes() {
    // Given:

    Set<String> givenPermissions =
        Set.of(
            "application.resource.action1",
            "application.resource.action2",
            "application.resource.action3");

    String[] requiredPermissions = {"application.resource.action2"};
    Permissions permissionsAnnotation = getPermissionsAnnotationInstance(requiredPermissions);

    // When:

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, permissionsAnnotation, true);

    // Then:

    assertThat(missingPermissions).isEmpty();
  }

  @Test
  void methodWithExistentPermission_ComparedToAllPermission_Throws() throws NoSuchMethodException {
    // Given:

    Set<String> givenPermissions =
        Set.of(
            "application.resource.action1",
            "application.resource.action2",
            "application.resource.action3");

    Method testMethod = PermissionUtilsTest.class.getMethod("testMethod");


    // When:

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, testMethod, true);

    // Then:

    assertThat(missingPermissions).isEmpty();
  }

  @Test
  void annotationWithNonExistentPermission_ComparedToAllPermission_Throws() {
    // Given:
    Set<String> givenPermissions =
        Set.of("application.resource.action1", "application.resource.action2");

    String[] requiredPermissions = {"application.resource.action3"};
    Permissions permissionsAnnotation = getPermissionsAnnotationInstance(requiredPermissions);

    // When:
    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, permissionsAnnotation, true);

    // Then:
    assertThat(missingPermissions).contains("application.resource.action3");
  }

  @Test
  void methodWithNonExistentPermission_ComparedToAllPermission_Throws()
      throws NoSuchMethodException {
    // Given:
    Set<String> givenPermissions =
        Set.of("application.resource.action1", "application.resource.action2");

    Method testMethod = PermissionUtilsTest.class.getMethod("testMethod");

    // When:
    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, testMethod, true);

    // Then:
    assertThat(missingPermissions).contains("application.resource.action3");
  }

  Permissions getPermissionsAnnotationInstance(String[] keys) {
    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return keys;
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    return annotation;
  }

  @Permissions(keys = {"application.resource.action3"})
  public void testMethod(){
  }
}
