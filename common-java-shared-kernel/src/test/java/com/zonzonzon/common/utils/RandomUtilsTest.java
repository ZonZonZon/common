package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class RandomUtilsTest {

  @Test
  void enum_RandomValue_IsGenerated() {
    TestEnum caseA = RandomUtils.randomEnum(TestEnum.class);
    TestEnum caseB = RandomUtils.randomEnum(TestEnum.class);
    TestEnum caseC = RandomUtils.randomEnum(TestEnum.class);
    TestEnum caseD = RandomUtils.randomEnum(TestEnum.class);
    TestEnum caseE = RandomUtils.randomEnum(TestEnum.class);
    assertThat(caseA != caseB || caseA != caseC || caseA != caseD || caseA != caseE).isTrue();
  }

  @Test
  void double_RandomValue_IsGenerated() {
    double caseA = RandomUtils.randomDouble(5, 10);
    double caseB = RandomUtils.randomDouble(5, 10);
    double caseC = RandomUtils.randomDouble(5, 10);
    double caseD = RandomUtils.randomDouble(5, 10);
    double caseE = RandomUtils.randomDouble(5, 10);
    assertThat(caseA).isLessThan(11).isGreaterThan(4);
    assertThat(caseB).isLessThan(11).isGreaterThan(4);
    assertThat(caseC).isLessThan(11).isGreaterThan(4);
    assertThat(caseD).isLessThan(11).isGreaterThan(4);
    assertThat(caseE).isLessThan(11).isGreaterThan(4);
    assertThat(caseA != caseB || caseA != caseC || caseA != caseD || caseA != caseE).isTrue();
  }

  @Test
  void float_RandomValue_IsGenerated() {
    double caseA = RandomUtils.randomFloat(5, 10);
    double caseB = RandomUtils.randomFloat(5, 10);
    double caseC = RandomUtils.randomFloat(5, 10);
    double caseD = RandomUtils.randomFloat(5, 10);
    double caseE = RandomUtils.randomFloat(5, 10);
    assertThat(caseA).isLessThan(11).isGreaterThan(4);
    assertThat(caseB).isLessThan(11).isGreaterThan(4);
    assertThat(caseC).isLessThan(11).isGreaterThan(4);
    assertThat(caseD).isLessThan(11).isGreaterThan(4);
    assertThat(caseE).isLessThan(11).isGreaterThan(4);
    assertThat(caseA != caseB || caseA != caseC || caseA != caseD || caseA != caseE).isTrue();
  }

  @Test
  void integer_RandomValue_IsGenerated() {
    double caseA = RandomUtils.randomInteger(5, 10);
    double caseB = RandomUtils.randomInteger(5, 10);
    double caseC = RandomUtils.randomInteger(5, 10);
    double caseD = RandomUtils.randomInteger(5, 10);
    double caseE = RandomUtils.randomInteger(5, 10);
    assertThat(caseA).isLessThan(11).isGreaterThan(4);
    assertThat(caseB).isLessThan(11).isGreaterThan(4);
    assertThat(caseC).isLessThan(11).isGreaterThan(4);
    assertThat(caseD).isLessThan(11).isGreaterThan(4);
    assertThat(caseE).isLessThan(11).isGreaterThan(4);
    assertThat(caseA != caseB || caseA != caseC || caseA != caseD || caseA != caseE).isTrue();
  }

  @Test
  void boolean_RandomValue_IsGenerated() {
    boolean caseA = RandomUtils.randomBoolean();
    boolean caseB = RandomUtils.randomBoolean();
    boolean caseC = RandomUtils.randomBoolean();
    boolean caseD = RandomUtils.randomBoolean();
    boolean caseE = RandomUtils.randomBoolean();
    assertThat(!caseA || !caseB || !caseC || !caseD || !caseE).isTrue();
  }

  @Test
  void string_RandomValue_IsGenerated() {
    String caseA = RandomUtils.randomString(5, 10);
    String caseB = RandomUtils.randomString(5, 10);
    String caseC = RandomUtils.randomString(5, 10);
    String caseD = RandomUtils.randomString(5, 10);
    String caseE = RandomUtils.randomString(5, 10);
    assertThat(caseA.length()).isLessThan(11).isGreaterThan(4);
    assertThat(caseB.length()).isLessThan(11).isGreaterThan(4);
    assertThat(caseC.length()).isLessThan(11).isGreaterThan(4);
    assertThat(caseD.length()).isLessThan(11).isGreaterThan(4);
    assertThat(caseE.length()).isLessThan(11).isGreaterThan(4);
    assertThat(
        !caseA.equals(caseB)
            || !caseA.equals(caseC)
            || !caseA.equals(caseD)
            || !caseA.equals(caseE))
        .isTrue();
  }

  @Test
  void list_RandomSelection_IsGenerated() {
    List<Object> aList = List.of("one", "two", "three", "four", "five");
    String caseA = (String) RandomUtils.randomOfList(aList);
    String caseB = (String) RandomUtils.randomOfList(aList);
    String caseC = (String) RandomUtils.randomOfList(aList);
    String caseD = (String) RandomUtils.randomOfList(aList);
    String caseE = (String) RandomUtils.randomOfList(aList);
    assertThat(aList).contains(caseA);
    assertThat(aList).contains(caseB);
    assertThat(aList).contains(caseC);
    assertThat(aList).contains(caseD);
    assertThat(aList).contains(caseE);
    assertThat(
        !caseA.equals(caseB)
            || !caseA.equals(caseC)
            || !caseA.equals(caseD)
            || !caseA.equals(caseE))
        .isTrue();
  }

  @Test
  void dateTime_RandomSelection_IsGenerated() {
    ZonedDateTime caseA = RandomUtils.randomDateTime();
    ZonedDateTime caseB = RandomUtils.randomDateTime();
    ZonedDateTime caseC = RandomUtils.randomDateTime();
    ZonedDateTime caseD = RandomUtils.randomDateTime();
    ZonedDateTime caseE = RandomUtils.randomDateTime();
    assertThat(
        !caseA.equals(caseB)
            || !caseA.equals(caseC)
            || !caseA.equals(caseD)
            || !caseA.equals(caseE))
        .isTrue();
  }

  @Test
  void uuid_RandomSelection_IsGenerated() {
    Set<UUID> caseA = RandomUtils.randomUuids(5,10);
    Set<UUID> caseB = RandomUtils.randomUuids(5,10);
    Set<UUID> caseC = RandomUtils.randomUuids(5,10);
    Set<UUID> caseD = RandomUtils.randomUuids(5,10);
    Set<UUID> caseE = RandomUtils.randomUuids(5,10);
    assertThat(caseA).size().isGreaterThan(4).isLessThan(11);
    assertThat(caseB).size().isGreaterThan(4).isLessThan(11);
    assertThat(caseC).size().isGreaterThan(4).isLessThan(11);
    assertThat(caseD).size().isGreaterThan(4).isLessThan(11);
    assertThat(caseE).size().isGreaterThan(4).isLessThan(11);
    assertThat(
        caseA.size() != caseB.size()
            || caseA.size() != caseC.size()
            || caseA.size() != caseD.size()
            || caseA.size() != caseE.size())
        .isTrue();
  }

  private enum TestEnum {
    one,
    two,
    three,
    four,
    five
  }
}
