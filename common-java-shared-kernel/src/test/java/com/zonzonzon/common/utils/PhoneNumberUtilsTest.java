package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class PhoneNumberUtilsTest {

  @Test
  void phoneNumber_Valid_IsConsideredValid() {
    String given = "2018577757";
    boolean expected = true;
    boolean result = PhoneNumberUtils.isValid(given);
    assertThat(result).isEqualTo(expected);

    given = "12018577757";
    expected = false;
    result = PhoneNumberUtils.isValid(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void phoneNumber_ToDatabaseFormat_IsConverted() {
    String given = "(201) 857-7757";
    String expected = "2018577757";
    String result = PhoneNumberUtils.toDatabaseFormat(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void phoneNumber_FormatToApply_IsFormatted() {
    String given = "(201)8577757";
    String expected = "(201) 857-7757";
    String result = PhoneNumberUtils.format(given, null);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void phoneNumber_HyphensFormatToApply_IsFormatted() {
    String given = "(201)8577757";
    String expected = "201-857-7757";
    String result = PhoneNumberUtils.formatToHyphens(given);
    assertThat(result).isEqualTo(expected);
  }
}
