package com.zonzonzon.common.utils;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import org.junit.jupiter.api.Test;

class LambdaUtilsTest {

  @Test
  void double_ToTwoDecimals_IsRounded() {
    assertThrows(
        IOException.class,
        () ->
            LambdaUtils.sneakyThrow(
                parameters -> testFunction((String) parameters.get(0), (Integer) parameters.get(1)),
                "value",
                1));

    assertThrows(
        IOException.class,
        () ->
            LambdaUtils.sneakyThrowVoid(
                parameters -> testVoidFunction((String) parameters.get(0), (Integer) parameters.get(1)),
                "value",
                1));
  }

  Object testFunction(String text, int number) throws IOException {
    throw new IOException();
  }

  void testVoidFunction(String text, int number) throws IOException {
    throw new IOException();
  }
}
