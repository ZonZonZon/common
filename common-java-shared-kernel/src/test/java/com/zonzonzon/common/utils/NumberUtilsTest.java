package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class NumberUtilsTest {

  @Test
  void double_ToTwoDecimals_IsRounded() {
    double given = 1234567.1234567;
    double expected = 1234567.12;
    double result = NumberUtils.roundTo2Decimals(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void double_ToOneDecimal_IsRounded() {
    double given = 1234567.1234567;
    double expected = 1234567.1;
    double result = NumberUtils.roundTo1Decimal(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void double_RanksAndThousands_AreFormatted() {
    String result = NumberUtils.getReadableFormat(1123456789012.123);
    assertThat(result).isEqualTo("1,12T");
    result = NumberUtils.getReadableFormat(1123456789012L);
    assertThat(result).isEqualTo("1,12T");
    result = NumberUtils.getReadableFormat(1123456789.123);
    assertThat(result).isEqualTo("1,12B");
    result = NumberUtils.getReadableFormat(1123456789L);
    assertThat(result).isEqualTo("1,12B");
    result = NumberUtils.getReadableFormat(1123456.123);
    assertThat(result).isEqualTo("1,12M");
    result = NumberUtils.getReadableFormat(1123456L);
    assertThat(result).isEqualTo("1,12M");
    result = NumberUtils.getReadableFormat(1123.123);
    assertThat(result).isEqualTo("1,12K");
    result = NumberUtils.getReadableFormat(1123L);
    assertThat(result).isEqualTo("1,12K");
  }

  @Test
  void double_percentageRangeIsAssessed_IsEvaluated() {
    double result = NumberUtils.normalizePercentage(123.123);
    assertThat(result).isEqualTo(0.00);
    result = NumberUtils.normalizePercentage(99.123);
    assertThat(result).isEqualTo(99.12);
  }

  @Test
  void double_ThousandsAreCommaSeparated_IsFormatted() {
    String result = NumberUtils.separateThousands(1234567890L, " ");
    assertThat(result).isEqualTo("1 234 567 890");
    result = NumberUtils.separateThousands(1234567890L, ",");
    assertThat(result).isEqualTo("1,234,567,890");
  }

  @Test
  void valueAndMargin_MarginIsConverted_AsPercentage() {
    Float result = NumberUtils.getMarginAsPercentage(999.00, 123);
    assertThat(result).isEqualTo(12.31F);
  }
}
