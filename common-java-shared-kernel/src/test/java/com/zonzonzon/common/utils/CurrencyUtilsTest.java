package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import com.neovisionaries.i18n.CurrencyCode;
import java.util.Locale;
import org.junit.jupiter.api.Test;

class CurrencyUtilsTest {

  @Test
  void currencyValue_InSomeLocale_IsFormatted() {
    String formattedCurrency = CurrencyUtils.getFormattedCurrency(CurrencyCode.USD, Locale.US, 123);
    assertThat(formattedCurrency).isEqualTo("$123.00");
  }
}
