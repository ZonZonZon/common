package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.UUID;
import org.junit.jupiter.api.Test;

class UuidUtilsTest {

  @Test
  void byteArray_ToUuid_IsConverted() {
    byte[] given = new byte[] {-63, -118, -12, -8, 45, 61, 64, 28, -97, 14, 102, 2, 93, -94, -109, 68};
    UUID expected = UUID.fromString("C18AF4F8-2D3D-401C-9F0E-66025DA29344");
    UUID result = UuidUtils.from(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void uuid_ToByteArray_IsConverted() {
    UUID given = UUID.fromString("C18AF4F8-2D3D-401C-9F0E-66025DA29344");
    byte[] expected = new byte[] {-63, -118, -12, -8, 45, 61, 64, 28, -97, 14, 102, 2, 93, -94, -109, 68};
    byte[] result = UuidUtils.from(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void uuid_ToStringWithoutHyphens_IsConverted() {
    UUID given = UUID.fromString("C18AF4F8-2D3D-401C-9F0E-66025DA29344");
    String expected = "c18af4f82d3d401c9f0e66025da29344";
    String result = UuidUtils.toStringWithoutHyphens(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void stringWithoutHyphens_ToUuid_IsConverted() {
    String given = "c18af4f82d3d401c9f0e66025da29344";
    UUID expected = UUID.fromString("C18AF4F8-2D3D-401C-9F0E-66025DA29344");
    UUID result = UuidUtils.fromStringWithoutHyphens(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void stringWithHyphens_ToUuid_IsConverted() {
    String given = "c18af4f8-2d3d-401c-9f0e-66025da29344";
    UUID expected = UUID.fromString(given);
    UUID result = UuidUtils.fromStringWithHyphens(given);
    assertThat(result).isEqualTo(expected);
  }

  @Test
  void uuidString_WithoutHyphens_ReturnsUuidObject() {
    String given = "C18AF4F82D3D401C9F0E66025DA29344";
    UUID expected = UUID.fromString("C18AF4F8-2D3D-401C-9F0E-66025DA29344");
    UUID result = UuidUtils.fromStringWithoutHyphens(given);
    assertThat(result).isEqualTo(expected);
  }
}
