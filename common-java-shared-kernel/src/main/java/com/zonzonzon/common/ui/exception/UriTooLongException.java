package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The URI requested by the client is longer than the server is willing to interpret.
 */
@Slf4j
public class UriTooLongException extends RuntimeException implements HttpExceptionResponse {

	public static final int HTTP_STATUS_CODE = 414;

	public UriTooLongException(String message) {
		super(message);
		log.warn(message);
	}

	@Override
	public int getStatus() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public String getCode() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
