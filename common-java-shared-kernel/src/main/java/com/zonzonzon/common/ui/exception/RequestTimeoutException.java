package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * This response is sent on an idle connection by some servers, even without any previous request by
 * the client. It means that the server would like to shut down this unused connection. This
 * response is used much more since some browsers, like Chrome, Firefox 27+, or IE9, use HTTP
 * pre-connection mechanisms to speed up surfing. Also note that some servers merely shut down the
 * connection without sending this message.
 */
@Slf4j
public class RequestTimeoutException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 408;

  public RequestTimeoutException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
