package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * Indicates that the server is unwilling to risk processing a request that might be replayed.
 */
@Slf4j
public class TooEarlyException extends RuntimeException implements HttpExceptionResponse {

	public static final int HTTP_STATUS_CODE = 425;

	public TooEarlyException(String message) {
		super(message);
		log.warn(message);
	}

	@Override
	public int getStatus() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public String getCode() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
