package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The origin server requires the request to be conditional. This response is intended to prevent
 * the 'lost update' problem, where a client GETs a resource's state, modifies it and PUTs it back
 * to the server, when meanwhile a third party has modified the state on the server, leading to a
 * conflict.
 */
@Slf4j
public class PreconditionRequiredException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 428;

  public PreconditionRequiredException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
