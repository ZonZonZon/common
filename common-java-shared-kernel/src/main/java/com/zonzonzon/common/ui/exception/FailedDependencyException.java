package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The request failed due to failure of a previous request.
 */
@Slf4j
public class FailedDependencyException extends RuntimeException implements HttpExceptionResponse {

	public static final int HTTP_STATUS_CODE = 424;

	public FailedDependencyException(String message) {
		super(message);
		log.warn(message);
	}

	@Override
	public int getStatus() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public String getCode() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
