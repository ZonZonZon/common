package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The server is unwilling to process the request because its header fields are too large. The
 * request may be resubmitted after reducing the size of the request header fields.
 */
@Slf4j
public class RequestHeaderFieldsTooLargeException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 431;

  public RequestHeaderFieldsTooLargeException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
