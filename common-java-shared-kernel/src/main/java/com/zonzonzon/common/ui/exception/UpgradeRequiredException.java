package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The server refuses to perform the request using the current protocol but might be willing to do
 * so after the client upgrades to a different protocol. The server sends an Upgrade header in a 426
 * response to indicate the required protocol(s).
 */
@Slf4j
public class UpgradeRequiredException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 426;

  public UpgradeRequiredException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
