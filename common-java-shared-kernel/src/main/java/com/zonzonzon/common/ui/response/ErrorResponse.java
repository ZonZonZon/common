package com.zonzonzon.common.ui.response;

import com.zonzonzon.common.ui.exception.HttpExceptionResponse;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A structure of one exception returned in a REST response.
 */
@Data
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse implements HttpExceptionResponse {

  private int status;
  private String code;
  private String title;
  private String message;
  private String description;
  private List<Violation> violations;
}
