package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/** The request was well-formed but was unable to be followed due to semantic errors. */
@Slf4j
public class UnprocessableEntityException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 422;

  public UnprocessableEntityException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
