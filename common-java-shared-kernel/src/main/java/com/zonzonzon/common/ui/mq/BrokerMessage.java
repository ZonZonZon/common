package com.zonzonzon.common.ui.mq;

import java.time.ZonedDateTime;
import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * A message structure for Message Queue Broker Server. Wraps some payload (event) with additional
 * technical information.
 */
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BrokerMessage<P> {

  /** Identifier of MQ-message. */
  @NotNull UUID id;

  /** Producer-application name (bounded context) with path prefix. */
  @NotNull String application;

  /** Resource name. */
  @NotNull String resource;

  /** Action (command, event or aggregate snapshot) name. */
  @NotNull String action;

  /**
   * Resource model version. Is used to consider backward compatibility issues. Enables blue/green
   * or canary deployments.
   */
  @NotNull Long version;

  /** Identifier of application that has sent the message. Authorizes the requester. */
  @NotNull UUID sourceId;

  /**
   * Identifier of one trace sequentially distributed between different applications within a
   * system. Helps to trace it in the logs.
   */
  @NotNull UUID traceId;

  /** A user token to sign the message for authorization. */
  @NotNull String token;

  /** Timestamp with zone when the message was sent. */
  @NotNull ZonedDateTime timestamp;

  /** An object (aggregate snapshot, event, command) of any structure. */
  @NotNull P payload;

  /** Name of a topic in MQ broker or some other signature of where to send the response message. */
  @NotNull String responseChannel;

  /**
   * Correlate response messages with request messages when an application invokes a
   * request-response operation.
   */
  @NotNull UUID correlationId;
}
