package com.zonzonzon.common.ui.response;

import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * A common response for ACCEPTED HTTP-status. Returns identifier of the resource accepted for
 * modifications.
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class AcceptedResponse {
  UUID id;
}
