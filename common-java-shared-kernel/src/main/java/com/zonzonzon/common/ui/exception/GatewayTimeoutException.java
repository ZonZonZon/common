package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * This error response is given when the server is acting as a gateway and cannot get a response in
 * time.
 */
@Slf4j
public class GatewayTimeoutException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 504;

  public GatewayTimeoutException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
