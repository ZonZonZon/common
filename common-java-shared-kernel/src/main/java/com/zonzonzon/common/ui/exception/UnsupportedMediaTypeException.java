package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The media format of the requested data is not supported by the server, so the server is rejecting
 * the request.
 */
@Slf4j
public class UnsupportedMediaTypeException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 415;

  public UnsupportedMediaTypeException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
