package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * Although the HTTP standard specifies "unauthorized", semantically this response means
 * "unauthenticated". That is, the client must authenticate itself to get the requested response.
 */
@Slf4j
public class UnauthorizedException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 401;

  public UnauthorizedException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
