package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/** The client has indicated preconditions in its headers which the server does not meet. */
@Slf4j
public class PreconditionFailedException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 412;

  public PreconditionFailedException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
