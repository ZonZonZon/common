package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The request method is not supported by the server and cannot be handled. The only methods that
 * servers are required to support (and therefore that must not return this code) are GET and HEAD.
 */
@Slf4j
public class NotImplementedException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 501;

  public NotImplementedException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
