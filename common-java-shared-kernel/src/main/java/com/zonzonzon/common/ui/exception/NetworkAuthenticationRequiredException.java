package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/** Indicates that the client needs to authenticate to gain network access. */
@Slf4j
public class NetworkAuthenticationRequiredException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 511;

  public NetworkAuthenticationRequiredException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
