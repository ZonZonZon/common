package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * This response is sent when a request conflicts with the current state of the server.
 */
@Slf4j
public class ConflictException extends RuntimeException implements HttpExceptionResponse {

	public static final int HTTP_STATUS_CODE = 409;

	public ConflictException(String message) {
		super(message);
		log.warn(message);
	}

	@Override
	public int getStatus() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public String getCode() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
