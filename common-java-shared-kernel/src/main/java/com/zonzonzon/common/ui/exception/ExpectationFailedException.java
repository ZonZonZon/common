package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * This response code means the expectation indicated by the Expect request header field cannot be
 * met by the server.
 */
@Slf4j
public class ExpectationFailedException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 417;

  public ExpectationFailedException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
