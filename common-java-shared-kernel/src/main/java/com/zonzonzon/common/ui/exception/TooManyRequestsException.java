package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The user has sent too many requests in a given amount of time ("rate limiting").
 */
@Slf4j
public class TooManyRequestsException extends RuntimeException implements HttpExceptionResponse {

	public static final int HTTP_STATUS_CODE = 429;

	public TooManyRequestsException(String message) {
		super(message);
		log.warn(message);
	}

	@Override
	public int getStatus() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public String getCode() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
