package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * This response is sent when business logic is violated. Inherit from this exception to create
 * specific business exceptions.
 */
@Slf4j
public class BusinessException extends ConflictException implements HttpExceptionResponse {

  public static final String CODE = "BUSINESS_EXCEPTION";
  private String title;
  private String description;

  public BusinessException(String message) {
    super(message);
    log.warn(message);
  }

  public BusinessException(String title, String message) {
    super(message);
    this.title = title;
    log.warn(message);
  }

  public BusinessException(String title, String message, String description) {
    super(message);
    this.title = title;
    this.description = description;
    log.warn(message);
  }

  @Override
  public String getCode() {
    return CODE;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String getDescription() {
    return description;
  }
}
