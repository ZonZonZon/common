package com.zonzonzon.common.ui.exception;

public interface HttpExceptionResponse {

  /** An HTTP exception status. */
  int getStatus();

  /**
   * A human-readable code formatted uppercase and snake-case. Helps to distinguish between
   * responses with the same HTTP status code.
   */
  String getCode();

  /** A short title for an exception. */
  String getTitle();

  /** A message for users. */
  String getMessage();

  /** Exception details or support URL link. */
  String getDescription();
}
