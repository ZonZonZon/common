package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * Request entity is larger than limits defined by server. The server might close the connection or
 * return an Retry-After header field.
 */
@Slf4j
public class PayloadTooLargeException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 413;

  public PayloadTooLargeException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
