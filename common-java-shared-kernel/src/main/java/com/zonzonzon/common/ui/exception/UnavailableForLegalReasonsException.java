package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The user agent requested a resource that cannot legally be provided, such as a web page censored
 * by a government.
 */
@Slf4j
public class UnavailableForLegalReasonsException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 451;

  public UnavailableForLegalReasonsException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
