package com.zonzonzon.common.ui.response;

import lombok.Builder;
import lombok.Getter;

/** A failed validation structure. */
@Builder
@Getter
public class Violation {
  String field;
  String message;
}
