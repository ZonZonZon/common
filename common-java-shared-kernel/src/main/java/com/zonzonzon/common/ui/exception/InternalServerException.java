package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * Exception for unexpected condition that prevented server from fulfilling the request.
 */
@Slf4j
public class InternalServerException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 500;

  public InternalServerException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
