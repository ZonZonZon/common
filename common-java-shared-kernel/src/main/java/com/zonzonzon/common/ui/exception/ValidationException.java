package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/** This response is sent when request validation has failed. */
@Slf4j
public class ValidationException extends BadRequestException implements HttpExceptionResponse {

  public static final String CODE = "VALIDATION_FAILED";
  private String title;
  private String description;

  public ValidationException(String message) {
    super(message);
    log.warn(message);
  }

  public ValidationException(String title, String message) {
    super(message);
    this.title = title;
    log.warn(message);
  }

  public ValidationException(String title, String message, String description) {
    super(message);
    this.title = title;
    this.description = description;
    log.warn(message);
  }

  @Override
  public String getCode() {
    return CODE;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String getDescription() {
    return description;
  }
}
