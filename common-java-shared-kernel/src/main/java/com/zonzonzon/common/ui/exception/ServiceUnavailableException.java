package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The server is not ready to handle the request. Common causes are a server that is down for
 * maintenance or that is overloaded. Note that together with this response, a user-friendly page
 * explaining the problem should be sent. This response should be used for temporary conditions and
 * the Retry-After HTTP header should, if possible, contain the estimated time before the recovery
 * of the service. The webmaster must also take care about the caching-related headers that are sent
 * along with this response, as these temporary condition responses should usually not be cached.
 */
@Slf4j
public class ServiceUnavailableException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 503;

  public ServiceUnavailableException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
