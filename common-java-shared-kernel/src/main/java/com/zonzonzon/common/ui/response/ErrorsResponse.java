package com.zonzonzon.common.ui.response;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** A structure of a REST response with error or exception.
 * */
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorsResponse {

  /**
   * A list of errors. List is required for cases of form validation where order and differentiation
   * of errors for various fields is required in one response.
   */
  private List<ErrorResponse> errors;
}
