package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/** This response is sent when an unhandled exception is thrown. */
@Slf4j
public class UnhandledException extends InternalServerException implements HttpExceptionResponse {

  public static final String CODE = "UNHANDLED_EXCEPTION";
  private String title;
  private String description;

  public UnhandledException(String message) {
    super(message);
    log.warn(message);
  }

  public UnhandledException(String title, String message) {
    super(message);
    this.title = title;
    log.warn(message);
  }

  public UnhandledException(String title, String message, String description) {
    super(message);
    this.title = title;
    this.description = description;
    log.warn(message);
  }

  @Override
  public String getCode() {
    return CODE;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String getDescription() {
    return description;
  }
}
