package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The server can not find the requested resource. In the browser, this means the URL is not
 * recognized. In an API, this can also mean that the endpoint is valid but the resource itself does
 * not exist. Servers may also send this response instead of 403 Forbidden to hide the existence of
 * a resource from an unauthorized client. This response code is probably the most well known due to
 * its frequent occurrence on the web.
 */
@Slf4j
public class NotFoundException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 404;

  public NotFoundException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
