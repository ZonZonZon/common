package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The bandwidth which was available to you by your web hosting plan is exceeded.
 */
@Slf4j
public class BandwidthLimitExceededException extends RuntimeException implements HttpExceptionResponse {

	public static final int HTTP_STATUS_CODE = 509;

	public BandwidthLimitExceededException(String message) {
		super(message);
		log.warn(message);
	}

	@Override
	public int getStatus() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public String getCode() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
