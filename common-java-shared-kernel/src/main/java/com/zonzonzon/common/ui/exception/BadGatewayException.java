package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * This error response means that the server, while working as a gateway to get a response needed to
 * handle the request, got an invalid response.
 */
@Slf4j
public class BadGatewayException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 502;

  public BadGatewayException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
