package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The request method is known by the server but is not supported by the target resource. For
 * example, an API may not allow calling DELETE to remove a resource.
 */
@Slf4j
public class MethodNotAllowedException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 405;

  public MethodNotAllowedException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
