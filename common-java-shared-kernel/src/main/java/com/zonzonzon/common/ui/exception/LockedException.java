package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The resource that is being accessed is locked.
 */
@Slf4j
public class LockedException extends RuntimeException implements HttpExceptionResponse {

	public static final int HTTP_STATUS_CODE = 423;

	public LockedException(String message) {
		super(message);
		log.warn(message);
	}

	@Override
	public int getStatus() {
		return HTTP_STATUS_CODE;
	}

	@Override
	public String getCode() {
		return null;
	}

	@Override
	public String getTitle() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}
}
