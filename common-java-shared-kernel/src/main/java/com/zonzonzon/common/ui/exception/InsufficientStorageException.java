package com.zonzonzon.common.ui.exception;

import lombok.extern.slf4j.Slf4j;

/**
 * The method could not be performed on the resource because the server is unable to store the
 * representation needed to successfully complete the request.
 */
@Slf4j
public class InsufficientStorageException extends RuntimeException implements HttpExceptionResponse {

  public static final int HTTP_STATUS_CODE = 507;

  public InsufficientStorageException(String message) {
    super(message);
    log.warn(message);
  }

  @Override
  public int getStatus() {
    return HTTP_STATUS_CODE;
  }

  @Override
  public String getCode() {
    return null;
  }

  @Override
  public String getTitle() {
    return null;
  }

  @Override
  public String getDescription() {
    return null;
  }
}
