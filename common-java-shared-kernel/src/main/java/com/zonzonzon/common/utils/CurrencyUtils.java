package com.zonzonzon.common.utils;

import com.neovisionaries.i18n.CurrencyCode;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

public class CurrencyUtils {

  /** Formats a currency value based on currency type and locale. */
  public static String getFormattedCurrency(
      CurrencyCode currencyCode, Locale locale, int value) {

    Currency currency = Currency.getInstance(currencyCode.name());
    NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
    numberFormat.setCurrency(currency);
    String formatted = numberFormat.format(value);
    return formatted;
  }
}
