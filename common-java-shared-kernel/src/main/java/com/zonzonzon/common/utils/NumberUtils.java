package com.zonzonzon.common.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberUtils {

  private static final String DECIMAL_FORMAT_PATTERN = "###,###,###,###";
  private static final DecimalFormat NUMBER_FORMAT = new DecimalFormat(DECIMAL_FORMAT_PATTERN);
  private static final DecimalFormat PERCENTAGE_FORMAT = new DecimalFormat("###.##");
  private static final String DECIMAL_FORMAT = "###.##";

  public static double roundTo2Decimals(double value) {
    DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.US);
    DecimalFormat formatter = new DecimalFormat(DECIMAL_FORMAT, symbols);
    String formatted = formatter.format(value);
    return Double.parseDouble(formatted);
  }

  public static double roundTo1Decimal(double value) {
    return BigDecimal.valueOf(value).setScale(1, RoundingMode.HALF_UP).doubleValue();
  }

  /** Formats value with rank separators, thousands, millions and billions replacements. */
  public static String getReadableFormat(Long value) {
    if (value >= 1000000000000L) {
      return String.format("%.2fT", value / 1000000000000.0);
    }
    if (value >= 1000000000L) {
      return String.format("%.2fB", value / 1000000000.0);
    }
    if (value >= 1000000L) {
      return String.format("%.2fM", value / 1000000.0);
    }
    if (value >= 1000L) {
      return String.format("%.2fK", value / 1000.0);
    }
    return NUMBER_FORMAT.format(value);
  }

  /** An alias for {@link NumberUtils#getReadableFormat(Long)} */
  public static String getReadableFormat(Double value) {
    return getReadableFormat(Math.round(value));
  }

  /** Lead percentage to a range of 0 - 100. Consider all values outside this as zero. */
  public static Double normalizePercentage(Double value) {
    double normalizedDouble = value != null && value >= 0.00 && value <= 100.00 ? value : 0.00;
    return roundTo2Decimals(normalizedDouble);
  }

  /** Formats number to text with thousand-ranks comma separated. */
  public static String separateThousands(Long value, String thousandsSeparator) {
    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.getDefault());
    DecimalFormatSymbols symbols = NUMBER_FORMAT.getDecimalFormatSymbols();
    symbols.setGroupingSeparator(thousandsSeparator.charAt(0));
    formatter.setDecimalFormatSymbols(symbols);
    return formatter.format(value);
  }

  public static Float getMarginAsPercentage(double value, double margin) {
    if (value == 0.0) {
      return (float) 0.0;
    }
    float percentage = (float) (Math.round((margin / value * 100) * 100.0) / 100.0);
    return percentage;
  }
}
