package com.zonzonzon.common.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Matching singular and plural noun forms. Is used in reflections to find field and method names.
 */
@AllArgsConstructor
@Getter
public enum NounFormsEnum {

  ACTION("action", "actions"),
  APPLICATION("application", "applications"),
  RESOURCE("resource", "resources");

  public final String singular;
  public final String plural;
}
