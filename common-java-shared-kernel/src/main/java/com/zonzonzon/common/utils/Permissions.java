package com.zonzonzon.common.utils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotate methods that require token validation and authorization with permission. Such methods
 * require String token parameter be passed.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Permissions {

  String[] keys();
}
