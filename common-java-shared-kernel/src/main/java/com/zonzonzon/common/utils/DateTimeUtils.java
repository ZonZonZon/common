package com.zonzonzon.common.utils;

import static com.zonzonzon.common.utils.StringUtils.SPACE;
import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.format.DateTimeFormatter.ISO_ZONED_DATE_TIME;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.TextStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class DateTimeUtils {

  public static final DateTimeFormatter DATABASE_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

  public static final String ISO_8601_PATTERN_WITHOUT_ZONE = "yyyy-MM-dd'T'HH:mm:ss";

  public static final DateTimeFormatter ISO_8601_FORMATTER =
      DateTimeFormatter.ofPattern(ISO_8601_PATTERN_WITHOUT_ZONE);

  public static final DateTimeFormatter AM_PM_ZONE_FORMATTER =
      DateTimeFormatter.ofPattern("MMM dd, yyyy, hh:mm a");

  private static DateTimeFormatter DATE_FORMATTER = ISO_LOCAL_DATE;
  private static SimpleDateFormat SIMPLE_DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

  /**
   * Formats given date and time in a given formatter.
   *
   * @param dateTime Any date time string.
   * @param formatter Formatter matching dateTime string.
   * @return Date object in a system zone.
   * @throws ParseException
   */
  public static Date fromInSystemDefaultZone(String dateTime, DateTimeFormatter formatter)
      throws ParseException {

    ZonedDateTime zonedDateTime =
        ZonedDateTime.parse(dateTime, formatter.withZone(ZoneId.systemDefault()));

    return from(zonedDateTime);
  }

  public static String formatAsUtc(Date date, DateTimeFormatter formatter) {
    Instant instant = date.toInstant();
    ZoneId utc = ZoneId.of(ZoneOffset.UTC.toString());
    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, utc);
    return localDateTime.format(formatter);
  }

  /**
   * Formats a given date and time as text from one format to another.
   *
   * @param dateTime Given date and time, zone.
   * @param fromFormatter Format a dateTime is formatted by.
   * @param toFormatter An output formatter.
   * @return Output date and time value.
   */
  public static String format(
      String dateTime, DateTimeFormatter fromFormatter, DateTimeFormatter toFormatter) {

    ZoneId utc = ZoneId.of(ZoneOffset.UTC.toString());
    LocalDateTime dateTimeLocal = LocalDateTime.parse(dateTime, fromFormatter);
    ZonedDateTime zonedDateTime = ZonedDateTime.of(dateTimeLocal, utc);
    return zonedDateTime.format(toFormatter);
  }

  /**
   * Shifts given date and time by offset hours. Formats using given formatter.
   *
   * @param databaseUtcDateTime A date and time in a database formatted text, in UTC zone.
   * @param offsetInHours Hours to shift.
   * @param formatter Formatter for the output.
   * @return A shifted date and time in requested format.
   */
  public static String fromDatabaseUtcByOffsetHours(
      String databaseUtcDateTime, int offsetInHours, DateTimeFormatter formatter) {

    ZonedDateTime dateTimeAtUTC =
        ZonedDateTime.parse(databaseUtcDateTime, DATABASE_FORMATTER.withZone(ZoneId.of("UTC")));

    return fromUtcByOffsetHours(dateTimeAtUTC, offsetInHours, formatter);
  }

  public static String fromUtcByOffsetHours(
      ZonedDateTime utcDateTime, int offsetInHours, DateTimeFormatter formatter) {

    ZoneOffset offset = ZoneOffset.ofHours(offsetInHours);
    ZoneId zoneIdForOffset = ZoneId.of(offset.toString());

    LocalDateTime dateTimeAtOffset =
        LocalDateTime.ofInstant(utcDateTime.toInstant(), zoneIdForOffset);

    return dateTimeAtOffset.format(formatter);
  }

  public static String formatToDaysHourMinSec(Long millis) {
    long days = TimeUnit.MILLISECONDS.toDays(millis);
    millis -= TimeUnit.DAYS.toMillis(days);
    long hours = TimeUnit.MILLISECONDS.toHours(millis);
    millis -= TimeUnit.HOURS.toMillis(hours);
    long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
    millis -= TimeUnit.MINUTES.toMillis(minutes);
    long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

    String stringBuilder =
        days + " Days " + hours + " Hours " + minutes + " Minutes " + seconds + " Seconds";

    return stringBuilder;
  }

  public static String formatIntervalToDaysHourMinSec(ZonedDateTime from, ZonedDateTime to) {
    long fromMillis = from.toInstant().toEpochMilli();
    long toMillis = to.toInstant().toEpochMilli();
    long interval = toMillis - fromMillis;
    return formatToDaysHourMinSec(interval);
  }

  public static ZonedDateTime from(Date date) {
    if (date == null) {
      return null;
    }
    LocalDateTime localDate;
    if (date instanceof java.sql.Date) {
      localDate =
          ((java.sql.Date) date)
              .toLocalDate()
              .atTime(date.getHours(), date.getMinutes(), date.getSeconds());
    } else {
      localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
    ZonedDateTime zonedDateTime = ZonedDateTime.of(localDate, ZoneId.systemDefault());
    return zonedDateTime;
  }

  public static Date from(ZonedDateTime dateTime) throws ParseException {
    if (dateTime == null) {
      return null;
    }
    Date dateInSystemZone = Date.from(dateTime.toInstant());
    SimpleDateFormat iso8601Format = new SimpleDateFormat(ISO_8601_PATTERN_WITHOUT_ZONE);
    iso8601Format.setTimeZone(TimeZone.getTimeZone(dateTime.getZone()));
    return dateInSystemZone;
  }

  /**
   * Converts a string to zoned date and time.
   *
   * @param dateTimeAndZone Data with date, time and possibly zone.
   * @param pattern Pattern to format. If pattern is not provided, then ISO 8601 is used. If pattern
   *     has no zone, then UTC is used.
   * @return Zoned date and time. Milliseconds are cut.
   */
  public static ZonedDateTime from(String dateTimeAndZone, String pattern) {
    if (dateTimeAndZone == null) {
      return null;
    }
    ZonedDateTime zonedDateTime = null;
    if (pattern != null) {
      try {
        zonedDateTime = ZonedDateTime.parse(dateTimeAndZone, DateTimeFormatter.ofPattern(pattern));

      } catch (Exception retryWithDefaultZone) {

        zonedDateTime =
            LocalDateTime.parse(dateTimeAndZone, DateTimeFormatter.ofPattern(pattern))
                .atZone(ZoneId.of("UTC"));
      }
    } else {
      ZonedDateTime.from(DateTimeFormatter.ISO_ZONED_DATE_TIME.parse(dateTimeAndZone));
    }
    return zonedDateTime;
  }

  /**
   * Converts an ISO 8601 date-time-zone like 2022-12-03T11:15:29+01:00[Europe/Paris] into UTC zone.
   * Shifts to UTC zone.
   */
  public static String toIso8601UtcFromIso8601Formatted(String zonedDateTime) {
    return ZonedDateTime.parse(zonedDateTime).withZoneSameInstant(ZoneOffset.UTC).toString();
  }

  /** Moves zoned date and time to a given time zone. */
  public static ZonedDateTime toAnotherZone(ZonedDateTime zonedDateTime, TimeZone timeZone) {
    return zonedDateTime.withZoneSameInstant(timeZone.toZoneId());
  }

  /**
   * Moves zoned date and time to a given time zone and converts to an ISO 8601 as
   * 2019-11-03T10:18:31+01:00[Europe/Paris]
   */
  public static String offsetAsIso8601Formatted(TimeZone timeZone, ZonedDateTime zonedDateTime) {
    return zonedDateTime.withZoneSameInstant(timeZone.toZoneId()).toString();
  }

  /**
   * Converts an ISO 8601 date-time-zone as 2022-01-29T21:01:31.000+11:00 into Database format
   * 2022-01-29 21:01:31. Cuts off zone information
   */
  public static String offsetToUtcDatabaseFormatted(String iso8601ZonedDateTime) {
    ZonedDateTime dateTime = ZonedDateTime.parse(iso8601ZonedDateTime, ISO_ZONED_DATE_TIME);
    dateTime = dateTime.minusHours(getOffsetHoursDifference(ZoneId.of("UTC"), dateTime.getZone()));
    return dateTime.toString().substring(0, 19).replace("T", " ");
  }

  public int getOffsetHoursDifference(ZoneId from, ZoneId to) {
    ZonedDateTime now = ZonedDateTime.now();
    OffsetDateTime fromOffset = OffsetDateTime.ofInstant(now.toInstant(), from);
    OffsetDateTime toOffset = OffsetDateTime.ofInstant(now.toInstant(), to);
    int offsetDifference = toOffset.getHour() - fromOffset.getHour();
    return offsetDifference;
  }

  /** Format zoned date and time in AM PM format like Oct 13, 2020, 08:02 PM ECT */
  public static String formatAsAmPm(ZonedDateTime time, Locale locale) {
    return time.format(AM_PM_ZONE_FORMATTER.withLocale(locale))
        + " "
        + getZoneAsStandardAbbreviation(time);
  }

  public static String getZoneAsStandardAbbreviation(ZonedDateTime dateTime) {
    return TimeZoneEnum.getByAnyText(dateTime.getZone().getId()).getStdAbbreviation();
  }

  public static String getZoneAsDstAbbreviation(ZonedDateTime dateTime) {
    return TimeZoneEnum.getByAnyText(dateTime.getZone().getId()).getDstAbbreviation();
  }

  public static String getZoneAsStandardOffset(ZonedDateTime dateTime) {
    return TimeZoneEnum.getByAnyText(dateTime.getZone().getId()).getStdUtcOffset();
  }

  public static String getZoneAsDstOffset(ZonedDateTime dateTime) {
    return TimeZoneEnum.getByAnyText(dateTime.getZone().getId()).getDstUtcOffset();
  }

  /**
   * Convert String to ZonedDateTime.
   *
   * @param dateTimeAndZone String like 2021-01-14T11:52:02.160-08:00[America/Los_Angeles]
   * @return ZonedDateTime converted object.
   */
  public static ZonedDateTime fromIso8601(String dateTimeAndZone) {
    return ZonedDateTime.parse(
        dateTimeAndZone,
        new DateTimeFormatterBuilder()
            .parseCaseInsensitive()
            .append(DateTimeFormatter.ISO_LOCAL_DATE)
            .optionalStart() // time made optional
            .appendLiteral('T')
            .append(DateTimeFormatter.ISO_LOCAL_TIME)
            .optionalStart() // zone and offset made optional
            .appendOffsetId()
            .optionalStart()
            .appendLiteral('[')
            .parseCaseSensitive()
            .appendZoneRegionId()
            .appendLiteral(']')
            .optionalEnd()
            .optionalEnd()
            .optionalEnd()
            .toFormatter());
  }

  /**
   * Converts two date-times into a date with time range in a given time zone.
   *
   * @param from Date with time as range from.
   * @param to Date with time as range to. The same date is expected as in "from".
   * @param timezone An IANA timezone.
   * @param is24HFormat Is time in 24-hour format.
   * @param locale Locale to use in formatting. Can be NULL - system default will be used then.
   * @return A string formatted as "Jan 21, 2022 7:00pm-8:00pm" in respective timezone.
   */
  public static String toDateWithTimeRangeOfGivenZone(
      ZonedDateTime from, ZonedDateTime to, TimeZone timezone, boolean is24HFormat, Locale locale) {

    if (from == null || to == null) {
      return null;
    }

    ZonedDateTime fromZone = toAnotherZone(from, timezone);
    ZonedDateTime toZone = toAnotherZone(to, timezone);
    locale = locale == null ? Locale.getDefault() : locale;

    String fromFormatted =
        formatAmPm(
            fromZone.format(
                is24HFormat
                    ? DateTimeFormatter.ofPattern("MMM d, yyyy HH:mm").withLocale(locale)
                    : DateTimeFormatter.ofPattern("MMM d, yyyy H:mma").withLocale(locale)),
            true);

    String toFormatted =
        formatAmPm(
            toZone.format(
                is24HFormat
                    ? DateTimeFormatter.ofPattern("HH:mm").withLocale(locale)
                    : DateTimeFormatter.ofPattern("H:mma").withLocale(locale)),
            true);

    return fromFormatted + "-" + toFormatted;
  }

  /**
   * Formats time postfixes AM & PM to lower or upper case in a time-containing string.
   *
   * @param toFormat String to format.
   * @param toLowerCase Convert to lower case.
   * @return String with cases-formatted AMs - PMs.
   */
  private static String formatAmPm(String toFormat, boolean toLowerCase) {
    StringBuilder formatted = new StringBuilder(toFormat);
    if (toLowerCase) {
      try {
        formatted.replace(toFormat.lastIndexOf("AM"), toFormat.lastIndexOf("AM") + 2, "am");
      } catch (Exception ignore) {
      }
      try {
        formatted.replace(toFormat.lastIndexOf("PM"), toFormat.lastIndexOf("PM") + 2, "pm");
      } catch (Exception ignore) {
      }
    } else {
      try {
        formatted.replace(toFormat.lastIndexOf("am"), toFormat.lastIndexOf("am") + 2, "AM");
      } catch (Exception ignore) {
      }
      try {
        formatted.replace(toFormat.lastIndexOf("pm"), toFormat.lastIndexOf("pm") + 2, "PM");
      } catch (Exception ignore) {
      }
    }
    return formatted.toString();
  }

  /** Convert between types. Only year, month and day are persisted. */
  public static Date fromDate(LocalDate date) {
    Instant instant = date.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant();
    return Date.from(instant);
  }

  public static LocalDate fromDate(Date date) {
    return date == null
        ? null
        : Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
  }

  /**
   * Defines if dates fall into required period. If both or either of dates are not set - always
   * match the period.
   *
   * @param periodStartDate Required period start date inclusive. May be null - then no upper limit.
   * @param periodEndDate Required period end date inclusive. May be null - then no lower limit.
   * @param startDate Start-date to evaluate. Should be within period or on border.
   * @param endDate End-date to evaluate. Should be within period or on border.
   * @return Dates are inside required period.
   */
  public static boolean isWithinPeriod(
      ZonedDateTime periodStartDate,
      ZonedDateTime periodEndDate,
      ZonedDateTime startDate,
      ZonedDateTime endDate) {

    if (startDate == null && endDate == null) {
      return true;
    }

    boolean startDateIsBefore =
        startDate != null && periodStartDate != null && startDate.isBefore(periodStartDate);

    boolean startDateIsEqual =
        startDate != null && (periodStartDate == null || startDate.isEqual(periodStartDate));

    boolean endDateIsAfter =
        endDate != null && periodEndDate != null && endDate.isAfter(periodEndDate);

    boolean endDateIsEqual =
        endDate != null && (periodEndDate == null || endDate.isEqual(periodEndDate));

    return (!startDateIsBefore || startDateIsEqual) && (!endDateIsAfter || endDateIsEqual);
  }

  /**
   * See @{@link DateTimeUtils#isWithinPeriod(ZonedDateTime, ZonedDateTime, ZonedDateTime,
   * ZonedDateTime)}
   */
  public static boolean isWithinPeriod(
      Date periodStartDate, Date periodEndDate, Date startDate, Date endDate) {

    return isWithinPeriod(
        from(periodStartDate), from(periodEndDate), from(startDate), from(endDate));
  }

  public static String offsetAndFormat(Date date, String timeZone, DateTimeFormatter formatter) {
    ZoneId zoneId = ZoneId.of(timeZone);
    ZonedDateTime dateTime = date.toInstant().atZone(zoneId);
    String zoneAbbr = zoneId.getDisplayName(TextStyle.SHORT, Locale.US);
    return dateTime.format(formatter) + SPACE + zoneAbbr;
  }

  public static String offsetAndFormat(
      ZonedDateTime zonedDateTime, String timeZone, DateTimeFormatter formatter) {

    ZoneId zoneId = ZoneId.of(timeZone);
    String zoneAbbr = zoneId.getDisplayName(TextStyle.SHORT, Locale.US);
    return zonedDateTime.format(formatter) + SPACE + zoneAbbr;
  }

  public static String offsetAndFormat(
      ZonedDateTime zonedDateTime, TimeZone timeZone, DateTimeFormatter formatter) {

    ZoneId zoneId = timeZone.toZoneId();
    String zoneAbbr = zoneId.getDisplayName(TextStyle.SHORT, Locale.US);
    return zonedDateTime.format(formatter) + SPACE + zoneAbbr;
  }

  public static List<Date> getMonthBoundaryDays(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.DAY_OF_MONTH, 1);
    Date firstDate = calendar.getTime();
    int lastDay = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
    calendar.set(Calendar.DAY_OF_MONTH, lastDay);
    Date lastDate = calendar.getTime();
    return List.of(firstDate, lastDate);
  }

  public static String fromObject(Object dateTime, DateTimeFormatter formatter) {
    if (dateTime instanceof Date) {
      return from((Date) dateTime).format(formatter);
    }
    if (dateTime instanceof ZonedDateTime) {
      return ((ZonedDateTime) dateTime).format(formatter);
    }
    return null;
  }

  /**
   * @param initialZonedDateTime Zoned date and time to fetch zone name from.
   * @param zoneNameType One of the following field names: countryCode, stdAbbreviation,
   *     dstAbbreviation, stdUtcOffset, dstUtcOffset, description, location, iana, coordinates;
   * @return A text name of the zone.
   */
  public static String getZoneName(ZonedDateTime initialZonedDateTime, String zoneNameType) {
    String initialZoneName = initialZonedDateTime.getZone().normalized().toString();
    TimeZoneEnum zone = TimeZoneEnum.getByAnyText(initialZoneName);
    String zoneName = TimeZoneEnum.get(zoneNameType, zone);
    return zoneName;
  }

  public static boolean isTodayWithinPeriod(
      ZonedDateTime periodStartDate, ZonedDateTime periodEndDate) {

    return isWithinPeriod(periodStartDate, periodEndDate, ZonedDateTime.now(), ZonedDateTime.now());
  }

  public static boolean isTodayWithinPeriod(Date periodStartDate, Date periodEndDate) {
    return isWithinPeriod(
        from(periodStartDate), from(periodEndDate), ZonedDateTime.now(), ZonedDateTime.now());
  }
}
