package com.zonzonzon.common.utils;

import com.zonzonzon.common.ui.exception.BusinessException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PhoneNumberUtils {

  public static final String BRACKETS_HYPHENS_FORMAT = "(%s) %s-%s";
  private static final String HYPHENS_FORMAT = "$1-$2-$3";
  public static final String PHONE_VALIDATION_REGEX =
      "^[(]?[2-9]\\d{2}[)]?([,-]|\\s)?\\d{3}([,-]|\\s)?\\d{4}";

  /*
   * Validate phone number without a country code.
   *
   * @param phoneNumber Phone number - digits only. The country code should be omitted.
   */
  public static boolean isValid(String phoneNumber) {
    return phoneNumber != null && phoneNumber.matches(PHONE_VALIDATION_REGEX);
  }

  /**
   * Removes everything from the given phone number except for digits.
   *
   * @param phoneNumber A phone number with digits and formatting characters.
   * @return A phone number as a pure digits string. A leading 0 is possible.
   */
  public static String toDatabaseFormat(String phoneNumber) {
    if (isValid(phoneNumber)) {
      return phoneNumber.replaceAll("[^\\d]", "");
    } else {
      throw new BusinessException("Invalid Phone Number", "Please check the phone number input. No country code is expected.");
    }
  }

  /**
   * Formats a phone number text into a given String format.
   *
   * @param phoneNumber A phone number to be formatted. Country code should be omitted.
   * @param format Format to apply. Can be NULL, then "(123) 123-1234" is used by default.
   * @return A formatted phone number, or unformatted if format is not applicable.
   */
  public static String format(String phoneNumber, String format) {
    String cleanPhoneNumber = toDatabaseFormat(phoneNumber);
    if (cleanPhoneNumber.length() == 10) {
      String areaCode = cleanPhoneNumber.substring(0, 3);
      String prefix = cleanPhoneNumber.substring(3, cleanPhoneNumber.length() - 4);
      String lineNumber = cleanPhoneNumber.substring(cleanPhoneNumber.length() - 4);
      return String.format(
          format == null ? BRACKETS_HYPHENS_FORMAT : format, areaCode, prefix, lineNumber);
    } else {
      log.warn("Format {} is not applicable to a phone number {}", format, cleanPhoneNumber);
      return cleanPhoneNumber;
    }
  }

  /**
   * Formats a phone number with hyphens: 123-123-1234
   *
   * @param phoneNumber A phone number to format.
   * @return Formatted phone number.
   */
  public static String formatToHyphens(String phoneNumber) {
    String cleanPhoneNumber = toDatabaseFormat(phoneNumber);
    String pattern = "(\\d\\d\\d)(\\d\\d\\d)(\\d\\d\\d\\d)";
    String formatted = cleanPhoneNumber.replaceAll(pattern, HYPHENS_FORMAT);
    return formatted;
  }
}
