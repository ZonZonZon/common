package com.zonzonzon.common.utils;

import static java.util.Arrays.asList;

import java.util.List;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/** Helper methods to work with lambdas. */
@UtilityClass
@Slf4j
public class LambdaUtils {

  /**
   * Wraps lambda function with return value to sneaky throw its exception.
   *
   * <p>Example example = sneakyThrow(parameters -> lambdaFunction((String)
   * parameters.get(0), (UUID) parameters.get(1)), value, id);
   */
  @SneakyThrows
  public <T> T sneakyThrow(
      SneakyThrownFunction<List<?>, T> methodToTest, Object... parameters) {
    return methodToTest.apply(asList(parameters));
  }

  /**
   * Wraps lambda void function to sneaky throw its exception.
   *
   * <p> runVoidWithSneakyThrows(parameters -> lambdaFunction((String)
   * parameters.get(0), (UUID) parameters.get(1)), value, id);
   */
  @SneakyThrows
  public void sneakyThrowVoid(
      SneakyThrownConsumer<List<?>> methodToTest, Object... parameters) {
    methodToTest.accept((asList(parameters)));
  }

  /**
   * Use this type when you pass a lambda function that throws unhandled exception. Avoids wrapping
   * in a try-catch block as exception handling is done internally.
   *
   * @param <P> Parameters to be passed to a function.
   * @param <R> The function return type.
   */
  @FunctionalInterface
  public interface SneakyThrownFunction<P, R> {
    R apply(P parameters) throws Exception;
  }

  /**
   * Use this type when you pass a lambda consumer (void function) that throws unhandled exception.
   * Avoids wrapping in a try-catch block as exception handling is done internally.
   *
   * @param <P> Parameters to be passed to a function.
   */
  @FunctionalInterface
  public interface SneakyThrownConsumer<P> {
    void accept(P parameters) throws Exception;
  }
}

