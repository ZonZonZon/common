package com.zonzonzon.common.utils;

import static java.time.LocalDate.now;
import static java.time.ZoneOffset.UTC;

import java.security.SecureRandom;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import lombok.experimental.UtilityClass;

/** Helper methods to generate random values. */
@UtilityClass
public class RandomUtils {

  private static final SecureRandom SECURE_RANDOM_INSTANCE = new SecureRandom();
  private static final Random RANDOM_INSTANCE = new Random();

  public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
    int x = SECURE_RANDOM_INSTANCE.nextInt(clazz.getEnumConstants().length);
    return clazz.getEnumConstants()[x];
  }

  public static double randomDouble(double min, double max) {
    return min + Math.random() * (max - min);
  }

  public static float randomFloat(float min, float max) {
    return min + SECURE_RANDOM_INSTANCE.nextFloat() * (max - min);
  }

  public static int randomInteger(int min, int max) {
    return min + RANDOM_INSTANCE.nextInt(max - min);
  }

  public static boolean randomBoolean() {
    return RANDOM_INSTANCE.nextBoolean();
  }

  /**
   * String from ASCII characters from code 97 to code 122.
   *
   * @param min Minimal amount of characters in a string.
   * @param max Maximal amount of characters in a string.
   * @return A random length and content string.
   */
  public static String randomString(int min, int max) {
    if (min >= max) {
      return null;
    }
    StringBuilder result = new StringBuilder();
    int length = randomInteger(min,max);
    int position = 0;
    while (position <= length) {
      result.append((char) (randomInteger(97, 122)));
      position++;
    }
    return result.toString();
  }

  public static Object randomOfList(List<Object> objects) {
    return objects.isEmpty() ? null : objects.get(randomInteger(0, objects.size()));
  }

  public static ZonedDateTime randomDateTime() {
    int year = randomInteger(now().getYear() - 10, now().getYear() + 10);
    int month = randomInteger(1, 12);
    int day = randomInteger(1, 28);
    int hour = randomInteger(1, 24);
    int minute = randomInteger(1, 60);
    int second = randomInteger(1, 60);
    int millisecond = randomInteger(1, 1000);
    return ZonedDateTime.of(year, month, day, hour, minute, second, millisecond, UTC);
  }

  /**
   * Generates a random amount of UUIDs in a set.
   * @param min Min amount to generate.
   * @param max Max amount to generate.
   * @return A set of UUIDs.
   */
  public static Set<UUID> randomUuids(int min, int max) {
    int uuidsAmount = randomInteger(min, max);
    Set<UUID> uuids = new HashSet<>();
    int uuidsAmountIndex;
    for (uuidsAmountIndex = 0; uuidsAmountIndex < uuidsAmount; uuidsAmountIndex++) {
      uuids.add(UUID.randomUUID());
    }
    return uuids;
  }
}
