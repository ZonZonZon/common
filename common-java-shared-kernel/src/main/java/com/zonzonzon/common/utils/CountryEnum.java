package com.zonzonzon.common.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Deprecated(since = "Use com.neovisionaries.i18n.CountryCode instead")
public enum CountryEnum {}
