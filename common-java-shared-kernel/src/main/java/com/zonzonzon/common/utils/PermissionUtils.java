package com.zonzonzon.common.utils;

import static java.util.Collections.emptySet;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class PermissionUtils {

  /**
   * Defines if REST-method is authorized. If no permission annotations are set on the endpoint,
   * then it is allowed.
   *
   * @param givenPermissions Permissions given. If empty - authorized only if requiredPermissions
   *     are empty too.
   * @param method Method to evaluate authorization.
   * @param matchAll All required permissions should be found in given permissions. Otherwise, any
   *     match is enough for authorization.
   * @return This method is authorized.
   */
  public Set<String> getMissingPermissions(
      Set<String> givenPermissions, Method method, boolean matchAll) {

    Stream<Annotation> methodAnnotations = Arrays.stream(method.getAnnotations());

    Annotation permissionAnnotation =
        methodAnnotations
            // .filter(Permissions.class::isInstance)
            .filter(annotation -> annotation instanceof Permissions)
            .findAny()
            .orElse(null);

    if (permissionAnnotation != null) {
      return getMissingPermissions(givenPermissions, permissionAnnotation, matchAll);
    } else {
      return emptySet();
    }
  }

  /**
   * Defines if all required REST-method permissions are given.
   *
   * @param givenPermissions Permissions given. If empty - authorized only if requiredPermissions
   *     are empty too.
   * @param annotation Permissions annotation that contains permissions keys.
   * @param matchAll All required permissions should be found in given permissions. Otherwise any
   *     match is enough for authorization.
   * @return A set of missing permissions.
   */
  public Set<String> getMissingPermissions(
      Set<String> givenPermissions, Annotation annotation, boolean matchAll) {

    if (annotation instanceof Permissions) {
      Set<String> requiredPermissions = Set.of(((Permissions) annotation).keys());
      // No permission check if nothing is required:
      if (requiredPermissions.isEmpty()) {
        return emptySet();
      }
      Set<String> missingPermissions = new HashSet<>();
      if (matchAll) {
        // Contains all the required permissions:
        requiredPermissions.forEach(
            requiredPermission -> {
              if (!givenPermissions.contains(requiredPermission)) {
                missingPermissions.add(requiredPermission);
              }
            });
        return missingPermissions;
      } else {
        // Contains at least one of required permissions:
        if (givenPermissions.stream().anyMatch(requiredPermissions::contains)) {
          return emptySet();
        } else {
          return requiredPermissions;
        }
      }
    }
    return emptySet();
  }
}
