package com.zonzonzon.common.utils;

public final class ValidationUtils {

  /**
   * Removes disallowed symbols from string to prevent input injection.
   * @param input User input with possible injection.
   * @return Value without injection-sensible symbols.
   */
  public static String sanitizeInjection(String input){
    return input.replaceAll("[^A-Za-z0-9 ]", "");
  }
}
