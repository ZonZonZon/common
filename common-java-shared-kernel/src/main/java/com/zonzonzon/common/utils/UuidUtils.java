package com.zonzonzon.common.utils;

import java.nio.ByteBuffer;
import java.util.UUID;
import lombok.experimental.UtilityClass;

/** Helper methods to work with UUID type. */
@UtilityClass
public class UuidUtils {

  public static final String UUID_VALIDATION_REGEX =
      "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-4[0-9a-fA-F]{3}-[89abAB][0-9a-fA-F]{3}-[0-9a-fA-F]{12}";

  /**
   * Get bytes array for storage based on UUID value.
   *
   * @param uuid UUID value.
   * @return UUID as bytes array.
   */
  public static byte[] from(UUID uuid) {
    ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
    bb.putLong(uuid.getMostSignificantBits());
    bb.putLong(uuid.getLeastSignificantBits());
    return bb.array();
  }

  /**
   * Get UUID from bytes array.
   *
   * @param bytes Bytes value.
   * @return UUID as bytes array.
   */
  public static UUID from(byte[] bytes) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
    long high = byteBuffer.getLong();
    long low = byteBuffer.getLong();
    return new UUID(high, low);
  }

  /**
   * Get UUID for storage as a string without hyphens.
   *
   * @param uuid UUID value.
   * @return UUID as bytes array.
   */
  public static String toStringWithoutHyphens(UUID uuid) {
    return uuid.toString().replaceAll("-", "");
  }

  /**
   * Converts a string containing uuid without hyphens into a UUID.
   *
   * @param uuidWithoutHyphens A UUID string without hyphens.
   * @return UUID type value.
   */
  @SuppressWarnings("UnnecessaryLocalVariable")
  public static UUID fromStringWithoutHyphens(String uuidWithoutHyphens) {
    if (uuidWithoutHyphens == null || uuidWithoutHyphens.length() != 32) {
      return null;
    }
    String hyphenSign = "-";
    StringBuilder finalString = new StringBuilder(uuidWithoutHyphens);
    finalString.insert(8, hyphenSign);
    finalString.insert(13, hyphenSign);
    finalString.insert(18, hyphenSign);
    finalString.insert(23, hyphenSign);

    UUID uuid = UUID.fromString(finalString.toString());
    return uuid;
  }

  /**
   * Converts a string containing uuid with hyphens into a UUID.
   *
   * @param uuidWithHyphens A UUID string with hyphens.
   * @return UUID type value.
   */
  @SuppressWarnings("UnnecessaryLocalVariable")
  public static UUID fromStringWithHyphens(String uuidWithHyphens) {
    if (uuidWithHyphens == null || uuidWithHyphens.length() != 36) {
      return null;
    }
    UUID uuid = UUID.fromString(uuidWithHyphens);
    return uuid;
  }
}
