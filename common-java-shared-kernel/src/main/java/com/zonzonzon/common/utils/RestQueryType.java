package com.zonzonzon.common.utils;

import java.util.Arrays;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum RestQueryType {
  POST("POST", "PostMapping"),
  GET("GET", "GetMapping"),
  PUT("PUT", "PutMapping"),
  PATCH("PATCH", "PatchMapping"),
  DELETE("DELETE", "DeleteMapping");

  private final String name;
  private final String annotationName;

    public static RestQueryType findByAnnotationName(String annotationName) {
        return Arrays.stream(RestQueryType.values())
            .map(
                restQueryType -> {
                    if (restQueryType.getAnnotationName().equals(annotationName)) {
                        return restQueryType;
                    } else {
                        return null;
                    }
                })
            .filter(Objects::nonNull)
            .findAny()
            .orElse(null);
    }

    public static RestQueryType findByName(String name) {
        return Arrays.stream(RestQueryType.values())
            .map(
                restQueryType -> {
                    if (restQueryType.getName().equals(name)) {
                        return restQueryType;
                    } else {
                        return null;
                    }
                })
            .filter(Objects::nonNull)
            .findAny()
            .orElse(null);
    }
}
