package com.zonzonzon.common.utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@Deprecated(since = "Use com.neovisionaries.i18n.CurrencyCode instead")
public enum CurrencyCode {}
