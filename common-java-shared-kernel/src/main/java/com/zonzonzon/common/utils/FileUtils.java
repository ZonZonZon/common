package com.zonzonzon.common.utils;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;
import lombok.experimental.UtilityClass;

/**
 * Helper methods to work with files.
 */
@SuppressWarnings("UnnecessaryLocalVariable")
@UtilityClass
public class FileUtils {

  /**
   * Read the file.
   *
   * @param path A file path.
   * @param encoding File content encoding.
   * @return File content as text.
   * @throws IOException Unable to read the file.
   */
  public static String readFile(String path, Charset encoding) throws IOException {

    byte[] encoded = Files.readAllBytes(Paths.get(path));
    return new String(encoded, encoding);
  }

  /**
   * Read the file with the encoding.
   *
   * @param callerClass Caller class: MyClass.class or this.getClass().
   * @param fileName File path to read from resources root: /sql/my_file.sql
   * @param charset Desired encoding.
   * @return File content as text.
   */
  public static String readFile(Class<?> callerClass, String fileName, Charset charset) {

    String fileContent = new Scanner(callerClass.getResourceAsStream(fileName), charset.toString())
        .useDelimiter("\\A")
        .next();

    return fileContent;
  }

  /**
   * Read a text file in default UTF-8 encoding.
   *
   * @param callerClass A caller class: MyClass.class or this.getClass().
   * @param fileName Path to file to read from resources root: /sql/my_file.sql
   * @return File content as text.
   */
  public static String readFile(Class<?> callerClass, String fileName) {
    return readFile(callerClass, fileName, StandardCharsets.UTF_8);
  }
}
