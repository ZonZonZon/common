package com.zonzonzon.common.utils;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/** Helper methods to work with reflection. */
@SuppressWarnings({"UnnecessaryLocalVariable", "RedundantIfStatement", "unused"})
@UtilityClass
@Slf4j
public class ReflectionUtils {

  /**
   * Replaces throwing any exception with NULL result.
   *
   * @param statement A statement that can throw any exception.
   * @param <T> Expression result type to be returned if had been executed normally.
   * @return Expression normal result or NULL.
   */
  public static <T> T getSafe(Supplier<T> statement) {
    try {
      T value = statement.get();

      if (value instanceof String
          && (String.valueOf(value).equals("")
              || String.valueOf(value).equals("null")
              || String.valueOf(value).equals("Null")
              || String.valueOf(value).equals("NULL"))) {

        return null;
      } else {
        return value;
      }
    } catch (Exception anyException) {
      return null;
    }
  }

  /**
   * A way to read object's private field. Should be used in extremely rare cases. Looks for fields
   * in up one parent class as well.
   *
   * @param <O> Object type.
   * @param <F> Field type.
   * @param object Object to look for a field.
   * @param fieldName The name of the field to read.
   * @return The field.
   */
  public static <O, F> F getPrivateField(O object, String fieldName) {
    if (object == null) {
      log.error("Can't get private field from a NULL object");
    }
    F result = null;
    try {
      Field field = null;
      Class<?> currentClass = object.getClass();
      while (currentClass != null) {
        boolean hasField =
            Arrays.stream(currentClass.getDeclaredFields())
                .map(Field::getName)
                .anyMatch(declaredFieldName -> declaredFieldName.equals(fieldName));
        if (hasField) {
          field = currentClass.getDeclaredField(fieldName);
          break;
        }
        currentClass = currentClass.getSuperclass();
      }
      if (field != null) {
        field.setAccessible(true);
        result = (F) field.get(object);
        field.setAccessible(false);
      }
    } catch (NoSuchFieldException | IllegalAccessException e) {
      log.error("Can't get private field using reflection: " + e.getLocalizedMessage());
    }
    return result;
  }

  public static List<Method> getPublicGetters(Class<?> aClass) {
    List<Method> getterList = new ArrayList<>();
    Method[] methods = aClass.getMethods();

    for (Method method : methods) {
      if (isPublicGetter(method, aClass)) {
        getterList.add(method);
      }
    }

    return getterList;
  }

  private static List<Method> getPublicSetters(Class<?> aClass) {
    List<Method> setterList = new ArrayList<>();
    Method[] methods = aClass.getMethods();

    for (Method method : methods) {
      if (isPublicSetter(method, aClass)) {
        setterList.add(method);
      }
    }

    return setterList;
  }

  private static List<Method> getPublicGetters(Class<?> aClass, List<String> gettersToIgnore) {
    List<Method> getterList = getPublicGetters(aClass);

    gettersToIgnore.forEach(
        ignoredGetterName ->
            getterList.removeIf(method -> method.getName().equals(ignoredGetterName)));

    return getterList;
  }

  private static boolean isPublicGetter(Method method, Class<?> aClass) {
    if (!aClass.isEnum() && !method.getName().startsWith("get")) {
      return false;
    }

    if (method.getParameterTypes().length != 0) {
      return false;
    }

    if (void.class.equals(method.getReturnType())) {
      return false;
    }

    if (!Modifier.isPublic(method.getModifiers())) {
      return false;
    }

    if (Modifier.isNative(method.getModifiers())) {
      return false;
    }

    return true;
  }

  private static boolean isPublicSetter(Method method, Class<?> aClass) {
    if (!aClass.isEnum() && !method.getName().startsWith("set")) {
      return false;
    }

    if (method.getParameterTypes().length != 0) {
      return false;
    }

    if (!Modifier.isPublic(method.getModifiers())) {
      return false;
    }

    if (Modifier.isNative(method.getModifiers())) {
      return false;
    }

    return true;
  }

  /**
   * Update non-null valued fields only.
   *
   * @param target A target object to update.
   * @param source A source object to update.
   * @param failOnUnknownMethod Fail on NoSuchMethodException is thrown.
   * @param gettersToIgnore Getter method names to be ignored (like getId()).
   */
  public static <T> void updateWithNonNull(
      T source, T target, boolean failOnUnknownMethod, List<String> gettersToIgnore)
      throws InvocationTargetException, IllegalAccessException, NoSuchFieldException,
          ClassNotFoundException, NoSuchMethodException {

    if (target.getClass() != source.getClass()) {
      log.error(
          "To update, object should be of the same type. Currently source is of type {} and target is {}",
          source.getClass().getName(),
          target.getClass().getName());
      if (failOnUnknownMethod) {
        throw new ClassNotFoundException();
      }
    }

    List<Method> getterList = getPublicGetters(source.getClass(), gettersToIgnore);
    for (Method getter : getterList) {
      try {
        Object newValue = getter.invoke(source);
        setValue(target, getter, newValue);
      } catch (IllegalAccessException
          | InvocationTargetException
          | NoSuchFieldException
          | ClassNotFoundException
          | NoSuchMethodException e) {
        log.error(
            "Failed to update one object with another. Maybe you don't have required getters and setters. See {}",
            e.getLocalizedMessage());
        if (failOnUnknownMethod) {
          throw e;
        }
      }
    }
  }

  /** An alias for @{@link ReflectionUtils#updateWithNonNull(Object, Object, boolean, List)} */
  public static <T> void updateWithNonNull(T source, T target) {
    try {
      updateWithNonNull(source, target, false, emptyList());
    } catch (IllegalAccessException
        | InvocationTargetException
        | NoSuchFieldException
        | ClassNotFoundException
        | NoSuchMethodException ignored) {
    }
  }

  /** An alias for @{@link ReflectionUtils#updateWithNonNull(Object, Object, boolean, List)} */
  public static <T> void updateWithNonNull(T source, T target, List<String> gettersToIgnore) {
    try {
      updateWithNonNull(target, source, false, gettersToIgnore);
    } catch (IllegalAccessException
        | InvocationTargetException
        | NoSuchFieldException
        | ClassNotFoundException
        | NoSuchMethodException ignored) {
    }
  }

  /** Compare collections to contain the same object instances. */
  private static boolean isIdenticalListByMatchingObjectInstances(
      Collection<?> expectedValue, Collection<?> givenValue) {

    if (expectedValue.size() != givenValue.size()
        || expectedValue instanceof List && !(givenValue instanceof List)
        || expectedValue instanceof Queue && !(givenValue instanceof Queue)
        || expectedValue instanceof Set && !(givenValue instanceof Set)) {

      return false;
    }

    boolean collectionsAreEqual =
        givenValue.containsAll(expectedValue)
            && expectedValue.containsAll(givenValue)
            && givenValue.size() == expectedValue.size();

    return collectionsAreEqual;
  }

  private static boolean isPrimitiveWrapperType(Class<?> clazz) {
    final Set<Class<?>> WRAPPER_TYPES =
        new HashSet<>(
            asList(
                Boolean.class,
                Character.class,
                Byte.class,
                Short.class,
                Integer.class,
                Long.class,
                Float.class,
                Double.class,
                Void.class));

    return WRAPPER_TYPES.contains(clazz);
  }

  /**
   * Sets value using this object's setter. Works both for primitives and wrappers. Don't set lists.
   */
  private static <T> void setValue(T given, Method getter, Object newValue)
      throws IllegalAccessException, InvocationTargetException, ClassNotFoundException,
          NoSuchFieldException, NoSuchMethodException {
    // TODO: Implement for collections.
    if (newValue != null && !(newValue instanceof Collection)) {
      String setterName = getter.getName().replaceFirst("g", "s");
      Method setter;
      try {
        setter = given.getClass().getDeclaredMethod(setterName, newValue.getClass());
      } catch (NoSuchMethodException tryAsPrimitive) {
        // Try method signature with primitive parameter:
        Class<?> primitiveType =
            (Class<?>) Class.forName(newValue.getClass().getName()).getField("TYPE").get(newValue);

        setter = given.getClass().getDeclaredMethod(setterName, primitiveType);
      }

      setter.invoke(given, newValue);
    }
  }

  /**
   * Get the name of the class that has called current method.
   *
   * @param traceShift Amount of steps back to be shifted in a stack trace, to get previous caller
   *     method.
   * @return Class name.
   */
  public static String getCallerClassName(int traceShift) {
    StackTraceElement caller = getCaller(traceShift);
    return caller == null ? null : caller.getClassName() + "." + getMethodName(caller) + "()";
  }

  /**
   * An alias for {@link ReflectionUtils#getCallerClassName(int)}. The trace shift is 1 by default.
   */
  public static String getCallerClassName() {
    return getCallerClassName(1);
  }

  /**
   * Get the name of the class that has called current method.
   *
   * @param traceShift Amount of steps back to be shifted in a stack trace, to get previous caller
   *     method.
   * @return Stack trace element.
   */
  public static StackTraceElement getCaller(int traceShift) {
    StackTraceElement[] trace = Thread.currentThread().getStackTrace();
    for (int i = traceShift; i < trace.length; i++) {
      StackTraceElement element = trace[i];
      if (!element.getClassName().equals(ReflectionUtils.class.getName())
          && element.getClassName().indexOf("java.lang.Thread") != 0) {
        return element;
      }
    }
    return null;
  }

  /** An alias for @{@link ReflectionUtils#getMethodsByParameterType(Class, Class, String)} */
  public static List<Method> getMethodsByParameterType(Class<?> aClass, Class<?> parameterType) {
    List<Method> methods = new ArrayList<>();
    for (Method method : aClass.getDeclaredMethods()) {
      Class<?>[] parameterTypes = method.getParameterTypes();
      if (parameterTypes.length == 1) {
        if (parameterTypes[0].getName().equals(parameterType.getName())
            || parameterType.isAssignableFrom(parameterTypes[0])) {
          methods.add(method);
        }
      }
    }
    return methods;
  }

  /**
   * Searches for class methods that have a single parameter of a given type.
   *
   * @param classToSearchIn A class to search in.
   * @param parameterType A parameter class or interface this class is implementing.
   * @param methodName Only methods with a given name will be returned.
   * @return Methods matching the search conditions.
   */
  public Set<Method> getMethodsByParameterType(
      Class<?> classToSearchIn, Class<?> parameterType, String methodName) {

    Set<Method> methods =
        Arrays.stream(classToSearchIn.getMethods())
            .filter(method -> method.getName().equals(methodName))
            .filter(
                method -> {
                  Set<Class<?>> parameterTypes =
                      Arrays.stream(method.getParameterTypes()).collect(Collectors.toSet());

                  boolean matchesMethod =
                      !parameterTypes.stream()
                          .filter(
                              type -> {
                                boolean matchesParameterTypeName =
                                    type.getName().equals(parameterType.getName());

                                boolean matchesInterfaceTypeName = false;
                                if (!matchesParameterTypeName) {
                                  matchesInterfaceTypeName =
                                      Stream.of(parameterType.getInterfaces())
                                          .map(Class::getName)
                                          .collect(Collectors.toSet())
                                          .contains(type.getName());
                                }
                                return matchesParameterTypeName || matchesInterfaceTypeName;
                              })
                          .collect(Collectors.toSet())
                          .isEmpty();

                  return matchesMethod;
                })
            .collect(Collectors.toSet());

    return methods;
  }

  /**
   * Finds method name of a caller. Uses stack trace. Removes lambda prefix and suffix if found.
   *
   * @param caller Stack trace element.
   * @return Pure method name.
   */
  public static String getMethodName(StackTraceElement caller) {
    if (caller == null) {
      return null;
    }
    String original = caller.getMethodName();
    String result = original;
    boolean isWrappedInLambda = original.replaceAll("[^$]", "").length() == 2;
    if (isWrappedInLambda) {
      int indexOfPrefixEnd = original.indexOf("$") + 1;
      int indexOfSuffixStart = original.lastIndexOf("$");
      result = original.substring(indexOfPrefixEnd, indexOfSuffixStart);
    }
    return result;
  }

  /**
   * Finds any method by a name in a class.
   *
   * @param name Name or a part of a name.
   * @param aClass A class to search in.
   * @param isFullNameMatch Search for a full name match.
   * @return A matching method.
   */
  public static Method getMethod(String name, Class<?> aClass, boolean isFullNameMatch) {
    final Optional<Method> matchedMethod =
        Arrays.stream(aClass.getDeclaredMethods())
            .filter(
                method ->
                    isFullNameMatch
                        ? method.getName().equalsIgnoreCase(name)
                        : method.getName().toLowerCase().contains(name.toLowerCase()))
            .findAny();

    if (matchedMethod.isEmpty()) {
      throw new RuntimeException("No method containing: " + name);
    }

    return matchedMethod.get();
  }

  /**
   * Substitutes complex object tree with some readable format, keeping only useful information.
   *
   * @param object Complex object tree.
   * @return A simplified substitution for the object.
   */
  private static Object simplifyObject(Object object) {
    Object substitution = object;

    if (object instanceof ZonedDateTime) {
      substitution = ((ZonedDateTime) object).format(DateTimeFormatter.ISO_ZONED_DATE_TIME);
    }

    if (object instanceof LocalDate) {
      substitution = ((LocalDate) object).format(DateTimeFormatter.ISO_DATE);
    }

    return substitution;
  }

  /**
   * Detects if class has the expected field. Also looks for fields in the embedded objects when
   * parameters are passed using dots.
   *
   * @param aClass A class to search in.
   * @param singularFieldName Expected field name singular notation. If not found - a simple plural
   *     notation with "s" will be tried. Use dots for embedded object fields. Can be NULL.
   * @param specialFieldName Full getter method name. Use dots for embedded object fields. Can be
   *     NULL.
   * @return Class has such a field.
   */
  @SneakyThrows
  public static boolean hasField(
      Class<?> aClass, String singularFieldName, String specialFieldName) {

    // Singular check:
    if (singularFieldName != null && singularFieldName.contains(".")) {
      List<String> fieldsChain = List.of(singularFieldName.split("\\."));
      Class<?> currentClass = aClass;
      Iterator<String> iterator = fieldsChain.iterator();
      while (iterator.hasNext()) {
        String currentField = iterator.next();
        boolean isFinalField = !iterator.hasNext();

        Method getter =
            isFinalField && specialFieldName != null
                ? getGetter(currentClass, null, specialFieldName)
                : getGetter(currentClass, currentField, null);

        if (getter != null && !isFinalField) {
          currentClass = getter.getReturnType();
          continue;
        }
        return getter != null;
      }
    } else {
      Method getter = getGetter(aClass, singularFieldName, specialFieldName);
      return getter != null;
    }
    return false;
  }

  /** Finds getter by field name. See {@link ReflectionUtils#hasField(Class, String, String)} */
  public static Method getGetter(
      Class<?> aClass, String singularFieldName, String specialFieldName) {

    return getPublicGetters(aClass).stream()
        .map(
            method -> {
              String getterName = StringUtils.getterToCamelCase(method.getName());

              String singularGetter = getterName.equals(singularFieldName) ? getterName : null;

              String pluralGetterName =
                  singularGetter == null && getterName.equals(singularFieldName + "s")
                      ? getterName
                      : null;

              try {
                pluralGetterName =
                    pluralGetterName == null
                            && getterName.equals(
                                NounFormsEnum.valueOf(singularFieldName).getPlural())
                        ? getterName
                        : pluralGetterName;
              } catch (Exception ignore) {
              }

              String specialGetterName =
                  method.getName().equals(specialFieldName) ? method.getName() : null;

              boolean hasField =
                  Stream.of(singularGetter, pluralGetterName, specialGetterName)
                          .filter(Objects::nonNull)
                          .findFirst()
                          .orElse(null)
                      != null;

              if (hasField) {
                return method;
              } else {
                return null;
              }
            })
        .filter(Objects::nonNull)
        .findFirst()
        .orElse(null);
  }

  /**
   * Get type of collection field.
   *
   * @param aClass A class containing collection.
   * @param collectionName A collection field name.
   */
  @SneakyThrows
  public static Class<?> getCollectionType(Class<?> aClass, String collectionName) {
    Field field = aClass.getDeclaredField(collectionName);
    ParameterizedType genericType = (ParameterizedType) field.getGenericType();
    Class<?> typeClass = (Class<?>) genericType.getActualTypeArguments()[0];
    return typeClass;
  }

  /**
   * Method to check if a Class is a Collection or not.
   *
   * @param aClass Class to inspect.
   * @return Class extends or implements Collection.
   */
  public static boolean isClassCollection(Class<?> aClass) {
    return Collection.class.isAssignableFrom(aClass) || Map.class.isAssignableFrom(aClass);
  }

  /**
   * Create an object from a class with given constructor parameters.
   *
   * @param aClass A class to create an object from.
   * @param constructorParameters Parameters to apply.
   * @return An object from a class.
   */
  @SneakyThrows
  public static Object getClassInstance(Class<?> aClass, Object... constructorParameters) {
    return aClass.getConstructor(String.class).newInstance(constructorParameters);
  }

  /**
   * Get a class field by name.
   * @param clazz Class to find a field in.
   * @param fieldName Field name to search by.
   * @return A field reflection object.
   */
  public static Field getField(Class<?> clazz, String fieldName) {
    Class<?> temporaryClass = clazz;
    do {
      try {
        Field field = temporaryClass.getDeclaredField(fieldName);
        return field;
      } catch (NoSuchFieldException exception) {
        temporaryClass = temporaryClass.getSuperclass();
      }
    } while (temporaryClass != null);

    throw new RuntimeException("The field '" + fieldName + "' is not found on the class " + clazz);
  }
}
