package com.zonzonzon.common.utils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

/** Helper methods for controllers. */
@UtilityClass
@Slf4j
public class RestUtils {

  /** A name of the header in an HTTP request to contain an authorization token. */
  public static final String TOKEN_HEADER_NAME = "authorization";

  /** A name of the variable in the logging system to contain an authorization token. */
  public static final String TOKEN_LOGGER_NAME = "token";

  /**
   * A name of the header in an HTTP request to contain an identifier to trace a distributed
   * operation.
   */
  public static final String TRACE_ID_HEADER_NAME = "trace-id";

  /**
   * A name of the variable in the logging system to contain an identifier to trace a distributed
   * operation.
   */
  public static final String TRACE_ID_LOGGER_NAME = "trace-id";

  /**
   * A name of the header in an HTTP request to contain an identifier of the client calling the
   * backend.
   */
  public static final String API_KEY_HEADER_NAME = "api-key";

  /**
   * A name of the variable in the logging system to contain an identifier of the client calling the
   * backend.
   */
  public static final String API_KEY_LOGGER_NAME = "api-key";

  /**
   * A name of the header in an HTTP request to contain an identifier of the user executing an
   * operation.
   */
  public static final String USER_ID_HEADER_NAME = "user-id";

  /**
   * A name of the variable in the logging system to contain an identifier of the user executing an
   * operation.
   */
  public static final String USER_ID_LOGGER_NAME = "user-id";

  /**
   * A name of the header in an HTTP request to contain a version of the client application that
   * calls the backend.
   */
  public static final String CLIENT_VERSION_HEADER_NAME = "client-version";

  /**
   * A name of the variable in the logging system to contain a version of the mobile application
   * that calls the backend.
   */
  public static final String CLIENT_VERSION_LOGGER_NAME = "client-version";

  /**
   * A name of the header in an HTTP request to contain an identifier of the client instance that
   * calls the backend.
   */
  public static final String CLIENT_ID_HEADER_NAME = "client-id";

  /**
   * A name of the variable in the logging system to contain an identifier of the client instance
   * that calls the backend.
   */
  public static final String CLIENT_ID_LOGGER_NAME = "client-id";

  /** A name of the header in an HTTP request to contain a host name. */
  public static final String HOSTNAME_HEADER_NAME = "hostname";

  public String getHostname() {
    return System.getenv("HOSTNAME") == null ? "undefined" : System.getenv("HOSTNAME");
  }

  /**
   * Adds value to MDC for logging. Removes if NULL.
   *
   * @param loggerName A placeholder name in a logger.
   * @param value A value to replace the placeholder in logs.
   */
  public void addHeaderToMdc(String loggerName, String value) {
    if (value != null) {
      MDC.put(loggerName, value);
    } else {
      MDC.remove(loggerName);
    }
  }

  /**
   * Defines REST request type by an enpoint method annotation.
   *
   * @param method An endpoint method.
   * @return Rest request enum.
   */
  public static RestQueryType getRequestType(Method method) {
    return Arrays.stream(method.getAnnotations())
        .map(
            annotation ->
                RestQueryType.findByAnnotationName(annotation.annotationType().getSimpleName()))
        .filter(Objects::nonNull)
        .findAny()
        .orElse(null);
  }

  /**
   * Returns form-data CGI-parameters from a request body. Works for unique parameter names only.
   *
   * @param requestBody Request string parameters of key-value pairs joined with &.
   */
  public static Map<String, String> getRequestParameters(String requestBody)
      throws UnsupportedEncodingException {

    Map<String, String> requestParams = new HashMap<>();
    String[] pairs = requestBody.split("&");
    for (String pair : pairs) {
      String[] keyValuePair = pair.split("=");
      requestParams.put(
          URLDecoder.decode(keyValuePair[0], StandardCharsets.UTF_8.name()),
          keyValuePair.length > 1
              ? URLDecoder.decode(keyValuePair[1], StandardCharsets.UTF_8.name())
              : null);
    }
    return requestParams;
  }
}
