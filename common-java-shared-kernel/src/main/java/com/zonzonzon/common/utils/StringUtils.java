package com.zonzonzon.common.utils;

import java.io.Reader;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.constraints.NotNull;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class StringUtils {

  public static final String EMPTY = "";
  public static final String SPACE = " ";
  public static final String COMMA = ",";
  public static final String HYPHEN = "-";
  public static final String BLANK = "";
  public static final String SINGLE_SPACE = " ";
  public static final String DOT = ".";

  /**
   * Converts a text to a title case where each word is from the capital letter. Abbreviations are
   * all capitalized.
   */
  private static String getTitleText(String text) {
    if (!text.isEmpty()) {
      text = text.toLowerCase();
      if (!text.isEmpty()) {
        char[] buffer = text.toCharArray();
        boolean capitalizeNext = true;
        for (int i = 0; i < buffer.length; ++i) {
          char character = buffer[i];
          if (capitalizeNext) {
            buffer[i] = Character.toTitleCase(character);
          }
          capitalizeNext = buffer[i] == ' ';
        }
        return new String(buffer);
      } else {
        return text;
      }
    } else {
      return text;
    }
  }

  /** Converts text to a Title Case. */
  public static String toTitleCase(String text) {
    String titleText = getTitleText(text);
    if (!titleText.equals(EMPTY)) {
      String[] titleTextSplit = titleText.split(SPACE);
      if (titleTextSplit[0].length() <= 3) {
        if (titleTextSplit.length > 1) {
          return String.format(
              "%s%s",
              titleTextSplit[0].toUpperCase(), titleText.substring(titleText.indexOf(SPACE)));
        }
        return titleTextSplit[0].toUpperCase();
      }
    }
    return titleText;
  }

  /**
   * Converts constant-case text to header-case text, where all words start from capital letter.
   *
   * @param text Text in constant-case like MY_CONSTANT.
   * @return Text in title-case like My Constant.
   */
  public static String constantCaseToHeaderCase(String text) {
    final String INITIAL_WORD_SEPARATOR = "_";
    final String FINAL_WORD_SEPARATOR = " ";

    if (text == null || text.isEmpty()) {
      return text;
    }

    String result =
        Arrays.stream(text.split(INITIAL_WORD_SEPARATOR))
            .map(StringUtils::getTitleText)
            .collect(Collectors.joining(FINAL_WORD_SEPARATOR));

    return result;
  }

  /**
   * Make-up a method name in Java notation. Used to generate getter and setter method names from a
   * field name.
   *
   * @param prefix First method word.
   * @param fieldName A field name to make a method from.
   * @return A methodName.
   */
  public static String toMethodCase(String prefix, String fieldName) {
    return prefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
  }

  /** Takes space separated words and converts them into a snake-case concatenation. */
  public static String wordsToSnakeCase(String text) {
    String result = text.toLowerCase();

    while (result.contains("  ")) {
      result = result.replace("  ", " ");
    }

    result = result.replace(" ", "_");
    return result;
  }

  /**
   * Converts space separated words into camel-case concatenated names.
   *
   * @param text A text of words.
   * @param keepDots If dots inside words are kept, then every first character after them will be
   *     capitalized.
   * @return A camel-case name.
   */
  public static String wordsToCamelCase(String text, boolean keepDots) {
    while (text.contains("  ")) {
      text = text.replace("  ", " ");
    }

    String bactrianCamel =
        Stream.of(text.split(keepDots ? "[^a-zA-Z0-9.]" : "[^a-zA-Z0-9]"))
            .map(v -> v.substring(0, 1).toUpperCase() + v.substring(1).toLowerCase())
            .collect(Collectors.joining());

    String dromedaryCamel = bactrianCamel.toLowerCase().charAt(0) + bactrianCamel.substring(1);

    return dromedaryCamel;
  }

  /**
   * Derives fieldName from getter method name, if getter follows the conventions.
   *
   * @param getterName Getter method name.
   * @return Field name the getter refers to.
   */
  public static String getterToCamelCase(String getterName) {
    return getterName.replaceFirst("get", "").substring(0, 1).toLowerCase()
        + getterName.replaceFirst("get", "").substring(1);
  }

  /**
   * Any char except for letter and digit is converted to hyphen. Multiple hyphens are substituted
   * by one.
   */
  public static String toKebabCase(@NotNull String text) {
    final StringBuilder[] kebabCase = {new StringBuilder()};
    StringBuilder finalKebabCase = kebabCase[0];
    final boolean[] isFirst = {true};
    String KEBAB_SEPARATOR = "-";
    text.chars()
        .mapToObj(c -> (char) c)
        .collect(Collectors.toList())
        .forEach(
            thisChar -> {
              boolean isSeparator = String.valueOf(thisChar).matches("[^a-z0-9]");
              boolean isCharSeparator = String.valueOf(thisChar).matches("[A-Z]");
              if (isCharSeparator) {
                if (!isFirst[0]) {
                  finalKebabCase.append(KEBAB_SEPARATOR);
                }
                finalKebabCase.append(String.valueOf(thisChar).toLowerCase());
              } else if (isSeparator) {
                finalKebabCase.append(KEBAB_SEPARATOR);
              } else {
                finalKebabCase.append(thisChar);
              }
              isFirst[0] = false;
            });

    String kebabCaseString = kebabCase[0].toString();

    // Remove double separators:
    while (kebabCaseString.contains("--")) {
      kebabCaseString = kebabCaseString.replace("--", KEBAB_SEPARATOR);
    }

    // Remove first char separator:
    if (String.valueOf(kebabCaseString.toCharArray()[0]).equals(KEBAB_SEPARATOR)) {
      kebabCaseString = kebabCaseString.substring(1, kebabCase.length);
    }

    // Remove last char separator:
    if (String.valueOf(kebabCaseString.toCharArray()[kebabCaseString.toCharArray().length - 1])
        .equals(KEBAB_SEPARATOR)) {
      kebabCaseString = kebabCaseString.substring(0, kebabCaseString.toCharArray().length - 1);
    }

    return kebabCaseString;
  }

  @SneakyThrows
  public static String toString(Reader reader) {
    char[] buffer = new char[4096];
    StringBuilder builder = new StringBuilder();
    int numChars;

    while ((numChars = reader.read(buffer)) >= 0) {
      builder.append(buffer, 0, numChars);
    }
    String result = builder.toString().replace(", null", "");
    return result;
  }

  public static String removeInvisibleCharacters(String text) {
    return text.replaceAll("\\p{C}", "");
  }
}
