package com.zonzonzon.common.utils;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/** Helper methods to work with native sql queries. */
@UtilityClass
@Slf4j
public class SqlUtils {

  // TODO: In WHERE clauses '= NULL' should be replaced with IS NULL. But for assignment - not.

  /**
   * Substitutes sql pattern variable as ":variableName" with the given value. Correctly processes
   * substitution if the value is NULL. Checks for input injection by removing dangerous signs.
   *
   * @param sqlPattern Original SQL-script with variable placeholders.
   * @param name Variable name to substitute.
   * @param value Variable value to substitute.
   * @param sanitize Remove dangerous injected signs in user input.
   * @return SQL-script with values in places.
   */
  public static String substituteVariable(
      String sqlPattern, String name, String value, boolean sanitize) {

    String sanitized = sanitize ? ValidationUtils.sanitizeInjection(value) : value;

    String resultingSql =
        sqlPattern.replaceAll(":" + name, value == null ? "NULL" : "'" + sanitized + "'");

    return resultingSql;
  }

  /**
   * Integer variable substitution. See {@link SqlUtils#substituteVariable(String, String, String,
   * boolean)}
   */
  public static String substituteVariable(String sqlPattern, String name, Integer value) {
    String resultingSql =
        sqlPattern.replaceAll(":" + name, value == null ? "NULL" : String.valueOf(value));
    return resultingSql;
  }

  /**
   * Double variable substitution. See {@link SqlUtils#substituteVariable(String, String, String,
   * boolean)}
   */
  public static String substituteVariable(String sqlPattern, String name, Double value) {
    String resultingSql =
        sqlPattern.replaceAll(":" + name, value == null ? "NULL" : String.valueOf(value));
    return resultingSql;
  }

  /**
   * UUID variable substitution. See {@link SqlUtils#substituteVariable(String, String, String,
   * boolean)}
   */
  public static String substituteVariable(String sqlPattern, String name, UUID value) {
    String resultingSql =
        sqlPattern.replaceAll(
            ":" + name, value == null ? "NULL" : "0x" + UuidUtils.toStringWithoutHyphens(value));
    return resultingSql;
  }

  /**
   * UUID List variable substitution. A IN (:myPlaceholder) is expected in a query. See {@link
   * SqlUtils#substituteVariable(String, String, String, boolean)}
   */
  public static String substituteVariable(String sqlPattern, String name, List<UUID> values) {

    String uuidStringsConcatenation =
        values.stream()
            .map(value -> value == null ? "NULL" : "0x" + UuidUtils.toStringWithoutHyphens(value))
            .collect(Collectors.joining(", "));

    String resultingSql = sqlPattern.replaceAll(":" + name, uuidStringsConcatenation);
    return resultingSql;
  }

  /**
   * Substitute pageable variables in the SQL-script with given values. Expected placeholders are:
   * ":offset" and ":limit".
   *
   * @param sqlPattern Initial SQL-script with placeholders.
   * @param offset Records skipped before the current page.
   * @param limit Records on a page.
   * @return SQL-script with pageable values.
   */
  public static String substitutePageable(String sqlPattern, long offset, int limit) {
    String resultingSql = sqlPattern;
    if (!resultingSql.contains(":offset") || !resultingSql.contains(":limit")) {
      log.warn(
          "Paging variables like ':offset' and ':limit' are not found "
              + "in the native query - won't be substituted");
      return resultingSql;
    }

    resultingSql = resultingSql.replace(":offset", String.valueOf(offset));
    resultingSql = resultingSql.replace(":limit", String.valueOf(limit));
    return resultingSql;
  }

  /**
   * When using assignment signs like := in SQL-scripts, Hibernate wouldn't treat them correctly.
   * They should be screened: Hibernate screening should be preceded by Java screening.
   */
  public static String screenAssignmentSigns(String sqlPattern) {
    String resultingSql = sqlPattern.replace(":=", "\\\\:=");
    return resultingSql;
  }
}
