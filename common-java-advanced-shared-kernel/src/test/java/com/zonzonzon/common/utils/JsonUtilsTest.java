package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;

public class JsonUtilsTest {

  @Test
  void _1_Properties_DotChained_AreConvertedToJsonTree() throws IOException {

    Map<String, Object> given = new HashMap<>();
    given.put("com.zonzonzon.parentA.childA", 123);
    given.put("com.zonzonzon.parentA.childB.propertyA", "text of child B");
    given.put("com.zonzonzon.parentA.childB.propertyB", null);
    given.put("com.zonzonzon.parentB.childA.propertyA", true);
    given.put("com.zonzonzon.parentB.childD.propertyA", 123.00);
    given.put("com.zonzonzon.parentB.childD.propertyC", 0);
    given.put("com.zonzonzon.parentB.childD.propertyD", "text of property D");

    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

    JsonNode expected =
        mapper.readTree(
            "{"
                + "  \"com\": {"
                + System.lineSeparator()
                + "    \"zonzonzon\": {"
                + System.lineSeparator()
                + "      \"parentA\": {"
                + System.lineSeparator()
                + "        \"childA\": 123,"
                + System.lineSeparator()
                + "        \"childB\": {"
                + System.lineSeparator()
                + "          \"propertyA\": \"text of child B\""
                + System.lineSeparator()
                + "        }"
                + System.lineSeparator()
                + "      },"
                + System.lineSeparator()
                + "      \"parentB\": {"
                + System.lineSeparator()
                + "        \"childD\": {"
                + System.lineSeparator()
                + "          \"propertyD\": \"text of property D\","
                + System.lineSeparator()
                + "          \"propertyA\": 123.0,"
                + System.lineSeparator()
                + "          \"propertyC\": 0"
                + System.lineSeparator()
                + "        },"
                + System.lineSeparator()
                + "        \"childA\": {"
                + System.lineSeparator()
                + "          \"propertyA\": true"
                + System.lineSeparator()
                + "        }"
                + System.lineSeparator()
                + "      }"
                + System.lineSeparator()
                + "    }"
                + System.lineSeparator()
                + "  }"
                + "}");

    JsonNode result = JsonUtils.toJsonNode(given, mapper);

    assertThat(expected.toString()).isEqualTo(result.toString());
  }

  @Test
  void collection_Mapped_ToString() {
    List<String> given = List.of("123", "abc", "Some Text");
    String result = JsonUtils.toString(given, null);
    assertEquals("[ \"123\", \"abc\", \"Some Text\" ]", result);
  }

  @Test
  void targetJsonNode_MergedWithUpdateJsonNode_IsOverriden() throws JsonProcessingException {
    JsonNode given =
        new ObjectMapper().readTree("{\"a\": \"value A\", \"b\": \"value B\", \"c\": \"value C\"}");

    JsonNode toUpdate = new ObjectMapper().readTree("{\"a\": \"value Z\", \"c\": \"value Y\"}");

    JsonNode result = JsonUtils.merge(given, toUpdate);

    assertThat(result.get("a").textValue()).isEqualTo("value Z");
    assertThat(result.get("b").textValue()).isEqualTo("value B");
    assertThat(result.get("c").textValue()).isEqualTo("value Y");
  }

  @Test
  void jsonString_OneLiner_IsPrettyPrinted() throws JsonProcessingException {
    String given = "{\"a\": \"value A\", \"b\": \"value B\", \"c\": \"value C\"}";
    String result = JsonUtils.prettyPrint(given);

    assertThat(result)
        .isEqualTo(
            "{\n"
                + "  \"a\" : \"value A\",\n"
                + "  \"b\" : \"value B\",\n"
                + "  \"c\" : \"value C\"\n"
                + "}");
  }
}
