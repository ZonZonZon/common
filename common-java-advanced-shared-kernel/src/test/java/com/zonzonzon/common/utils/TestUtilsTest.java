package com.zonzonzon.common.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Set;
import org.junit.jupiter.api.Test;

class TestUtilsTest {

  @Test
  void classes_WithEmbedded_SeveralObjectsAreGenerated() {
    Set<Example> result = TestUtils.getRandom(Example.class, 3);

    assertThat(result.size()).isEqualTo(3);
    assertThat(result.stream().findAny().orElse(null).number).isNotZero();
    assertThat(result.stream().findAny().orElse(null).text).isNotEmpty();
    assertThat(result.stream().findAny().orElse(null).embedded).isNotNull();
  }

  private static class Example{
    int number;
    String text;
    Embedded embedded;
  }

  private static class Embedded{
    String name;
  }
}