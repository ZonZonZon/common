package com.zonzonzon.common.service.benchmark;

import com.zonzonzon.common.utils.ReflectionUtils;
import java.lang.reflect.Method;
import java.time.Duration;
import java.time.ZonedDateTime;
import javax.validation.constraints.NotNull;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

@Slf4j
@Aspect
@Setter
public class BenchmarkAspect {

  @Around("@annotation(com.zonzonzon.common.service.benchmark.Benchmark)")
  public Object decorateAnnotatedMethod(@NotNull ProceedingJoinPoint joinPoint) {
    // Do before the join point:
    ZonedDateTime startTime = ZonedDateTime.now();
    MethodSignature signature = (MethodSignature) joinPoint.getSignature();
    Method method = signature.getMethod();

    // Execute the join point:
    Object result = null;
    try {
      result = joinPoint.proceed();
    } catch (Throwable throwable) {
      log.error(
          "Benchmark aspect error. Remove @Benchmark to see which method throws an Exception.\n{}",
          throwable.getLocalizedMessage());
    }

    // Do after the join point:
    doBenchmark(startTime, method.getName());
    return result;
  }

  /**
   * Log duration from the start to the current benchmark.
   *
   * @param startTime The start time.
   * @param postfix A postfix to be added to a benchmark to better find it in logs.
   * @return The current benchmark time to be passed as the start time for the next benchmark.
   */
  public static ZonedDateTime doBenchmark(ZonedDateTime startTime, String postfix) {
    ZonedDateTime endTime = ZonedDateTime.now();
    long milliseconds = Duration.between(startTime, endTime).toMillis();

    log.info(
        "\n********** "
            + milliseconds
            + " ms - "
            + ReflectionUtils.getCallerClassName(3)
            + (postfix == null ? "" : "#" + postfix)
            + " - "
            + startTime
            + " - "
            + endTime);

    return endTime;
  }

  /**
   * An alias for {@link BenchmarkAspect#doBenchmark(ZonedDateTime, String)}
   */
  public static ZonedDateTime doBenchmark(ZonedDateTime startTime) {
    return doBenchmark(startTime, null);
  }
}
