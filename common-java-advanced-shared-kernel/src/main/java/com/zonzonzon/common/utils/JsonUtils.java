package com.zonzonzon.common.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

/** Utilities for JSON structures. */
@UtilityClass
@Slf4j
public class JsonUtils {

  /**
   * Converts a key-value list into a node tree.
   *
   * @param propertyToPrimitiveValue A map with dot-chained properties as keys and values as
   *     objects. Each name part should correspond enclosed object-field name. Last name part can
   *     point to a primitive or to an object.
   * @param objectMapper Optional, Nullable. If object mapper instance with special settings should
   *     be used.
   */
  public JsonNode toJsonNode(
      Map<String, Object> propertyToPrimitiveValue, ObjectMapper objectMapper) {

    ObjectMapper mapper = objectMapper != null ? objectMapper : new ObjectMapper();
    JsonNode tree = mapper.createObjectNode();

    propertyToPrimitiveValue
        .keySet()
        .forEach(
            key -> {
              String[] keyParts = key.split("\\.");
              JsonNode treeLocation = tree;
              for (int keyPartIndex = 0; keyPartIndex < keyParts.length; keyPartIndex++) {
                String keyPart = keyParts[keyPartIndex];

                String nextKeyPart =
                    keyPartIndex < keyParts.length - 1 ? keyParts[keyPartIndex + 1] : null;

                Object value = propertyToPrimitiveValue.get(key);

                if (nextKeyPart == null) {
                  // Insert final path value:
                  if (value instanceof Short) {
                    ((ObjectNode) treeLocation).put(keyPart, (Short) value);
                  } else if (value instanceof Integer) {
                    ((ObjectNode) treeLocation).put(keyPart, (Integer) value);
                  } else if (value instanceof Long) {
                    ((ObjectNode) treeLocation).put(keyPart, (Long) value);
                  } else if (value instanceof Float) {
                    ((ObjectNode) treeLocation).put(keyPart, (Float) value);
                  } else if (value instanceof Double) {
                    ((ObjectNode) treeLocation).put(keyPart, (Double) value);
                  } else if (value instanceof BigDecimal) {
                    ((ObjectNode) treeLocation).put(keyPart, (BigDecimal) value);
                  } else if (value instanceof BigInteger) {
                    ((ObjectNode) treeLocation).put(keyPart, (BigInteger) value);
                  } else if (value instanceof String) {
                    ((ObjectNode) treeLocation).put(keyPart, (String) value);
                  } else if (value instanceof Boolean) {
                    ((ObjectNode) treeLocation).put(keyPart, (Boolean) value);
                  } else if (value != null) {
                    ((ObjectNode) treeLocation).putObject(keyPart);
                  }
                } else if (treeLocation.get(keyPart) == null && treeLocation.isObject()) {
                  // Insert path object:
                  treeLocation = ((ObjectNode) treeLocation).putObject(keyPart);
                } else {
                  // Move to next location:
                  treeLocation = treeLocation.get(keyPart);
                }
              }
            });

    return tree;
  }

  /**
   * Converts collection into Jackson JSON array.
   *
   * @param collection Any collection.
   * @param objectMapper A mapper to use. May be NULL.
   * @param <T> Type of collection content.
   * @return Jackson JSON array.
   */
  public <T> ArrayNode toJsonArray(Collection<T> collection, ObjectMapper objectMapper) {
    ObjectMapper mapper = objectMapper != null ? objectMapper : new ObjectMapper();
    ArrayNode arrayNode = mapper.createArrayNode();

    List<JsonNode> jsonNodes =
        collection.stream()
            .map(pojo -> (JsonNode) mapper.valueToTree(pojo))
            .collect(Collectors.toList());

    arrayNode.addAll(jsonNodes);
    return arrayNode;
  }

  /**
   * Converts collection into JSON array String.
   *
   * @param collection Collection of primitives or POJOs.
   * @param <T> Type of collection content.
   * @return Jackson JSON array.
   */
  public <T> String toString(Collection<T> collection, ObjectMapper objectMapper) {
    ObjectMapper mapper = objectMapper != null ? objectMapper : new ObjectMapper();
    ArrayNode arrayNode = toJsonArray(collection, mapper);
    String result = null;
    try {
      result = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(arrayNode);
    } catch (JsonProcessingException e) {
      log.error("Unable to parse a node into a string. See: {}", e.getLocalizedMessage());
    }
    return result;
  }

  /**
   * Merges update JSON-node into main JSON-node, overriding it.
   *
   * @param mainNode Initial JSON-node.
   * @param updateNode Overriding JSON-node.
   * @return Merged JSON-node.
   */
  public static JsonNode merge(JsonNode mainNode, JsonNode updateNode) {
    Iterator<String> fieldNames = updateNode.fieldNames();
    while (fieldNames.hasNext()) {
      String fieldName = fieldNames.next();
      JsonNode jsonNode = mainNode.get(fieldName);
      // If field exists and is an embedded object:
      if (jsonNode != null && jsonNode.isObject()) {
        merge(jsonNode, updateNode.get(fieldName));
      } else {
        if (mainNode instanceof ObjectNode) {
          // Overwrite field:
          JsonNode value = updateNode.get(fieldName);
          ((ObjectNode) mainNode).set(fieldName, value);
        }
      }
    }
    return mainNode;
  }

  /**
   * Pretty prints a given JSON string.
   *
   * @param json Given JSON string.
   * @return A Pretty printed JSON string.
   */
  @SneakyThrows
  public static String prettyPrint(String json) {
    String prettyString = json;
    try {
      if (json == null || json.isEmpty()) {
        return "";
      }
      ObjectMapper mapper = new ObjectMapper();
      JsonNode tree = mapper.readTree(json);
      prettyString = tree.toPrettyString();
    } catch (Exception e) {
      log.error("Not able to pretty print a JSON string");
    }
    return prettyString;
  }
}
