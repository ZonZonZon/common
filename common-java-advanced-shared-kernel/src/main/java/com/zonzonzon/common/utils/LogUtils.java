package com.zonzonzon.common.utils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

@UtilityClass
@Slf4j
public class LogUtils {

  /**
   * Builds a message to log. Missing parameters are substituted with "?" sign.
   *
   * @param method HTTP method.
   * @param uri URI excluding host and port.
   * @param queryParameters Query parameters after "?" sign.
   * @param headers Headers passed with the query.
   * @param ssl SSL used.
   * @param addressFrom Client IP address.
   * @param remoteAddressTo Server external IP address.
   * @param localAddressTo Server internal IP address.
   * @param cookies Cookies applied to the request.
   * @param requestBody Request body.
   */
  public static void logRequest(
      String method,
      String uri,
      String queryParameters,
      String headers,
      String ssl,
      String addressFrom,
      String remoteAddressTo,
      String localAddressTo,
      String cookies,
      String requestBody) {

    logWithNonEmpty(
        "REST request "
            + method
            + " "
            + uri
            + " from "
            + addressFrom
            + " to "
            + remoteAddressTo
            + " ("
            + localAddressTo
            + ") with parameters ["
            + queryParameters
            + "], headers ["
            + headers
            + "], SSL "
            + ssl
            + ", cookies ["
            + cookies
            + "], and the body "
            + requestBody);
  }

  /**
   * Builds a message to log. Missing parameters are substituted with "?" sign.
   *
   * @param statusCode HTTP-response status.
   * @param headers HTTP-response headers.
   * @param responseBody Response body.
   * @param isToPrettyPrint Pretty printing can be disabled for external logging systems where each
   *     line can be displayed as a separate log entry.
   */
  public static void logResponse(
      Integer statusCode, String headers, String responseBody, boolean isToPrettyPrint) {

    String body =
        isToPrettyPrint
            ? JsonUtils.prettyPrint(
                StringUtils.removeInvisibleCharacters(
                    responseBody.startsWith("<!DOCTYPE html>") ? "" : responseBody))
            : StringUtils.removeInvisibleCharacters(
                responseBody.startsWith("<!DOCTYPE html>") ? "" : responseBody);

    logWithNonEmpty(
        "REST response with HTTP status code {}, headers [{}], and the body {}",
        String.valueOf(statusCode),
        headers,
        body);
  }

  /**
   * An alias to replace missing parameters with a replacement sign.
   *
   * @param parameters Parameters passed.
   */
  private static void logWithNonEmpty(String logText, String... parameters) {
    final String REPLACEMENT = "?";

    List<String> nonEmptyParameters =
        Arrays.stream(parameters)
            .map(
                parameter ->
                    parameter = parameter == null || parameter.isEmpty() ? REPLACEMENT : parameter)
            .collect(Collectors.toList());

    String[] replacedText = {logText};

    nonEmptyParameters.forEach(
        parameter ->
            replacedText[0] =
                replacedText[0].replaceFirst(
                    Pattern.quote("{}"), Matcher.quoteReplacement(parameter)));

    log.info(replacedText[0]);
  }
}
