package com.zonzonzon.common.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import java.util.HashSet;
import java.util.Set;
import lombok.experimental.UtilityClass;
import org.jeasy.random.EasyRandom;
import org.jeasy.random.EasyRandomParameters;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

/** Helper methods to work with tests. */
@UtilityClass
public class TestUtils {

  private static final EasyRandom GENERATOR;

  static {
    EasyRandomParameters parameters = new EasyRandomParameters();
    parameters.stringLengthRange(3, 6);
    parameters.collectionSizeRange(3, 5);
    parameters.randomize(JsonNode.class, JsonNodeFactory.instance::objectNode);
    GENERATOR = new EasyRandom(parameters);
  }

  /**
   * Converts CSV-array of values to an array for parameterized classes with @CsvSource and arrays
   * passed as values.
   */
  public static class CollectionConverter extends SimpleArgumentConverter {

    @Override
    protected Object convert(Object source, Class<?> targetType)
        throws ArgumentConversionException {

      String csvArray = (String) source;
      return csvArray.split("\\s*,\\s*");
    }
  }

  /**
   * Generates any amount of POJOs of some type. Embedded entities are also generated.
   *
   * @param type Type of POJO to generate.
   * @param amount Amount of POJOs to generate.
   * @param <T> Type of POJO.
   * @return A collection of generated POJOs.
   */
  public static <T> Set<T> getRandom(Class<T> type, int amount) {
    Set<T> pojos = new HashSet<>();
    while (amount > 0) {
      pojos.add(GENERATOR.nextObject(type));
      amount--;
    }
    return pojos;
  }

  public static <T> T getRandom(Class<T> type) {
    return GENERATOR.nextObject(type);
  }
}
