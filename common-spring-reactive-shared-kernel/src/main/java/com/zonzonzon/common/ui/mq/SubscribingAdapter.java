package com.zonzonzon.common.ui.mq;

/**
 * Common type for all specific Google Pub/Sub Adapters that subscribe to topics.
 *
 * @param <I> Incoming message type.
 * @param <R> Message processing result type.
 */
public interface SubscribingAdapter<I, R> {

  /** Subscription name. */
  String getSubscription();

  /**
   * Processing of specific incoming message type.
   *
   * @param incomingMessage Message of expected type.
   */
  R processMessage(I incomingMessage);
}
