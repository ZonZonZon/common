package com.zonzonzon.common.ui.mq;

import javax.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

/** Common type for all specific Google Pub/Sub Adapters to publish to a topic. */
@Validated
public interface PublishingAdapter {

  /**
   * A message to be logged after execution of the {@link PublishingAdapter#publish(Object,
   * String, String, String)} method.
   */
  String PUBLISHED_MESSAGE_LOG_TEXT =
      "************************** A message is published to the topic {}: {}";

  /** Topic name. */
  String getTopic();

  /**
   * Publish message to the topic specified for current adapter.
   *
   * @param <P> Payload type.
   */
  <P> void publish(
      @NotNull P payload,
      @NotNull String resourceName,
      @NotNull String actionName,
      @NotNull String token);
}
