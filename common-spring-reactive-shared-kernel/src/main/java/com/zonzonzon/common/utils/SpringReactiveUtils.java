package com.zonzonzon.common.utils;

import static com.zonzonzon.common.utils.ReflectionUtils.getSafe;
import static com.zonzonzon.common.utils.RestUtils.API_KEY_LOGGER_NAME;
import static com.zonzonzon.common.utils.RestUtils.TRACE_ID_LOGGER_NAME;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.slf4j.MDC.MDCCloseable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;
import reactor.util.context.Context;
import reactor.util.context.ContextView;

/**
 * Helper methods to pass values across multiple threads in Reactive Streams application. Used for
 * logging in MQ-originated operations chain.
 */
@UtilityClass
@Slf4j
public class SpringReactiveUtils {

  /**
   * Relays API Key and Trace ID from reactive context to MDC. Allows printing out these
   * values in logs within current reactive context.
   */
  private static <T> void relayFromContextToMdc(Consumer<T> logMethod, Signal<T> signal) {
    String contextApiKey = getSafe(() -> signal.getContextView().get(API_KEY_LOGGER_NAME));
    String contextTraceId =
        getSafe(() -> signal.getContextView().get(TRACE_ID_LOGGER_NAME));

    try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
        MDCCloseable mdcTraceId = MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId); ) {
      logMethod.accept(signal.get());
    }
  }

  /**
   * Alternative to {@link SpringReactiveUtils#runFunctionInContext(Function, Mono)} but for current
   * function context.
   *
   * @deprecated Doesn't work as expected for now.
   */
  @Deprecated(forRemoval = true)
  public static Mono<?> propagateContext() {

    return getCurrentContext()
        .map(
            context -> {
              String contextApiKey = context.isEmpty() ? "" : context.get(API_KEY_LOGGER_NAME);
              String contextTraceId =
                  context.isEmpty() ? "" : context.get(TRACE_ID_LOGGER_NAME);

              try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
                  MDCCloseable mdcTraceId =
                      MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

                return Mono.just(context);
              } catch (Exception functionAppliedFailure) {
                log.error(
                    "Failed to propagate context into a function: {}",
                    functionAppliedFailure.getLocalizedMessage());
              }
              return Mono.just(context);
            });
  }

  /**
   * Wrap your function with this method if you want all your context variables to be logged inside.
   * Waits for passed monos and applies their results to a given function.
   *
   * @param function Function with logging inside.
   * @param parametersMono One parameter passed to function.
   * @param <R> Function return type.
   * @return A return value from function.
   */
  public static <R> Mono<R> runFunctionInContext(
      Function<List<?>, R> function, Mono<?> parametersMono) {

    return Mono.zip(getCurrentContext(), parametersMono)
        .map(
            tuple -> {
              ContextView context = tuple.getT1();
              String contextApiKey = context.isEmpty() ? "" : context.get(API_KEY_LOGGER_NAME);
              String contextTraceId =
                  context.isEmpty() ? "" : context.get(TRACE_ID_LOGGER_NAME);

              R returnValue = null;
              try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
                  MDCCloseable mdcTraceId =
                      MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

                returnValue = function.apply(List.of(tuple.getT2()));
              } catch (Exception functionAppliedFailure) {
                log.error(
                    "Failed to propagate context into a function: {}",
                    functionAppliedFailure.getLocalizedMessage());
              }
              return returnValue;
            });
  }

  /**
   * Alternative to {@link SpringReactiveUtils#runFunctionInContext(Function, Mono)} but with
   * non-reactive parameters.
   */
  public static <R> Mono<R> runFunctionInContext(
      Function<List<?>, R> function, List<?> parameters) {

    return getCurrentContext()
        .map(
            context -> {
              String contextApiKey = context.isEmpty() ? "" : context.get(API_KEY_LOGGER_NAME);
              String contextTranceId =
                  context.isEmpty() ? "" : context.get(TRACE_ID_LOGGER_NAME);

              R returnValue = null;
              try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
                  MDCCloseable mdcTraceId =
                      MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTranceId)) {

                returnValue = function.apply(parameters);
              } catch (Exception functionAppliedFailure) {
                log.error(
                    "Failed to propagate context into a function: {}",
                    functionAppliedFailure.getLocalizedMessage());
              }
              return null;
            });
  }

  /**
   * @deprecated Doesn't work as expected for now.
   */
  @Deprecated(forRemoval = true)
  public static <R> Mono<R> runFunctionInContext(
      Function<List<?>, R> function, Flux<?> parametersFlux) {

    return Mono.zip(getCurrentContext(), parametersFlux.collectList())
        .map(
            tuple -> {
              ContextView context = tuple.getT1();
              String contextApiKey = context.isEmpty() ? "" : context.get(API_KEY_LOGGER_NAME);
              String contextTraceId =
                  context.isEmpty() ? "" : context.get(TRACE_ID_LOGGER_NAME);

              R returnValue = null;
              try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
                  MDCCloseable mdcTraceId =
                      MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

                returnValue = function.apply(tuple.getT2());
              } catch (Exception functionAppliedFailure) {
                log.error(
                    "Failed to propagate context into a function: {}",
                    functionAppliedFailure.getLocalizedMessage());
              }
              return returnValue;
            });
  }

  /** Retrieves current reactive method context if it exists. */
  public static Mono<ContextView> getCurrentContext() {
    Mono<ContextView> context = getSafe(() -> Mono.deferContextual(Mono::just));
    return context != null ? context : Mono.just(Context.empty());
  }

  /** Creates a new context from a non-reactive method and put MDC values for logging into it. */
  public Context createContext() {
    return Context.of(
        Map.of(
            API_KEY_LOGGER_NAME,
            Optional.ofNullable(getSafe(() -> MDC.get(API_KEY_LOGGER_NAME))).orElse(""),
            TRACE_ID_LOGGER_NAME,
            Optional.ofNullable(getSafe(() -> MDC.get(TRACE_ID_LOGGER_NAME))).orElse("")));
  }

  /** Creates a new context from a non-reactive method and put MDC values for logging into it. */
  public Context createContext(String apiKey, String traceId) {
    return Context.of(Map.of(API_KEY_LOGGER_NAME, apiKey, TRACE_ID_LOGGER_NAME, traceId));
  }

  /**
   * Alternative to {@link SpringReactiveUtils#runFunctionInContext(Function, Mono)} but with
   * non-reactive parameters.
   */
  public static <T> T runFunctionInContext(
      ContextView context, Function<List<?>, T> function, Object... parameters) {

    String contextApiKey = context.get(API_KEY_LOGGER_NAME);
    String contextTraceId = context.get(TRACE_ID_LOGGER_NAME);
    T returnValue;
    try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
        MDCCloseable mdcTraceId =
            MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

      returnValue = function.apply(List.of(parameters));
    }
    return returnValue;
  }

  /**
   * Alternative to {@link SpringReactiveUtils#runFunctionInContext(Function, Mono)} but with
   * non-reactive parameters.
   */
  public static void runVoidInContext(
      ContextView context, Consumer<List<?>> voidFunction, Object... parameters) {

    String contextApiKey = context.get(API_KEY_LOGGER_NAME);
    String contextTraceId = context.get(TRACE_ID_LOGGER_NAME);
    try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
        MDCCloseable mdcTraceId =
            MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

      voidFunction.accept(List.of(parameters));
    }
  }

  /** An alias for {@link SpringReactiveUtils#logInfo(String, ContextView)} ()} */
  public static Void logInfo(String text) {
    getCurrentContext().map(contextView -> logInfo(text, contextView)).subscribe(unused -> {});
    return null;
  }

  /**
   * Log info with reactive context. Alternative to {@link
   * SpringReactiveUtils#runFunctionInContext(Function, Mono)}.
   */
  public static Void logInfo(String text, ContextView context) {

    String contextApiKey = context.get(API_KEY_LOGGER_NAME);
    String contextTraceId = context.get(TRACE_ID_LOGGER_NAME);
    try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
        MDCCloseable mdcTraceId =
            MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

      log.info(text);
    }
    return null;
  }

  /** An alias for {@link SpringReactiveUtils#logError(String, ContextView)} ()} */
  public static Void logError(String text) {
    getCurrentContext().map(contextView -> logError(text, contextView)).subscribe(unused -> {});
    return null;
  }

  /**
   * Log error with reactive context. Alternative to {@link
   * SpringReactiveUtils#runFunctionInContext(Function, Mono)}.
   */
  public static Void logError(String text, ContextView context) {

    String contextApiKey = context.get(API_KEY_LOGGER_NAME);
    String contextTraceId = context.get(TRACE_ID_LOGGER_NAME);
    try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
        MDCCloseable mdcTraceId =
            MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

      log.error(text);
    }
    return null;
  }

  /** An alias for {@link SpringReactiveUtils#logWarn(String, ContextView)} ()} */
  public static Void logWarn(String text) {
    getCurrentContext().map(contextView -> logWarn(text, contextView)).subscribe(unused -> {});
    return null;
  }

  /**
   * Log warning with reactive context. Alternative to {@link
   * SpringReactiveUtils#runFunctionInContext(Function, Mono)}.
   */
  public static Void logWarn(String text, ContextView context) {

    String contextApiKey = context.get(API_KEY_LOGGER_NAME);
    String contextTraceId = context.get(TRACE_ID_LOGGER_NAME);
    try (MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
        MDCCloseable mdcTraceId =
            MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

      log.warn(text);
    }
    return null;
  }

  /**
   * A reactive wrapper around MDC to add request log values (Api Key & Trace Id) when
   * response context is continued in another thread. Use like this: <code>
   *     Flux<Boolean> isOk =
   *         Flux.just("ok", "not ok")
   *             .map(text -> Pair.of(text.equals("ok"), text))
   *             .doOnEach(
   *                 logOnNext(
   *                     results -> {
   *                       log.info("OK = {} ? {} ", results.getFirst(), results.getSecond());
   *                     }))
   *             .map(Pair::getFirst);
   * </code>
   *
   * @param logMethod Logging operations.
   * @param <T> Type of data passed a logging method.
   * @return Logging method returns the same type that was passed.
   */
  public static <T> Consumer<Signal<T>> logOnNext(Consumer<T> logMethod) {
    return signal -> {
      if (!signal.isOnNext()) return;
      relayFromContextToMdc(logMethod, signal);
    };
  }

  public static <T> Consumer<Signal<T>> logOnComplete(Consumer<T> logMethod) {
    return signal -> {
      if (!signal.isOnComplete()) return;
      relayFromContextToMdc(logMethod, signal);
    };
  }

  public static <T> Consumer<Signal<T>> logOnError(Consumer<T> logMethod) {
    return signal -> {
      if (!signal.isOnError()) return;
      relayFromContextToMdc(logMethod, signal);
    };
  }

  public static <T> Consumer<Signal<T>> logOnCompleteOrError(
      Consumer<T> logOnCompleteMethod, Consumer<T> logOnErrorMethod) {

    return signal -> {
      if (signal.isOnComplete()) {
        logOnComplete(logOnCompleteMethod);
      }
      if (signal.isOnError()) {
        logOnError(logOnErrorMethod);
      }
    };
  }

  /**
   * Log on any reactive response signal type. See {@link SpringReactiveUtils#logOnNext(Consumer)}
   */
  public static <T> Consumer<Signal<T>> logOnEach(Consumer<T> logMethod) {
    return signal -> relayFromContextToMdc(logMethod, signal);
  }

  /**
   * Pass variables for logging into MDC and Reactive context without specifically logging something
   * for now.
   */
  public static <T> Consumer<Signal<T>> logContext() {
    return signal -> relayFromContextToMdc(x -> {}, signal);
  }

  /**
   * Adds given key-value to the context by creating a new Context with given key-value and adding
   * all the preexisting context values.
   *
   * @param key A context to be added.
   * @param value A context value to be added.
   * @return A context to be attached to your subscriber.
   */
  public static Context setReactiveContextValue(String key, Object value) {
    ContextView currentContext = getCurrentContext().block();
    Context enrichedContext = null;
    if (key!= null && value != null) {
      enrichedContext = Context.of(key, value);
    }
    if (currentContext != null && enrichedContext != null) {
      enrichedContext.putAll(currentContext);
      return enrichedContext;
    }
    if (currentContext != null) {
      return Context.of(currentContext);
    }
    return Context.empty();
  }

  /**
   * Gets a value from the current reactive context.
   *
   * @param key A key to find a value.
   * @return A value.
   */
  public static Object getReactiveContextValue(String key) {
    return getSafe(() -> getCurrentContext().block().get(key));
  }
}
