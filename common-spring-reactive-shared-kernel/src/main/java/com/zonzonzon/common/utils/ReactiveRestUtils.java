package com.zonzonzon.common.utils;

import static com.zonzonzon.common.utils.ReflectionUtils.getCaller;
import static com.zonzonzon.common.utils.ReflectionUtils.getMethod;
import static com.zonzonzon.common.utils.ReflectionUtils.getMethodName;
import static com.zonzonzon.common.utils.RestUtils.API_KEY_LOGGER_NAME;
import static com.zonzonzon.common.utils.RestUtils.TRACE_ID_LOGGER_NAME;
import static java.util.stream.Collectors.joining;
import static org.springframework.http.MediaType.APPLICATION_JSON;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zonzonzon.common.ui.response.AcceptedResponse;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.result.method.RequestMappingInfo;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilterChain;
import org.springframework.web.util.pattern.PathPattern;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;
import reactor.util.context.Context;

@UtilityClass
@Slf4j
public class ReactiveRestUtils {
  /**
   * An alias for {@link ReactiveRestUtils#getAcceptedServerResponse(Mono, ObjectMapper)}
   * Provides only an id of a resource object.
   */
  public Mono<ServerResponse> getAcceptedServerResponseId(
      Mono<UUID> resourceIdMono, ObjectMapper mapper) {

    Mono<String> acceptedResponseMono =
        resourceIdMono.flatMap(
            resourceId -> Mono.just(convertAcceptedResponseToJson(mapper, resourceId)));

    return ServerResponse.accepted()
        .contentType(APPLICATION_JSON)
        .body(acceptedResponseMono, String.class);
  }

  /**
   * Standard reactive response on accepted operation over some resource.
   *
   * @param resourceMono A stream providing resource.
   * @param mapper Mapper to be used for JSON conversion.
   * @return A response structure on accepted resource operation. Contains resource identifier.
   */
  public <T> Mono<ServerResponse> getAcceptedServerResponse(
      Mono<T> resourceMono, ObjectMapper mapper) {

    Mono<String> acceptedResponseMono =
        resourceMono.flatMap(
            resource -> Mono.just(convertAcceptedResponseToJson(mapper, resource)));

    return ServerResponse.accepted()
        .contentType(APPLICATION_JSON)
        .body(acceptedResponseMono, String.class);
  }

  private static String convertAcceptedResponseToJson(ObjectMapper mapper, UUID resourceId) {
    String responseJson = "";
    try {
      responseJson = mapper.writeValueAsString(AcceptedResponse.builder().id(resourceId).build());
    } catch (Exception jsonException) {
      log.error(
          "Unable to convert accepted response to JSON: {}", jsonException.getLocalizedMessage());
    }
    return responseJson;
  }

  private static <T> String convertAcceptedResponseToJson(ObjectMapper mapper, T resource) {
    String responseJson = "";
    try {
      responseJson = mapper.writeValueAsString(resource);
    } catch (Exception jsonException) {
      log.error(
          "Unable to convert accepted response to JSON: {}", jsonException.getLocalizedMessage());
    }
    return responseJson;
  }

  /**
   * Relays API Key and Trace ID from reactive context to MDC. Allows printing out these
   * values in logs within current reactive context.
   */
  private static <T> void relayFromContextToMdc(Consumer<T> logMethod, Signal<T> signal) {
    String contextApiKey = signal.getContextView().get(API_KEY_LOGGER_NAME);
    String contextTraceId = signal.getContextView().get(TRACE_ID_LOGGER_NAME);
    try (MDC.MDCCloseable mdcApiKey = MDC.putCloseable(API_KEY_LOGGER_NAME, contextApiKey);
        MDC.MDCCloseable mdcTraceId = MDC.putCloseable(TRACE_ID_LOGGER_NAME, contextTraceId)) {

      logMethod.accept(signal.get());
    }
  }

  /**
   * Adds header value to reactive context. Also adds it to the response.
   *
   * @param exchange Current REST communication exchange.
   * @param chain Current filter chain.
   * @param requestHeaders Current request headers.
   * @param headerName Request header name.
   * @param loggerName Variable name in Logback.
   * @return Mono without a return type.
   */
  public Mono<Void> addHeaderToReactiveContext(
      ServerWebExchange exchange,
      WebFilterChain chain,
      HttpHeaders requestHeaders,
      String headerName,
      String loggerName) {

    String value =
        Optional.ofNullable(requestHeaders.getFirst(headerName)).stream().findAny().orElse(null);

    if (value == null) {
      // Not added:
      // Clear MDC from previous request value in case a Netty app is used as non-reactive and
      // one-threaded:
      MDC.remove(loggerName);
      return chain.filter(exchange);
    } else {
      // Added:
      // Also putting into MDC in case a Netty app is used as non-reactive and one-threaded:
      MDC.put(loggerName, value);
      exchange.getResponse().getHeaders().add(headerName, value);
      return chain.filter(exchange).contextWrite(Context.of(loggerName, value));
    }
  }

  public Mono<Void> addHeaderToReactiveContext(
      ServerWebExchange exchange,
      WebFilterChain chain,
      HttpHeaders requestHeaders,
      Set<Pair<String, String>> headerNamesAndLoggerNames) {

    List<Mono<Void>> monos = headerNamesAndLoggerNames.stream()
        .map(
            headerNameAndLoggerName ->
                addHeaderToReactiveContext(
                    exchange,
                    chain,
                    requestHeaders,
                    headerNameAndLoggerName.getFirst(),
                    headerNameAndLoggerName.getSecond()))
        .collect(Collectors.toList());

    return Mono.zip(monos, objects -> null);
  }

  /**
   * Adds header value to reactive context.
   *
   * @param exchange Current REST communication exchange.
   * @param chain Current filter chain.
   * @param requestHeaders Current request headers.
   * @param headerName Request header name.
   * @param loggerName Variable name in Logback.
   * @return Mono without a return type.
   */
  public Mono<Void> addHeaderToReactiveContext(
      ServerWebExchange exchange,
      GatewayFilterChain chain,
      HttpHeaders requestHeaders,
      String headerName,
      String loggerName) {

    String value =
        Optional.ofNullable(requestHeaders.getFirst(headerName)).stream().findAny().orElse(null);

    if (value == null) {
      // Not added:
      // Clear MDC from previous request value in case a Netty app is used as non-reactive and
      // one-threaded:
      MDC.remove(loggerName);
      return chain.filter(exchange);
    } else {
      // Added:
      // Also putting into MDC in case a Netty app is used as non-reactive and one-threaded:
      MDC.put(loggerName, value);
      return chain.filter(exchange).contextWrite(Context.of(loggerName, value));
    }
  }

  public Mono<Void> addHeaderToReactiveContext(
      ServerWebExchange exchange,
      GatewayFilterChain chain,
      HttpHeaders requestHeaders,
      Set<Pair<String, String>> headerNamesAndLoggerNames) {

    List<Mono<Void>> monos = headerNamesAndLoggerNames.stream()
        .map(
            headerNameAndLoggerName ->
                addHeaderToReactiveContext(
                    exchange,
                    chain,
                    requestHeaders,
                    headerNameAndLoggerName.getFirst(),
                    headerNameAndLoggerName.getSecond()))
        .collect(Collectors.toList());

    return Mono.zip(monos, objects -> null);
  }

  /**
   * A single-thread REST request implementation to get header value from logger MDC.
   *
   * @param httpHeaders Current headers.
   * @param mdcKey MDC key.
   * @param headerName Header name.
   */
  public static void addHeaderFromMdcValue(
      HttpHeaders httpHeaders, String mdcKey, String headerName) {

    String mdcValue = MDC.get(mdcKey);
    if (mdcValue != null) {
      if (httpHeaders.get(headerName) != null) {
        httpHeaders.remove(headerName);
      }
      httpHeaders.add(headerName, mdcValue);
    }
  }

  /**
   * Distinguishes between read and update queries in controller.
   *
   * @param callerClass Controller class.
   * @param traceShift Stack trace shift from current position to the endpoint method order.
   * @return This is an create-update-delete endpoint.
   */
  public static boolean isUpdateRequest(Class<?> callerClass, int traceShift) {
    Method method = getMethod(getMethodName(getCaller(traceShift)), callerClass, true);
    RestQueryType queryType = getRequestType(method);
    return queryType != null && !queryType.equals(RestQueryType.GET);
  }

  private static RestQueryType getRequestType(Method method) {
    return Arrays.stream(method.getAnnotations())
        .map(
            annotation ->
                RestQueryType.findByAnnotationName(annotation.annotationType().getSimpleName()))
        .filter(Objects::nonNull)
        .findAny()
        .orElse(null);
  }

  private static String getRequestType(RequestMappingInfo path) {
    return path.getMethodsCondition().getMethods().stream().map(Enum::toString).collect(joining());
  }

  /**
   * Builds an endpoint link.
   *
   * @param endpoint Endpoint request information.
   * @param placeholders Request path variables to be substituted.
   * @return A string representation of endpoint request for HATEOAS link HREF-field.
   */
  private String getHref(RequestMappingInfo endpoint, Map<String, String> placeholders) {

    return endpoint.getPatternsCondition().getPatterns().stream()
        .map(PathPattern::toString)
        .map(
            href -> {
              final String[] newHref = {href};
              placeholders.forEach((key, value) -> newHref[0] = href.replace(key, value));
              return newHref[0];
            })
        .collect(Collectors.joining());
  }
}
