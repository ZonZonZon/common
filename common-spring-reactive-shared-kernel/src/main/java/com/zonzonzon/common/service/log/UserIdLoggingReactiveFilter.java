package com.zonzonzon.common.service.log;

import static com.zonzonzon.common.utils.RestUtils.USER_ID_HEADER_NAME;
import static com.zonzonzon.common.utils.RestUtils.USER_ID_LOGGER_NAME;

import com.zonzonzon.common.utils.ReactiveRestUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

/** Intercepts REST queries to put a User ID from header to reactive context. */
@Component
@Slf4j
@RequiredArgsConstructor
public class UserIdLoggingReactiveFilter implements WebFilter, Ordered {

  @Value("${com.zonzonzon.common.filters.user-id-logging-reactive-filter.enabled:false}")
  private boolean isEnabled;

  @Override
  public int getOrder() {
    return -50;
  }

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    if (!isEnabled) {
      return chain.filter(exchange);
    }
    // User is defined by token in the Sentinel.
    ServerHttpRequest request = exchange.getRequest();

    Mono<Void> filtered =
        ReactiveRestUtils.addHeaderToReactiveContext(
            exchange, chain, request.getHeaders(), USER_ID_HEADER_NAME, USER_ID_LOGGER_NAME);

    return filtered;
  }
}
