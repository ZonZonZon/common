package com.zonzonzon.common.service.exception;

import com.zonzonzon.common.ui.response.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;

/** A filter to handle exceptions thrown within a REST request. */
@Component
@Slf4j
public class ExceptionHandlingReactiveFilter {

  /**
   * A filter for a WebClient.
   *
   * @return Function to be applied when filter is attached to a reactive WebClient. A reactive
   *     implementation. The filter is expected to be * attached in you WebConfiguration WebClient
   *     bean like this: <code>
   *  *     WebClient httpClient =
   *  *         WebClient.builder()
   *  *             .filter(myFilter.applyToWebClient())
   *  *             .exchangeStrategies(strategies)
   *  *             .build();
   *  * </code>
   */
  public ExchangeFilterFunction applyToWebClient() {
    return (request, next) ->
        next.exchange(request)
            .doOnNext(
                clientResponse -> {
                  boolean isClientError = clientResponse.statusCode().is4xxClientError();
                  boolean isServerError = clientResponse.statusCode().is5xxServerError();
                  if (isClientError || isServerError) {
                    clientResponse
                        .bodyToMono(ErrorResponse.class)
                        .map(
                            errorDetail -> {
                              throw (RuntimeException)
                                  GlobalExceptionEnum.getException(
                                      errorDetail.getMessage(), clientResponse.rawStatusCode());
                            })
                        .subscribe();
                  }
                });
  }
}
