package com.zonzonzon.common.service.log;

import static com.zonzonzon.common.utils.RestUtils.API_KEY_HEADER_NAME;
import static com.zonzonzon.common.utils.RestUtils.API_KEY_LOGGER_NAME;

import com.zonzonzon.common.utils.ReactiveRestUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

/** Manages API key in REST request and response headers. */
@Component
@Slf4j
@RequiredArgsConstructor
public class ApiKeyLoggingReactiveFilter implements WebFilter, Ordered {

  @Value("${com.zonzonzon.current-application.api-key}")
  private String currentApplicationApiKey;

  @Value("${com.zonzonzon.common.filters.api-key-logging-reactive-filter.enabled:false}")
  private boolean isEnabled;

  @Override
  public int getOrder() {
    return -60;
  }

  public ExchangeFilterFunction applyToWebClient() {
    return (request, next) ->
        next.exchange(
            ClientRequest.from(request)
                .headers(
                    (headers) -> {
                      String apiKey = MDC.get(API_KEY_LOGGER_NAME);
                      if (headers.get(API_KEY_HEADER_NAME) != null) {
                        headers.remove(API_KEY_HEADER_NAME);
                      }
                      headers.add(
                          API_KEY_HEADER_NAME, apiKey != null ? apiKey : currentApplicationApiKey);
                    })
                .build());
  }

  /**
   * A filter for Router endpoints. A reactive implementation. Intercepts REST queries to put an API
   * Key from header to reactive context.
   */
  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    if (!isEnabled) {
      return chain.filter(exchange);
    }
    // API key validation is done in the Sentinel.
    ServerHttpRequest request = exchange.getRequest();

    Mono<Void> filtered =
        ReactiveRestUtils.addHeaderToReactiveContext(
            exchange, chain, request.getHeaders(), API_KEY_HEADER_NAME, API_KEY_LOGGER_NAME);

    return filtered;
  }
}
