package com.zonzonzon.common.service.log;

import static com.zonzonzon.common.utils.ReflectionUtils.getSafe;
import static java.nio.charset.StandardCharsets.UTF_8;

import com.zonzonzon.common.utils.LogUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.channels.Channels;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.data.util.Pair;
import org.springframework.http.HttpMethod;
import org.springframework.http.server.reactive.ServerHttpRequestDecorator;
import org.springframework.http.server.reactive.ServerHttpResponseDecorator;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** A filter to log REST requests and responses. A reactive implementation. */
@Component
@Slf4j
@RequiredArgsConstructor
public class LoggingReactiveFilter implements WebFilter, Ordered {

  private static String localHostAddress;
  private static String localHostName;
  private static String remoteHostAddress;
  private static String remoteHostName;

  static {
    // Local address:
    try {
      localHostAddress = InetAddress.getLocalHost().getHostAddress();
    } catch (UnknownHostException ignore) {
    }
    try {
      localHostName = InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException ignore) {
    }
    // Remote address:
    remoteHostAddress = InetAddress.getLoopbackAddress().getHostAddress();
    remoteHostName = InetAddress.getLoopbackAddress().getHostName();
  }

  @Value("${com.zonzonzon.common.filters.logging-reactive-filter.enabled:false}")
  private boolean isEnabled;

  @Value("${com.zonzonzon.common.logging.is-to-pretty-print:false}")
  private boolean isToPrettyPrint;

  @Override
  public int getOrder() {
    return -70;
  }

  /**
   * A filter for a WebClient. The filter is expected to be attached in you WebConfiguration
   * WebClient bean like this: <code>
   *  *     WebClient httpClient =
   *  *         WebClient.builder()
   *  *             .filter(myFilter.applyToWebClient())
   *  *             .exchangeStrategies(strategies)
   *  *             .build();
   *  * </code>
   *
   * @return Function to be applied when filter is attached to a reactive WebClient.
   */
  public ExchangeFilterFunction applyToWebClient() {
    return (request, next) -> {
      LogUtils.logRequest(
          request.method().name(),
          request.url().toString(),
          request.attributes().toString(),
          request.headers().toString(),
          null,
          null,
          remoteHostAddress,
          localHostAddress,
          request.cookies().toString(),
          request.body().toString());

      log.info(request.toString());
      return next.exchange(request).doOnNext(clientResponse -> log.info(clientResponse.toString()));
    };
  }

  /**
   * A filter for Router endpoints. A reactive implementation. Intercepts REST requests and
   * responses to log its data.
   */
  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    return isEnabled ? getLoggingReactiveFilterMono(exchange, chain) : chain.filter(exchange);
  }

  public Pair<ServerHttpRequestDecorator, ServerHttpResponseDecorator> getRequestResponseDecorators(
      ServerWebExchange exchange) {

    ServerHttpRequestDecorator request =
        new ServerHttpRequestDecorator(exchange.getRequest()) {

          @Override
          public Flux<DataBuffer> getBody() {
            // Handler is called when request has a body:
            return super.getBody()
                .doOnNext(
                    dataBuffer -> {
                      try (ByteArrayOutputStream byteArrayOutputStream =
                          new ByteArrayOutputStream()) {

                        Channels.newChannel(byteArrayOutputStream)
                            .write(dataBuffer.asByteBuffer().asReadOnlyBuffer());

                        String requestBody = byteArrayOutputStream.toString(UTF_8);

                        LogUtils.logRequest(
                            getSafe(this::getMethodValue),
                            getSafe(() -> this.getURI().toString()),
                            getSafe(() -> this.getQueryParams().toString()),
                            getSafe(() -> this.getHeaders().toString()),
                            getSafe(() -> this.getSslInfo().toString()),
                            null,
                            getSafe(() -> this.getRemoteAddress().toString()),
                            getSafe(() -> this.getLocalAddress().toString()),
                            getSafe(() -> this.getCookies().toString()),
                            requestBody);
                      } catch (IOException e) {
                        log.error("Request body building failed: {}", e.getLocalizedMessage());
                      }
                    });
          }

          @Override
          public HttpMethod getMethod() {
            // Handler is called when request has no body:
            LogUtils.logRequest(
                getSafe(this::getMethodValue),
                getSafe(() -> this.getURI().toString()),
                getSafe(() -> this.getQueryParams().toString()),
                getSafe(() -> this.getHeaders().toString()),
                getSafe(() -> this.getSslInfo().toString()),
                null,
                getSafe(() -> this.getRemoteAddress().toString()),
                getSafe(() -> this.getLocalAddress().toString()),
                getSafe(() -> this.getCookies().toString()),
                null);
            return super.getMethod();
          }
        };

    ServerHttpResponseDecorator response =
        new ServerHttpResponseDecorator(exchange.getResponse()) {
          @Override
          public Mono<Void> writeWith(Publisher<? extends DataBuffer> body) {
            if (body instanceof Flux) {
              Flux<? extends DataBuffer> fluxBody = (Flux<? extends DataBuffer>) body;
              return super.writeWith(
                  fluxBody.map(
                      dataBuffer -> {
                        try {
                          byte[] content = new byte[dataBuffer.readableByteCount()];
                          dataBuffer.read(content);

                          LogUtils.logResponse(
                              exchange.getResponse().getStatusCode().value(),
                              exchange.getResponse().getHeaders().toString(),
                              new String(content, UTF_8).replaceAll(System.lineSeparator(), ""),
                              isToPrettyPrint);

                          return exchange.getResponse().bufferFactory().wrap(content);
                        } finally {
                          DataBufferUtils.release(dataBuffer);
                        }
                      }));
            }
            if (body instanceof Mono) {
              Mono<? extends DataBuffer> fluxBody = (Mono<? extends DataBuffer>) body;
              return super.writeWith(
                  fluxBody.map(
                      dataBuffer -> {
                        try {
                          byte[] content = new byte[dataBuffer.readableByteCount()];
                          dataBuffer.read(content);

                          LogUtils.logResponse(
                              exchange.getResponse().getStatusCode().value(),
                              exchange.getResponse().getHeaders().toString(),
                              new String(content, UTF_8).replaceAll(System.lineSeparator(), ""),
                              isToPrettyPrint);

                          return exchange.getResponse().bufferFactory().wrap(content);
                        } finally {
                          DataBufferUtils.release(dataBuffer);
                        }
                      }));
            }
            return super.writeWith(body);
          }
        };

    return Pair.of(request, response);
  }

  @NotNull
  public Mono<Void> getLoggingReactiveFilterMono(ServerWebExchange exchange, WebFilterChain chain) {
    Pair<ServerHttpRequestDecorator, ServerHttpResponseDecorator> requestResponse =
        getRequestResponseDecorators(exchange);

    return chain.filter(
        exchange
            .mutate()
            .request(requestResponse.getFirst())
            .response(requestResponse.getSecond())
            .build());
  }
}
