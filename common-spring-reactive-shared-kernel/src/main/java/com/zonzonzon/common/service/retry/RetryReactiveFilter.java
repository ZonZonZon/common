package com.zonzonzon.common.service.retry;

import java.time.Duration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import reactor.util.retry.Retry;

/** A filter to retry requests when server is not responding. A reactive implementation. */
@Component
@Slf4j
public class RetryReactiveFilter {

  @Value("${com.zonzonzon.rest-client.retry-max-attempts:3}")
  private int retryMaxAttempts;

  @Value("${com.zonzonzon.rest-client.retry-delay-seconds:3}")
  private int retryDelaySeconds;

  /**
   * A filter for a WebClient. The filter * is expected to be attached in you WebConfiguration
   * WebClient bean like this: <code>
   *  *     WebClient httpClient =
   *  *         WebClient.builder()
   *  *             .filter(myFilter.applyToWebClient())
   *  *             .exchangeStrategies(strategies)
   *  *             .build();
   *  * </code>
   *
   * @return Function to be applied when filter is attached to a reactive WebClient.
   */
  public ExchangeFilterFunction applyToWebClient() {
    return (request, next) ->
        next.exchange(request)
            .retryWhen(
                Retry.fixedDelay(retryMaxAttempts, Duration.ofSeconds(retryDelaySeconds))
                    .doAfterRetry(
                        retrySignal ->
                            log.warn(
                                "Retrying request {}. Max retry attempts {}. Retrying if no response in {} seconds",
                                request,
                                retryMaxAttempts,
                                retryDelaySeconds)));
  }
}
