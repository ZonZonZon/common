package com.zonzonzon.common.service.exception;

import static com.zonzonzon.common.service.exception.GlobalExceptionEnum.getHttpStatus;
import static com.zonzonzon.common.utils.ReflectionUtils.getSafe;
import static org.springframework.http.HttpStatus.BAD_GATEWAY;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.BANDWIDTH_LIMIT_EXCEEDED;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.EXPECTATION_FAILED;
import static org.springframework.http.HttpStatus.FAILED_DEPENDENCY;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.GATEWAY_TIMEOUT;
import static org.springframework.http.HttpStatus.INSUFFICIENT_STORAGE;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.LOCKED;
import static org.springframework.http.HttpStatus.LOOP_DETECTED;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.http.HttpStatus.NETWORK_AUTHENTICATION_REQUIRED;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NOT_IMPLEMENTED;
import static org.springframework.http.HttpStatus.PAYLOAD_TOO_LARGE;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;
import static org.springframework.http.HttpStatus.PRECONDITION_REQUIRED;
import static org.springframework.http.HttpStatus.REQUEST_HEADER_FIELDS_TOO_LARGE;
import static org.springframework.http.HttpStatus.REQUEST_TIMEOUT;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;
import static org.springframework.http.HttpStatus.TOO_EARLY;
import static org.springframework.http.HttpStatus.TOO_MANY_REQUESTS;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE;
import static org.springframework.http.HttpStatus.UPGRADE_REQUIRED;
import static org.springframework.http.HttpStatus.URI_TOO_LONG;

import com.zonzonzon.common.ui.exception.BadGatewayException;
import com.zonzonzon.common.ui.exception.BadRequestException;
import com.zonzonzon.common.ui.exception.BandwidthLimitExceededException;
import com.zonzonzon.common.ui.exception.BusinessException;
import com.zonzonzon.common.ui.exception.ConflictException;
import com.zonzonzon.common.ui.exception.ExpectationFailedException;
import com.zonzonzon.common.ui.exception.FailedDependencyException;
import com.zonzonzon.common.ui.exception.ForbiddenException;
import com.zonzonzon.common.ui.exception.GatewayTimeoutException;
import com.zonzonzon.common.ui.exception.HttpExceptionResponse;
import com.zonzonzon.common.ui.exception.InsufficientStorageException;
import com.zonzonzon.common.ui.exception.InternalServerException;
import com.zonzonzon.common.ui.exception.LockedException;
import com.zonzonzon.common.ui.exception.LoopDetectedException;
import com.zonzonzon.common.ui.exception.MethodNotAllowedException;
import com.zonzonzon.common.ui.exception.NetworkAuthenticationRequiredException;
import com.zonzonzon.common.ui.exception.NotAcceptableException;
import com.zonzonzon.common.ui.exception.NotFoundException;
import com.zonzonzon.common.ui.exception.NotImplementedException;
import com.zonzonzon.common.ui.exception.PayloadTooLargeException;
import com.zonzonzon.common.ui.exception.PreconditionFailedException;
import com.zonzonzon.common.ui.exception.PreconditionRequiredException;
import com.zonzonzon.common.ui.exception.RequestHeaderFieldsTooLargeException;
import com.zonzonzon.common.ui.exception.RequestTimeoutException;
import com.zonzonzon.common.ui.exception.ServiceUnavailableException;
import com.zonzonzon.common.ui.exception.TooEarlyException;
import com.zonzonzon.common.ui.exception.TooManyRequestsException;
import com.zonzonzon.common.ui.exception.UnauthorizedException;
import com.zonzonzon.common.ui.exception.UnavailableForLegalReasonsException;
import com.zonzonzon.common.ui.exception.UnhandledException;
import com.zonzonzon.common.ui.exception.UnprocessableEntityException;
import com.zonzonzon.common.ui.exception.UnsupportedMediaTypeException;
import com.zonzonzon.common.ui.exception.UpgradeRequiredException;
import com.zonzonzon.common.ui.exception.UriTooLongException;
import com.zonzonzon.common.ui.exception.ValidationException;
import com.zonzonzon.common.ui.response.ErrorResponse;
import com.zonzonzon.common.ui.response.Violation;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Global exception handler is used for unhandled exceptions or generalized exceptions like {@link
 * com.zonzonzon.common.ui.exception.BusinessException}. If you need a specific business exception
 * response - create a specific exception class.
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

  @Value("${spring.profiles.active:Unknown}")
  private String activeProfile;

  @Value("${spring.profiles.secured-profiles:}")
  private Set<String> securedProfiles;

  public ErrorResponse handleUnspecifiedException(Exception unspecifiedException) {
    String exceptionMessage = unspecifiedException.getMessage();

    String exceptionName =
        getSafe(() -> unspecifiedException.toString().replace(exceptionMessage, ""));

    String exceptionCause =
        unspecifiedException.getCause() == null
            ? unspecifiedException.getClass().getName()
            : unspecifiedException.getCause().getLocalizedMessage();

    String message =
        (exceptionName == null ? "" : exceptionName)
            + (exceptionMessage == null ? "" : " " + exceptionMessage)
            + (exceptionCause == null || exceptionCause.equals(exceptionMessage)
                ? ""
                : " " + exceptionCause);

    if (exceptionName != null && exceptionName.contains("org.springframework.web.bind")) {
      // Rethrow for Bad Request Type exceptions:
      BadRequestException badRequestException = new ValidationException(message);
      return handle(badRequestException);
    }
    return handle(new UnhandledException(message));
  }

  private ErrorResponse getResponse(HttpExceptionResponse exception) {
    boolean isSecuredProfile = securedProfiles.contains(activeProfile);
    boolean isBusinessException = exception instanceof BusinessException;
    boolean isMaskedException = isSecuredProfile && !isBusinessException;

    ErrorResponse response =
        ErrorResponse.builder()
            .code(
                getSafe(() -> String.valueOf(getHttpStatus((RuntimeException) exception).value())))
            .message(
                isMaskedException
                    ? "Неперехваченная ошибка на стороне сервера"
                    : exception.getMessage())
            .build();

    return response;
  }

  @ExceptionHandler({BadGatewayException.class})
  @ResponseStatus(BAD_GATEWAY)
  public ErrorResponse handle(BadGatewayException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({BadRequestException.class})
  @ResponseStatus(BAD_REQUEST)
  public ErrorResponse handle(BadRequestException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({MethodArgumentNotValidException.class})
  @ResponseStatus(BAD_REQUEST)
  public ErrorResponse handle(MethodArgumentNotValidException exception) {
    ErrorResponse response =
        getResponse(new ValidationException(exception.getMessage()));

    response.setViolations(fetchViolations(exception));
    return response;
  }

  @ExceptionHandler({BindException.class})
  @ResponseStatus(BAD_REQUEST)
  public ErrorResponse handle(BindException exception) {
    ErrorResponse response =
        getResponse(new ValidationException(exception.getMessage()));

    response.setViolations(fetchViolations(exception));
    return response;
  }

  private List<Violation> fetchViolations(BindException exception) {
    List<Violation> violations =
        exception.getBindingResult().getFieldErrors().stream()
            .map(
                error ->
                    getSafe(
                        () ->
                            Violation.builder()
                                .field(error.getField())
                                .message(error.getDefaultMessage())
                                .build()))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

    violations.addAll(
        exception.getBindingResult().getGlobalErrors().stream()
            .map(
                error ->
                    getSafe(
                        () ->
                            Violation.builder().message(error.getDefaultMessage()).build()))
            .filter(Objects::nonNull)
            .collect(Collectors.toList()));

    return violations;
  }

  @ExceptionHandler({BandwidthLimitExceededException.class})
  @ResponseStatus(BANDWIDTH_LIMIT_EXCEEDED)
  public ErrorResponse handle(BandwidthLimitExceededException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {BusinessException.class})
  @ResponseStatus(CONFLICT)
  public ErrorResponse handle(BusinessException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {ConflictException.class})
  @ResponseStatus(CONFLICT)
  public ErrorResponse handle(ConflictException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {ExpectationFailedException.class})
  @ResponseStatus(EXPECTATION_FAILED)
  public ErrorResponse handle(ExpectationFailedException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {FailedDependencyException.class})
  @ResponseStatus(FAILED_DEPENDENCY)
  public ErrorResponse handle(FailedDependencyException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {ForbiddenException.class})
  @ResponseStatus(FORBIDDEN)
  public ErrorResponse handle(ForbiddenException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {GatewayTimeoutException.class})
  @ResponseStatus(GATEWAY_TIMEOUT)
  public ErrorResponse handle(GatewayTimeoutException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {InsufficientStorageException.class})
  @ResponseStatus(INSUFFICIENT_STORAGE)
  public ErrorResponse handle(InsufficientStorageException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {Exception.class})
  @ResponseStatus(INTERNAL_SERVER_ERROR)
  public ErrorResponse handle(Exception exception) {
    return handleUnspecifiedException(exception);
  }

  @ExceptionHandler(value = {InternalServerException.class})
  @ResponseStatus(INTERNAL_SERVER_ERROR)
  public ErrorResponse handle(InternalServerException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {LockedException.class})
  @ResponseStatus(LOCKED)
  public ErrorResponse handle(LockedException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {LoopDetectedException.class})
  @ResponseStatus(LOOP_DETECTED)
  public ErrorResponse handle(LoopDetectedException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {MethodNotAllowedException.class})
  @ResponseStatus(METHOD_NOT_ALLOWED)
  public ErrorResponse handle(MethodNotAllowedException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {NetworkAuthenticationRequiredException.class})
  @ResponseStatus(NETWORK_AUTHENTICATION_REQUIRED)
  public ErrorResponse handle(NetworkAuthenticationRequiredException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {NotAcceptableException.class})
  @ResponseStatus(NOT_ACCEPTABLE)
  public ErrorResponse handle(NotAcceptableException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {NotFoundException.class})
  @ResponseStatus(NOT_FOUND)
  public ErrorResponse handle(NotFoundException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {NotImplementedException.class})
  @ResponseStatus(NOT_IMPLEMENTED)
  public ErrorResponse handle(NotImplementedException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {PayloadTooLargeException.class})
  @ResponseStatus(PAYLOAD_TOO_LARGE)
  public ErrorResponse handle(PayloadTooLargeException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {PreconditionFailedException.class})
  @ResponseStatus(PRECONDITION_FAILED)
  public ErrorResponse handle(PreconditionFailedException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {PreconditionRequiredException.class})
  @ResponseStatus(PRECONDITION_REQUIRED)
  public ErrorResponse handle(PreconditionRequiredException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {RequestHeaderFieldsTooLargeException.class})
  @ResponseStatus(REQUEST_HEADER_FIELDS_TOO_LARGE)
  public ErrorResponse handle(RequestHeaderFieldsTooLargeException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {RequestTimeoutException.class})
  @ResponseStatus(REQUEST_TIMEOUT)
  public ErrorResponse handle(RequestTimeoutException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {ServiceUnavailableException.class})
  @ResponseStatus(SERVICE_UNAVAILABLE)
  public ErrorResponse handle(ServiceUnavailableException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {TooEarlyException.class})
  @ResponseStatus(TOO_EARLY)
  public ErrorResponse handle(TooEarlyException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler(value = {TooManyRequestsException.class})
  @ResponseStatus(TOO_MANY_REQUESTS)
  public ErrorResponse handle(TooManyRequestsException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({UnauthorizedException.class})
  @ResponseStatus(UNAUTHORIZED)
  public ErrorResponse handle(UnauthorizedException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({UnavailableForLegalReasonsException.class})
  @ResponseStatus(UNAVAILABLE_FOR_LEGAL_REASONS)
  public ErrorResponse handle(UnavailableForLegalReasonsException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({UnprocessableEntityException.class})
  @ResponseStatus(UNPROCESSABLE_ENTITY)
  public ErrorResponse handle(UnprocessableEntityException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({UnsupportedMediaTypeException.class})
  @ResponseStatus(UNSUPPORTED_MEDIA_TYPE)
  public ErrorResponse handle(UnsupportedMediaTypeException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({UpgradeRequiredException.class})
  @ResponseStatus(UPGRADE_REQUIRED)
  public ErrorResponse handle(UpgradeRequiredException exception) {
    return getResponse(exception);
  }

  @ExceptionHandler({UriTooLongException.class})
  @ResponseStatus(URI_TOO_LONG)
  public ErrorResponse handle(UriTooLongException exception) {
    return getResponse(exception);
  }
}
