package com.zonzonzon.common.service.exception;

import static com.zonzonzon.common.service.exception.GlobalExceptionEnum.getHttpStatus;
import static com.zonzonzon.common.utils.ReflectionUtils.getSafe;

import com.zonzonzon.common.ui.response.ErrorResponse;
import com.zonzonzon.common.ui.response.ErrorsResponse;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.boot.autoconfigure.web.reactive.error.AbstractErrorWebExceptionHandler;
import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerCodecConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Hooks;
import reactor.core.publisher.Mono;

/**
 * A wrapper to handle exceptions thrown by reactive Webflux router endpoints.
 *
 * <p>We set our global error handler to @Order(-2) to give it a higher priority than the
 * DefaultErrorWebExceptionHandler which is registered at @Order(-1).
 */
@Component
@Slf4j
@Order(-2)
public class ReactiveGlobalExceptionHandler extends AbstractErrorWebExceptionHandler {

  private final GlobalExceptionHandler globalExceptionHandler;

  public ReactiveGlobalExceptionHandler(
      ErrorAttributes errorAttributes,
      WebProperties.Resources resourceProperties,
      ApplicationContext applicationContext,
      ServerCodecConfigurer configurer,
      GlobalExceptionHandler globalExceptionHandler) {

    super(errorAttributes, resourceProperties, applicationContext);
    this.setMessageWriters(configurer.getWriters());
    this.globalExceptionHandler = globalExceptionHandler;
  }

  @PostConstruct
  void init(){
    handleReactorObscureExceptions();
  }

  /**
   * Handler to process exceptions Reactor doesn't recognize.
   */
  private void handleReactorObscureExceptions() {
    Hooks.onErrorDropped(throwable -> {});
  }

  @Override
  protected RouterFunction<ServerResponse> getRoutingFunction(ErrorAttributes errorAttributes) {
    return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse);
  }

  private Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
    Exception exception =
        (Exception)
            request
                .attribute(
                    "org.springframework.boot.web.reactive.error.DefaultErrorAttributes.ERROR")
                .orElse(null);

    HttpStatus httpStatus = getHttpStatus(exception);
    ErrorsResponse response = getResponse(exception);

    return ServerResponse.status(httpStatus)
        .contentType(MediaType.APPLICATION_JSON)
        .body(BodyInserters.fromValue(response));
  }

  public static ErrorsResponse getResponse(Exception exception) {
    ErrorsResponse errorResponse =
        Arrays.stream(GlobalExceptionEnum.values())
            .filter(
                globalExceptionEnum -> globalExceptionEnum.getExceptionType().isInstance(exception))
            .findAny()
            .map(globalExceptionEnum -> buildErrorResponse(exception))
            .orElse(
                ErrorsResponse.builder()
                    .errors(
                        List.of(
                            ErrorResponse.builder()
                                .message(getUnspecifiedExceptionMessage(exception))
                                .build()))
                    .build());

    return errorResponse;
  }

  @SneakyThrows
  private static ErrorsResponse buildErrorResponse(Exception exception) {
    String code = null;
    String message = null;

    try {
      code = (String) exception.getClass().getField("CODE").get(null);
    } catch (Exception ignore) {
    }

    try {
      message = (String) exception.getClass().getField("MESSAGE").get(null);
    } catch (Exception ignore) {
    }

    if (message == null) {
      try {
        message = exception.getLocalizedMessage();
      } catch (Exception ignore) {
      }
    }

    if (message == null) {
      message = getUnspecifiedExceptionMessage(exception);
    }

    return ErrorsResponse.builder()
        .errors(List.of(ErrorResponse.builder().code(code).message(message).build()))
        .build();
  }

  private static String getUnspecifiedExceptionMessage(Exception unspecifiedException) {
    String exceptionMessage = unspecifiedException.getMessage();

    String exceptionName =
        getSafe(() -> unspecifiedException.toString().replace(exceptionMessage, ""));

    String exceptionCause =
        unspecifiedException.getCause() == null
            ? unspecifiedException.getClass().getName()
            : unspecifiedException.getCause().getLocalizedMessage();

    String message =
        (exceptionName == null ? "" : exceptionName)
            + (exceptionMessage == null ? "" : " " + exceptionMessage)
            + (exceptionCause == null || exceptionCause.equals(exceptionMessage)
            ? ""
            : " " + exceptionCause);
    return message;
  }
}
