package com.zonzonzon.common.service.log;

import static com.zonzonzon.common.utils.RestUtils.TRACE_ID_HEADER_NAME;
import static com.zonzonzon.common.utils.RestUtils.TRACE_ID_LOGGER_NAME;

import com.zonzonzon.common.utils.ReactiveRestUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

/**
 * Adds trace identifier to a scenario passing through multiple microservices as a REST response
 * header. A reactive implementation.
 */
@Component
@Slf4j
@RequiredArgsConstructor
public class TraceIdLoggingReactiveFilter implements WebFilter {

  @Value("${com.zonzonzon.common.filters.trace-id-logging-reactive-filter.enabled:false}")
  private boolean isEnabled;

  public ExchangeFilterFunction applyToWebClient() {
    return (request, next) ->
        next.exchange(
            ClientRequest.from(request)
                .headers(
                    headers -> {
                      String traceId = MDC.get(TRACE_ID_LOGGER_NAME);
                      if (traceId != null) {
                        if (headers.get(TRACE_ID_HEADER_NAME) != null) {
                          headers.remove(TRACE_ID_HEADER_NAME);
                        }

                        headers.add(TRACE_ID_HEADER_NAME, traceId);
                      }
                    })
                .build());
  }

  /**
   * A filter for Router endpoints. A reactive implementation. Intercepts REST queries to put a
   * trace id from header to reactive context.
   */
  @Override
  public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {
    return isEnabled ? getTraceFilterMono(exchange, chain) : chain.filter(exchange);
  }

  public static Mono<Void> getTraceFilterMono(ServerWebExchange exchange, WebFilterChain chain) {

    Mono<Void> filtered =
        ReactiveRestUtils.addHeaderToReactiveContext(
            exchange,
            chain,
            exchange.getRequest().getHeaders(),
            TRACE_ID_HEADER_NAME,
            TRACE_ID_LOGGER_NAME);

    return filtered;
  }
}
