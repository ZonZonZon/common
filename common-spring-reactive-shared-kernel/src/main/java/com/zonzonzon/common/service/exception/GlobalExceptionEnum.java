package com.zonzonzon.common.service.exception;

import static com.zonzonzon.common.utils.ReflectionUtils.getClassInstance;
import static org.springframework.http.HttpStatus.BAD_GATEWAY;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.BANDWIDTH_LIMIT_EXCEEDED;
import static org.springframework.http.HttpStatus.CONFLICT;
import static org.springframework.http.HttpStatus.EXPECTATION_FAILED;
import static org.springframework.http.HttpStatus.FAILED_DEPENDENCY;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.GATEWAY_TIMEOUT;
import static org.springframework.http.HttpStatus.INSUFFICIENT_STORAGE;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.LOCKED;
import static org.springframework.http.HttpStatus.LOOP_DETECTED;
import static org.springframework.http.HttpStatus.METHOD_NOT_ALLOWED;
import static org.springframework.http.HttpStatus.NETWORK_AUTHENTICATION_REQUIRED;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.NOT_IMPLEMENTED;
import static org.springframework.http.HttpStatus.PAYLOAD_TOO_LARGE;
import static org.springframework.http.HttpStatus.PRECONDITION_FAILED;
import static org.springframework.http.HttpStatus.PRECONDITION_REQUIRED;
import static org.springframework.http.HttpStatus.REQUEST_HEADER_FIELDS_TOO_LARGE;
import static org.springframework.http.HttpStatus.REQUEST_TIMEOUT;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;
import static org.springframework.http.HttpStatus.TOO_EARLY;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNAVAILABLE_FOR_LEGAL_REASONS;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE;
import static org.springframework.http.HttpStatus.UPGRADE_REQUIRED;
import static org.springframework.http.HttpStatus.URI_TOO_LONG;

import com.zonzonzon.common.ui.exception.BadGatewayException;
import com.zonzonzon.common.ui.exception.BadRequestException;
import com.zonzonzon.common.ui.exception.BandwidthLimitExceededException;
import com.zonzonzon.common.ui.exception.BusinessException;
import com.zonzonzon.common.ui.exception.ConflictException;
import com.zonzonzon.common.ui.exception.ExpectationFailedException;
import com.zonzonzon.common.ui.exception.FailedDependencyException;
import com.zonzonzon.common.ui.exception.ForbiddenException;
import com.zonzonzon.common.ui.exception.GatewayTimeoutException;
import com.zonzonzon.common.ui.exception.HttpExceptionResponse;
import com.zonzonzon.common.ui.exception.InsufficientStorageException;
import com.zonzonzon.common.ui.exception.LockedException;
import com.zonzonzon.common.ui.exception.LoopDetectedException;
import com.zonzonzon.common.ui.exception.MethodNotAllowedException;
import com.zonzonzon.common.ui.exception.NetworkAuthenticationRequiredException;
import com.zonzonzon.common.ui.exception.NotAcceptableException;
import com.zonzonzon.common.ui.exception.NotFoundException;
import com.zonzonzon.common.ui.exception.NotImplementedException;
import com.zonzonzon.common.ui.exception.PayloadTooLargeException;
import com.zonzonzon.common.ui.exception.PreconditionFailedException;
import com.zonzonzon.common.ui.exception.PreconditionRequiredException;
import com.zonzonzon.common.ui.exception.RequestHeaderFieldsTooLargeException;
import com.zonzonzon.common.ui.exception.RequestTimeoutException;
import com.zonzonzon.common.ui.exception.ServiceUnavailableException;
import com.zonzonzon.common.ui.exception.TooEarlyException;
import com.zonzonzon.common.ui.exception.TooManyRequestsException;
import com.zonzonzon.common.ui.exception.UnauthorizedException;
import com.zonzonzon.common.ui.exception.UnavailableForLegalReasonsException;
import com.zonzonzon.common.ui.exception.UnprocessableEntityException;
import com.zonzonzon.common.ui.exception.UnsupportedMediaTypeException;
import com.zonzonzon.common.ui.exception.UpgradeRequiredException;
import com.zonzonzon.common.ui.exception.UriTooLongException;
import java.util.Arrays;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;

/** Matching of HTTP status and different exception data. */
@AllArgsConstructor
@Getter
public enum GlobalExceptionEnum {
  BUSINESS_EXCEPTION(CONFLICT, BusinessException.class),
  BAD_GATEWAY_EXCEPTION(BAD_GATEWAY, BadGatewayException.class),
  BAD_REQUEST_EXCEPTION(BAD_REQUEST, BadRequestException.class),
  BANDWIDTH_LIMIT_EXCEEDED_EXCEPTION(
      BANDWIDTH_LIMIT_EXCEEDED, BandwidthLimitExceededException.class),
  CONFLICT_EXCEPTION(CONFLICT, ConflictException.class),
  EXPECTATION_FAILED_EXCEPTION(EXPECTATION_FAILED, ExpectationFailedException.class),
  FAILED_DEPENDENCY_EXCEPTION(FAILED_DEPENDENCY, FailedDependencyException.class),
  FORBIDDEN_EXCEPTION(FORBIDDEN, ForbiddenException.class),
  GATEWAY_TIMEOUT_EXCEPTION(GATEWAY_TIMEOUT, GatewayTimeoutException.class),
  INSUFFICIENT_STORAGE_EXCEPTION(INSUFFICIENT_STORAGE, InsufficientStorageException.class),
  INTERNAL_SERVER_EXCEPTION(INTERNAL_SERVER_ERROR, InsufficientStorageException.class),
  LOCKED_EXCEPTION(LOCKED, LockedException.class),
  LOOP_DETECTED_EXCEPTION(LOOP_DETECTED, LoopDetectedException.class),
  METHOD_NOT_ALLOWED_EXCEPTION(METHOD_NOT_ALLOWED, MethodNotAllowedException.class),
  NETWORK_AUTHENTICATION_REQUIRED_EXCEPTION(
      NETWORK_AUTHENTICATION_REQUIRED, NetworkAuthenticationRequiredException.class),
  NOT_ACCEPTABLE_EXCEPTION(NOT_ACCEPTABLE, NotAcceptableException.class),
  NOT_FOUND_EXCEPTION(NOT_FOUND, NotFoundException.class),
  NOT_IMPLEMENTED_EXCEPTION(NOT_IMPLEMENTED, NotImplementedException.class),
  PAYLOAD_TOO_LARGE_EXCEPTION(PAYLOAD_TOO_LARGE, PayloadTooLargeException.class),
  PRECONDITION_FAILED_EXCEPTION(PRECONDITION_FAILED, PreconditionFailedException.class),
  PRECONDITION_REQUIRED_EXCEPTION(PRECONDITION_REQUIRED, PreconditionRequiredException.class),
  REQUEST_HEADER_FIELDS_TOO_LARGE_EXCEPTION(
      REQUEST_HEADER_FIELDS_TOO_LARGE, RequestHeaderFieldsTooLargeException.class),
  REQUEST_TIMEOUT_EXCEPTION(REQUEST_TIMEOUT, RequestTimeoutException.class),
  SERVICE_UNAVAILABLE_EXCEPTION(SERVICE_UNAVAILABLE, ServiceUnavailableException.class),
  TOO_EARLY_EXCEPTION(TOO_EARLY, TooEarlyException.class),
  TOO_MANY_REQUESTS_EXCEPTION(TOO_EARLY, TooManyRequestsException.class),
  UNAUTHORIZED_EXCEPTION(UNAUTHORIZED, UnauthorizedException.class),
  UNAVAILABLE_FOR_LEGAL_REASONS_EXCEPTION(
      UNAVAILABLE_FOR_LEGAL_REASONS, UnavailableForLegalReasonsException.class),
  UNPROCESSABLE_ENTITY_EXCEPTION(UNPROCESSABLE_ENTITY, UnprocessableEntityException.class),
  UNSUPPORTED_MEDIA_TYPE_EXCEPTION(UNSUPPORTED_MEDIA_TYPE, UnsupportedMediaTypeException.class),
  UPGRADE_REQUIRED_EXCEPTION(UPGRADE_REQUIRED, UpgradeRequiredException.class),
  URI_TOO_LONG_EXCEPTION(URI_TOO_LONG, UriTooLongException.class);

  public final HttpStatus httpStatus;
  public final Class<?> exceptionType;

  /**
   * Creates an exception by HTTP-status code. Doesn't throw it, just returns.
   *
   * @param errorMessage A message to be passed to an exception.
   * @param httpStatusCode An HTTP-status code.
   * @return An exception to a generalized interface type.
   */
  @SneakyThrows
  public static HttpExceptionResponse getException(String errorMessage, int httpStatusCode) {
    return Arrays.stream(GlobalExceptionEnum.values())
        .filter(
            globalExceptionEnum -> globalExceptionEnum.getHttpStatus().value() == httpStatusCode)
        .findAny()
        .map(GlobalExceptionEnum::getExceptionType)
        .map(exceptionClass -> getClassInstance(exceptionClass, errorMessage))
        .map(HttpExceptionResponse.class::cast)
        .orElse(null);
  }

  public static HttpStatus getHttpStatus(Exception exception) {
    HttpStatus status =
        Arrays.stream(GlobalExceptionEnum.values())
            .filter(
                globalExceptionEnum -> globalExceptionEnum.getExceptionType().isInstance(exception))
            .findAny()
            .map(GlobalExceptionEnum::getHttpStatus)
            .orElse(INTERNAL_SERVER_ERROR);

    return status;
  }
}
