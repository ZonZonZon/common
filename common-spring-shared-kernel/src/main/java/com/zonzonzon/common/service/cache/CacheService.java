package com.zonzonzon.common.service.cache;

import java.util.Set;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Getter
@Service
@Slf4j
@ConditionalOnProperty(prefix = "com.zonzonzon.common.caching-api", name = "enabled", havingValue = "true")
public class CacheService {

  @Value("${com.zonzonzon.current-application.api-key}")
  private String apiKey;

  @Value("${spring.redis.database:0}")
  private int databaseIndex;

  private static final String ALL = "all";
  private static final String CACHE_FLUSH_COMPLETE = "Cache flushed";
  private static final String CACHE_FLUSH_NO_ENTRIES = "No entries found to flush";

  private final RedisTemplate<String, Object> redisTemplate;
  private final JedisConnectionFactory jedisConnectionFactory;

  public CacheService(
      RedisTemplate<String, Object> redisTemplate, JedisConnectionFactory jedisConnectionFactory) {

    this.redisTemplate = redisTemplate;
    this.jedisConnectionFactory = jedisConnectionFactory;
  }

  /**
   * @param apiKey Application secret API key to execute this operation.
   * @param cacheKey Cache key name to clear.
   * @return Cache clearing operation result.
   */
  public CacheServiceResponse clear(String apiKey, String cacheKey) {
    if (!apiKey.equals(this.apiKey)) {
      log.warn("It is forbidden to clear cache with this API key");
    }
    CacheServiceResponse response = new CacheServiceResponse();
    log.info("Attempting to acquire Redis Connection from pool");
    Jedis jedis = (Jedis) jedisConnectionFactory.getConnection().getNativeConnection();
    jedis.select(databaseIndex);
    log.info("Acquired Redis Connection from pool for database #{}" + databaseIndex);
    response.setKey(cacheKey);
    if (ALL.equalsIgnoreCase(cacheKey)) {
      // Delete all caches:
      jedis.flushAll();
      response.setResult("Cache flushed - All entries");
      response.setCount("All");
    } else {
      // Delete specific caches:
      Long count = deleteCache(jedis, cacheKey);
      if (count <= 0) response.setResult(CACHE_FLUSH_NO_ENTRIES);
      else response.setResult(CACHE_FLUSH_COMPLETE);
      response.setCount(count.toString());
    }
    log.info("Attempting to close Redis Connection from pool");
    jedis.close();
    log.info("Closed Redis Connection from pool");
    return response;
  }

  @SneakyThrows
  public CacheServiceResponse readOne(String apiKey, String token, String cacheKey) {
    if (!apiKey.equals(this.apiKey)) {
      log.warn("It is forbidden to clear cache with this API key");
    }

    log.info("Attempting to acquire Redis Connection from pool");
    Jedis jedis = (Jedis) jedisConnectionFactory.getConnection().getNativeConnection();
    jedis.select(databaseIndex);
    log.info("Acquired Redis Connection from pool");
    String value = jedis.get(cacheKey);

    CacheServiceResponse response = new CacheServiceResponse();
    response.setKey(cacheKey);
    response.setCount("1");
    response.setResult(value);

    log.info("Attempting to close Redis Connection from pool");
    jedis.close();
    log.info("Closed Redis Connection from pool");

    return response;
  }

  private Long deleteCache(Jedis jedis, String keysPattern) {
    Set<String> matchingKeys = jedis.keys(keysPattern);
    Long keysCleared = jedis.del(matchingKeys.toArray(new String[0]));
    return keysCleared;
  }
}
