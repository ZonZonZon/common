package com.zonzonzon.common.service.cache;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Response structure for cache operations.
 */
@Getter
@Setter
@NoArgsConstructor
public class CacheServiceResponse {

	private String key;
	private String count;
	private String result;
}
