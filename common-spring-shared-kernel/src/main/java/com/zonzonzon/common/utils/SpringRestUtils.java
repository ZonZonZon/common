package com.zonzonzon.common.utils;

import java.net.InetAddress;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;

/** Helper methods for controllers. */
@UtilityClass
@Slf4j
public class SpringRestUtils {

  /**
   * Provides a starting part of a URL, based on the environment of the current application
   * instance. Example: http://127.0.0.1:8888 If host or port are undefined, default values are
   * 127.0.0.1:8080
   *
   * @param environment Current environment instance.
   * @param isSecure Is HTTPS connection. Otherwise HTTP.
   * @return A String to concatenate with path continuation.
   */
  @SneakyThrows
  public static String getUrlHostPort(Environment environment, boolean isSecure) {
    String protocol = isSecure ? "https://" : "http://";
    String host = InetAddress.getLocalHost().getHostAddress();
    if (host == null) {
      log.warn("Current application host is undefined - using the default one 127.0.0.1");
    }
    String port = environment.getProperty("server.port");
    if (port == null) {
      port = "8080";
      log.warn("Current application port is undefined - using the default one 8080");
    }
    String urlHostPort = protocol + host + ":" + port;
    log.info("Current application instance host and port is " + urlHostPort);
    return urlHostPort;
  }
}
