package com.zonzonzon.common.ui.data;

import static com.zonzonzon.common.utils.ReflectionUtils.getSafe;
import static java.util.Arrays.asList;
import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.data.domain.Sort.Direction.DESC;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.lang.Nullable;
import org.springframework.util.ReflectionUtils;

/** Common pagination methods. */
@UtilityClass
@Slf4j
@SuppressWarnings("UnnecessaryLocalVariable")
public class PaginationUtils {

  /**
   * When declaring sort parameter in URL request, this char is used to separate field name and sort
   * direction in a parameter.
   */
  private static final String URL_PARAMETER_SORT_DIRECTION_SEPARATOR = "\\*";

  private static String URL_PARAMETER_SORT_DESCENDING_DIRECTION = "desc";

  /**
   * Paginate current collection data. This is a post-pagination as it just slices a piece from the
   * whole fetched data set.
   *
   * @param data All data.
   * @param pageIndex Page index when data is sliced into pages.
   * @param pageSize Records returned per page.
   */
  public static <T> Collection<T> getPaginated(
      Collection<T> data, Integer pageIndex, Integer pageSize) {

    Collection<T> paginatedData =
        data.stream().skip(pageIndex * pageSize).limit(pageSize).collect(Collectors.toList());

    return paginatedData;
  }

  /**
   * Prepare pageable object from an API with a single sort parameter that combines multiple sorts.
   *
   * @param sortBy A string to be used in sorting in the format: "myField.city,myOtherFieldDESC"
   * @param pageIndex Page number to return, starting from 0.
   * @param size Number of records per page.
   * @return Object for Spring pageable repository.
   */
  public static Pageable getPageable(String sortBy, int pageIndex, int size) {

    List<Order> orders = new ArrayList<>();
    List<String> allSorts = new LinkedList<>(asList(sortBy.split(",")));
    allSorts.forEach(
        sortString -> {
          boolean isDesc = sortString.endsWith("DESC");
          sortString = sortString.replaceAll("ASC", "");
          sortString = sortString.replaceAll("DESC", "");
          orders.add(new Order(isDesc ? DESC : ASC, sortString));
        });

    Pageable pageable = PageRequest.of(pageIndex, size, Sort.by(orders));

    return pageable;
  }

  /**
   * Prepare pageable object from an API with a multiple sort parameters.
   *
   * @param pageIndex Page number to return, starting from 0.
   * @param pageSize Number of records per page.
   * @param sorts A collection of sorting strings in the format: "myField,DESC". Default is ASC (if
   *     omitted or empty after comma).
   * @return Object for Spring pageable repository.
   */
  public static Pageable getPageable(int pageIndex, int pageSize, @NotNull List<String> sorts) {

    List<Order> orders = new ArrayList<>();
    sorts.forEach(
        sort -> {
          String[] split = sort.split(",");
          boolean isDesc = split.length > 1 && split[1].equals("DESC");
          sort = sort.replaceAll(",", "");
          sort = sort.replaceAll("ASC", "");
          sort = sort.replaceAll("DESC", "");
          orders.add(new Order(isDesc ? DESC : ASC, sort));
        });

    Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by(orders));
    return pageable;
  }

  public static int countNumberOfPages(int totalElements, int pageSize) {
    int numberOfPages = totalElements / pageSize;

    if (totalElements % pageSize != 0) {
      numberOfPages = numberOfPages + 1;
    }
    return numberOfPages;
  }

  /**
   * Wraps a native query with pagination into a Spring Data Page. To get Total Elements native
   * query should contain <code>SELECT SQL_CALC_FOUND_ROWS id AS rows_count ...
   * LIMIT :limit OFFSET :offset;</code>.
   *
   * @param pageable Initial pageable object from request.
   * @param results Database query result.
   * @param entityManager EntityManager used.
   */
  public <T> Page<T> getPage(List<T> results, Pageable pageable, EntityManager entityManager) {
    final String SELECT_FOUND_ROWS = "SELECT FOUND_ROWS();";

    int totalElements =
        Integer.parseInt(
            entityManager.createNativeQuery(SELECT_FOUND_ROWS).getSingleResult().toString());

    Page<T> page = new PageImpl<>(results, pageable, totalElements);
    return page;
  }

  /**
   * Converts a list of strings into Spring Data sorting.
   *
   * @param sort Strings of two placeholders each (comma separated): field name and sorting
   *     direction. If comma and sorting direction are missing, then default sorting direction is
   *     applied (ASC). Examples: 1) firstname 2) lastname,asc.
   * @param entityClass To check if field to sort exists, before trying to sort it.
   * @return A Sort object ready to be used in page requests.
   */
  public static Sort getSorting(@Nullable List<String> sort, Class<?> entityClass) {
    if (sort == null) {
      return Sort.unsorted();
    }

    List<Order> orders = new ArrayList<>();

    sort.forEach(
        sortField -> {
          String fieldName = sortField.split(URL_PARAMETER_SORT_DIRECTION_SEPARATOR)[0];
          Field field = getSafe(() -> ReflectionUtils.findField(entityClass, fieldName));
          if (field != null) {
            boolean isDescending =
                getSafe(() -> sortField.split(URL_PARAMETER_SORT_DIRECTION_SEPARATOR)[1]) != null
                    && sortField.split(URL_PARAMETER_SORT_DIRECTION_SEPARATOR)[1].equals(
                        URL_PARAMETER_SORT_DESCENDING_DIRECTION);

            orders.add(isDescending ? Order.desc(fieldName) : Order.asc(fieldName));
          }
        });

    return Sort.by(orders);
  }
}
