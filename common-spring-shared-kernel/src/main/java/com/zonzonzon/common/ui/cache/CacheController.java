package com.zonzonzon.common.ui.cache;

import static com.zonzonzon.common.utils.RestUtils.API_KEY_HEADER_NAME;
import static com.zonzonzon.common.utils.RestUtils.TOKEN_HEADER_NAME;
import static java.util.concurrent.CompletableFuture.supplyAsync;

import com.zonzonzon.common.service.cache.CacheService;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/cache", produces = "application/hal+json")
@Slf4j
@ConditionalOnProperty(prefix = "com.zonzonzon.common.caching-api", name = "enabled", havingValue = "true")
public class CacheController {

  private final CacheService cacheService;

  public CacheController(CacheService cacheService) {
    this.cacheService = cacheService;
  }

  @PostMapping("/clear/{cacheKey}")
  public CompletableFuture<ResponseEntity<?>> clear(
      @RequestHeader(API_KEY_HEADER_NAME) String apiKey, @PathVariable("cacheKey") String cacheKey) {

    CompletableFuture<ResponseEntity<?>> response =
        supplyAsync(() -> cacheService.clear(apiKey, cacheKey))
            .handle((resource, throwable) -> ResponseEntity.of(Optional.of(throwable)));

    return response;
  }

  @GetMapping("/{cacheKey}")
  public CompletableFuture<ResponseEntity<?>> readOne(
      @RequestHeader(API_KEY_HEADER_NAME) String apiKey,
      @RequestHeader(TOKEN_HEADER_NAME) String token,
      @PathVariable("cacheKey") String cacheKey) {

    CompletableFuture<ResponseEntity<?>> response =
        supplyAsync(() -> cacheService.readOne(apiKey, token, cacheKey))
            .handle((resource, throwable) -> ResponseEntity.of(Optional.of(resource)));

    return response;
  }
}
