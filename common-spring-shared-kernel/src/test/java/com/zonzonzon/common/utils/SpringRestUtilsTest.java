package com.zonzonzon.common.utils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.lang.annotation.Annotation;
import java.util.Set;
import org.junit.jupiter.api.Test;

class SpringRestUtilsTest {

  @Test
  void _1_Permissions_ContainingAllRequiredPermissions_ComparedIsAuthorized() {
    Set<String> givenPermissions = Set.of("abc", "def");
    Set<String> requiredPermissions = Set.of("abc", "def");

    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return requiredPermissions.toArray(String[]::new);
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, annotation, true);

    assertTrue(missingPermissions.isEmpty());
  }

  @Test
  void _1_Permissions_ContainingAnyRequiredPermissions_ComparedIsAuthorized() {
    Set<String> givenPermissions = Set.of("abc");
    Set<String> requiredPermissions = Set.of("abc", "def");

    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return requiredPermissions.toArray(String[]::new);
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, annotation, false);

    assertTrue(missingPermissions.isEmpty());
  }

  @Test
  void _1_Permissions_ContainingNoneOfAnyRequiredPermissions_ComparedIsNotAuthorized() {
    Set<String> givenPermissions = Set.of("hij");
    Set<String> requiredPermissions = Set.of("abc", "def");

    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return requiredPermissions.toArray(String[]::new);
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, annotation, false);

    assertFalse(missingPermissions.isEmpty());
  }

  @Test
  void _1_Permissions_ContainingNotAllOfRequiredPermissions_ComparedIsNotAuthorized() {
    Set<String> givenPermissions = Set.of("abc");
    Set<String> requiredPermissions = Set.of("abc", "def");

    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return requiredPermissions.toArray(String[]::new);
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, annotation, true);

    assertFalse(missingPermissions.isEmpty());
  }

  @Test
  void _1_Permissions_ContainingSomeOfNoneRequiredPermissions_ComparedIsAuthorized() {
    Set<String> givenPermissions = Set.of("abc");
    Set<String> requiredPermissions = Set.of();

    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return requiredPermissions.toArray(String[]::new);
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, annotation, false);

    assertTrue(missingPermissions.isEmpty());
  }

  @Test
  void _1_Permissions_GivenNoneAgainstSomeRequiredPermissions_ComparedIsNotAuthorized() {
    Set<String> givenPermissions = Set.of();
    Set<String> requiredPermissions = Set.of("abc");

    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return requiredPermissions.toArray(String[]::new);
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, annotation, false);

    assertFalse(missingPermissions.isEmpty());
  }

  @Test
  void _1_Permissions_GivenNoneAgainstNoneRequiredPermissions_ComparedIsAuthorized() {
    Set<String> givenPermissions = Set.of();
    Set<String> requiredPermissions = Set.of();

    Permissions annotation =
        new Permissions() {
          @Override
          public String[] keys() {
            return requiredPermissions.toArray(String[]::new);
          }

          @Override
          public Class<? extends Annotation> annotationType() {
            return Permissions.class;
          }
        };

    Set<String> missingPermissions =
        PermissionUtils.getMissingPermissions(givenPermissions, annotation, false);

    assertTrue(missingPermissions.isEmpty());
  }
}