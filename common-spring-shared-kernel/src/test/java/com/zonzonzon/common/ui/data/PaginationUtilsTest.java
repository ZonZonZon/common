package com.zonzonzon.common.ui.data;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

class PaginationUtilsTest {

  @Test
  void pageSizeAndSort_ConvertedTo_Pageable() {
    // Given:
    int page = 2;
    int size = 100;
    String field1 = "my1stField";
    String field2 = "my2ndField";
    String field3 = "my3rdField";
    String field4 = "my4thField";
    List<String> sort = List.of(field1 + ",DESC", field2 + ",ASC", field3 + ",", field4);
    // When:
    Pageable result = PaginationUtils.getPageable(page, size, sort);
    // Then:
    assertThat(result.getPageNumber()).isEqualTo(page);
    assertThat(result.getPageSize()).isEqualTo(size);
    List<Order> sortingOrders = result.getSort().stream().collect(Collectors.toList());
    assertThat(sortingOrders).hasSize(4);
    assertThat(sortingOrders.get(0).getProperty()).isEqualTo(field1);
    assertThat(sortingOrders.get(0).getDirection()).isEqualTo(Direction.DESC);
    assertThat(sortingOrders.get(1).getProperty()).isEqualTo(field2);
    assertThat(sortingOrders.get(1).getDirection()).isEqualTo(Direction.ASC);
    assertThat(sortingOrders.get(2).getProperty()).isEqualTo(field3);
    assertThat(sortingOrders.get(2).getDirection()).isEqualTo(Direction.ASC);
    assertThat(sortingOrders.get(3).getProperty()).isEqualTo(field4);
    assertThat(sortingOrders.get(3).getDirection()).isEqualTo(Direction.ASC);
  }
}
